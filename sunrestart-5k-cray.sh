#!/bin/bash --login
#
#SBATCH --account=pawsey0106
#SBATCH --time=8:00:00
##SBATCH --time=01:00:00
#SBATCH --export=PYTHONPATH="/home/mrayson/code:$PYTHONPATH"
#SBATCH --job-name=Hex5k_3D
#SBATCH --partition=workq
##SBATCH --partition=debugq
#SBATCH --output=SCENARIOS/LOGS/NWS_Hex5k-%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=matt.rayson@uwa.edu.au
###SBATCH --nodes=4

#######
# Example restart test case on a Cray system
#
# Usage:
#       (sbatch iwaves_ridge-restart-cray.sh $NUMNODES $RUNNUM)
#       sbatch --nodes=$NUMNODES sunrestart-cray.sh $RUNNUM)
#######

########
# Load environment stuff here
module use ~/code/modulefiles
module load anaconda-python/2.0.0


# Example python runs
##aprun -n 1 $PYTHON -u $PYTHONSCRIPT $2 $3 $4 $5

##########
# Input variables
SUNTANSHOME=SCENARIOS
SUN=$SUNTANSHOME/sun
#PYTHONEXEC=python
PYTHONEXEC="srun -n 1 /home/mrayson/anaconda2/bin/python -u"
#SUBCOMMAND=sh
SUBCOMMAND=sbatch
VERBOSE=-vvv

#. $SUNTANSHOME/Makefile.in

BASENAME=NWS_5km_hex
MAINDATADIR=SCENARIOS/${BASENAME}_rundata
DATADIR=SCENARIOS/${BASENAME}_data
MAKESCRIPT=SCRIPTS/suntans_driver_timorsea_3d.py
METFILE=ERA_SUNTANS_NWS_20052015_merc.nc
XYFILE=timor_station_xy.dat
RUNFILE=restart-run-2013-2014.txt
MYSCRIPT=sunrestart-5k-cray

SUNTANS_DT=30

MAXRUNS=12
#NUMPROCS=$1
NUMNODES=${SLURM_JOB_NUM_NODES}
#NUMCPUPNODE=${SLURM_JOB_CPUS_PER_NODE}
NUMCPUPNODE=24
NUMPROCS=$(($NUMNODES * $NUMCPUPNODE))
#NUMPROCS=`expr ${NUMNODES} * ${NUMCPUPNODE}`
RUNNUM=$1
NEXTRUN=`expr $RUNNUM + 1`


# End of inputs
##################################################

#if [ -z "$MPIHOME" ] ; then
#    EXEC=$SUN
#else
#    EXEC="$MPIHOME/bin/mpirun -np $NUMPROCS $SUN"
#fi
#EXEC="srun -n $NUMPROCS -N $NUMCPUPNODE $SUN"
EXEC="srun -n $NUMPROCS -N $NUMNODES $SUN"

echo NUMNODES = $NUMNODES, NUMPROCS = $NUMPROCS
echo EXEC = $EXEC

###
#Read in some input variables from a text file
###
ii=1;
while read -r line
do 
    name="$line"
    if [ $ii -eq $RUNNUM ]; then
        jj=0;
        for word in ${name}; do
	    if [ $jj -eq 1 ]; then
	       BASETIME=$word
	    elif [ $jj -eq 2 ]; then
	       NUMDAYS=$word
	    fi
	    #jj=$(($jj+1))
	    jj=`expr $jj + 1`
        done
    fi
    ii=`expr $ii + 1`
done < $RUNFILE

NSTEPS=$(($NUMDAYS * 86400 / $SUNTANS_DT))


###
# 
JOBDIR=${DATADIR}$RUNNUM

if [ $RUNNUM -eq 1 ] ; then
    if [ ! -d $JOBDIR ] ; then
	cp -r $MAINDATADIR $JOBDIR
	echo Creating input files...
	$PYTHONEXEC $MAKESCRIPT $JOBDIR $BASETIME $NUMDAYS 1 1 1
	echo Creating grid...
	$EXEC -g -vvv --datadir=$JOBDIR
    else
	cp $MAINDATADIR/suntans.dat $JOBDIR/.
    fi

    #echo Creating input files...
    #$PYTHONEXEC $MAKESCRIPT $JOBDIR $BASETIME $NUMDAYS 1 0 1
    PARAM=nsteps
    PARAMVALUE=$NSTEPS
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${JOBDIR}/suntans.dat

    PARAM=outputNetcdfFile
    PARAMVALUE=${BASENAME}_${BASETIME}
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${JOBDIR}/suntans.dat

    PARAM=averageNetcdfFile
    PARAMVALUE=${BASENAME}_${BASETIME}_AVG
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${JOBDIR}/suntans.dat

    echo Running suntans...
    $EXEC -s $VERBOSE --datadir=$JOBDIR 

else

    echo Creating input files...
    $PYTHONEXEC $MAKESCRIPT $JOBDIR $BASETIME $NUMDAYS 1 0 1

    PARAM=nsteps
    PARAMVALUE=$NSTEPS
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${JOBDIR}/suntans.dat

    PARAM=outputNetcdfFile
    PARAMVALUE=${BASENAME}_${BASETIME}
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${JOBDIR}/suntans.dat

    PARAM=averageNetcdfFile
    PARAMVALUE=${BASENAME}_${BASETIME}_AVG
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${JOBDIR}/suntans.dat

    $EXEC -s -r $VERBOSE --datadir=$JOBDIR 
fi

#######
# Copy files for the next run
#######
if [ $RUNNUM -lt $MAXRUNS ] ; then
    if [ ! -d ${DATADIR}$NEXTRUN ] ; then
	#cp -r $MAINDATADIR ${DATADIR}$NEXTRUN
        # Use Lustre striping to try and reduce IO time
        mkdir ${DATADIR}$NEXTRUN
        lfs setstripe ${DATADIR}$NEXTRUN -c 24 -S 1g
	cp $MAINDATADIR/*.* ${DATADIR}$NEXTRUN/
    fi

    # Copy the grid files
    cp $JOBDIR/vertspace.dat ${DATADIR}$NEXTRUN/
    cp ${JOBDIR}/$METFILE ${DATADIR}$NEXTRUN/
    cp ${JOBDIR}/$XYFILE ${DATADIR}$NEXTRUN/

    # Copy the processor-based files
    n=0;
    while(true)
    do
      if [ $n -lt $NUMPROCS ] ; then
	  cp $JOBDIR/cells.dat.$n ${DATADIR}$NEXTRUN/
	  cp $JOBDIR/celldata.dat.$n ${DATADIR}$NEXTRUN/
	  cp $JOBDIR/topology.dat.$n ${DATADIR}$NEXTRUN/
	  cp $JOBDIR/edges.dat.$n ${DATADIR}$NEXTRUN/
	  cp $JOBDIR/edgedata.dat.$n ${DATADIR}$NEXTRUN/
	  cp $JOBDIR/nodes.dat.$n ${DATADIR}$NEXTRUN/

	  cp $JOBDIR/store.dat.$n ${DATADIR}$NEXTRUN/start.dat.$n
	  n=`expr $n + 1`
      else
	  break;
      fi
    done

    if [ ! -z $VERBOSE ] ; then
	echo Job finished on `date`
    fi

    ###
    # Update suntans.dat parameters
    ###

    echo Updating suntans.dat...
    PARAM=readinitialnc
    PARAMVALUE=0
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${DATADIR}$NEXTRUN/suntans.dat

    PARAM=thetaramptime
    PARAMVALUE=0
    sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    sed -i $sedstr ${DATADIR}$NEXTRUN/suntans.dat

    #PARAM=nsteps
    #PARAMVALUE=$NSTEPS
    #sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    #sed -i $sedstr ${DATADIR}$NEXTRUN/suntans.dat

    #PARAM=ncfilectr
    #PARAMVALUE=1
    #sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    #sed -i $sedstr ${DATADIR}$NEXTRUN/suntans.dat

    ###
    # Do not need to adjust this for the restart time
    #PARAM=starttime
    #PARAMVALUE=$BASETIME
    #sedstr='s/^'$PARAM'.*/'$PARAM'\t\t\t'$PARAMVALUE'\t#/g' 
    #sed -i $sedstr ${DATADIR}$NEXTRUN/suntans.dat

    ###
    # Run the next scenario
    #echo $NEXTRUN > $RUNNUMFILE
    $SUBCOMMAND --nodes=$NUMNODES $MYSCRIPT.sh $NEXTRUN
else
    echo Finished with $MAXRUNS runs!
    #if [ ! -z $VERBOSE ] ; then
    #    if [ $blowup -ne 0 ] ; then
    #        echo Run $RUNNUM is blowing up.  Not resubmitting.
    #    else
    #        echo Finished with $MAXRUNS runs!
    #    fi
    #fi
fi
