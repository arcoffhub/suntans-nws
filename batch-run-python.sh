PYTHONSCRIPT=SCRIPTS/convert_surface_suntans.py
BASEDIR=SCENARIOS/NWS_5km_hex_data
OUTDIR=SCENARIOS/OUTPUT_NWS_5km_hex/

for i in $(seq 1 12)
   do
        indir=$BASEDIR$i
        echo $indir
        infile=$(printf "NWS_5km_hex_2014%02g01_0000.nc" $i)
        outfile=$(printf "NWS_5km_hex_2014%02g01_surface.nc" $i)
        #sbatch
        echo $indir $infile $outfile
        sbatch zeus-python-2 $PYTHONSCRIPT $indir $infile $outfile
        #mv $indir/$outfile $OUTDIR
   done
