#!/bin/bash
#
# Short script to concatenate the output data from multiple runs
# into one file.
#
set -x # echo on

MAXRUNS=12

#DATADIR=SCENARIOS/NWS_2km_hex_2013_2014_data
#OUTDIR=SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/

#DATADIR=SCENARIOS/NWS_2km_GLORYS_hex_Nk80_2013_2014_data
#OUTDIR=SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2013_2014/

DATADIR=SCENARIOS/NWS_2km_GLORYS_hex_Nk80dt60_2013_2014_data
OUTDIR=SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2013_2014/

#DATADIR=SCENARIOS/NWS_2km_hex_2019_data
#OUTDIR=SCENARIOS/OUTPUT_NWS_2km_hex_2019/


#DATADIR=SCENARIOS/NWS_2km_GLORYS_hex_2012_2013_data
#OUTDIR=SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2012_2013/




if [ ! -d $OUTDIR ] ; then
    mkdir $OUTDIR
    lfs setstripe $OUTDIR -c 24 -S 1g
fi

cat ${DATADIR}{1..12}/fs.dat.prof > ${OUTDIR}/fs.dat.prof
cat ${DATADIR}{1..12}/u.dat.prof > ${OUTDIR}/u.dat.prof
cat ${DATADIR}{1..12}/T.dat.prof > ${OUTDIR}/T.dat.prof
cat ${DATADIR}{1..12}/s.dat.prof > ${OUTDIR}/s.dat.prof
cat ${DATADIR}{1..12}/kappat.dat.prof > ${OUTDIR}/kappat.dat.prof

cp ${DATADIR}1/profdata.dat ${OUTDIR}/profdata.dat

##if [ $MAXRUNS -gt 9 ] ; then
##    cat data??/fs.dat.prof >> data/fs.dat.prof
##    cat data??/u.dat.prof >> data/u.dat.prof
##fi
##
##if [ $MAXRUNS -gt 99 ] ; then
##    cat data???/fs.dat.prof >> data/fs.dat.prof
##    cat data???/u.dat.prof >> data/u.dat.prof
##fi


