# Bathymetry Interpolation Procedures

## Step 1) Convert sparse datasets to hdf format (quicker to load) `convert_xyz_bathy2hdf5.py`

   Filename: 'WEL_NWS_raw_bathymetry.h5', 'GA_Multibeam_NWS_250m_DEM.nc',
        'BrowseRawBathymetry_xyz.h5'

## Step 2) Create a GA-GEBCO basegrid `create_nws_dem.py`
    
   Filename: 'TimorSea_GA_GEBCO_Combined_DEM.nc'

## Step 3) Interpolate sparse data onto a common grid `create_ga_dem_from_hdf.py`
    `create_dem_from_hdf.py`
   
   Filename: 'WEL_Multibeam_NWS_250m_DEM.nc',
            'WEL_Multibeam_Browse_250m_DEM.nc',
            'GA_Multibeam_NWS_250m_DEM.nc'

## Step 4) Merge GA and Woodside sparse multibeam datasets `create_nws_multibeam_dem.py`

   Filename: 'GA_WEL_MultiBeam_NWSBrowse_DEM.nc'

## Step 5) Merge gridded with gappy data `create_dem_wfilter_nws.py`

    Filename: 'GA_WEL_NWS_250m_DEM.nc'

