# Method/Instructions to build an internal tide climatology from SUNTANS output

An overview of the steps are:

   1. Compute $SSH_BC$ from the raw density data
   2. Fit tidal harmonics to short-time (30 day) windows of all 2D and 3D model variables
   3. Fit vertical modes to buoyancy and velocity data
   4. Merge the harmonic-modal data into one file
   5. Fit annual harmonics to the tidal harmonic-modal data
   6. Merge mean variables into a climatology file
   7. Fit a parametric profile to the short-time window mean density/buoyancy frequency

---

SCENARIO_NAME=NWS_2km_GLORYS_hex_2013_2014

## Step 1: Compute baroclinic sea surface height

Script(s):
    - `calc_sshbc_mpi.py`

Output file details:
    - File name(s): NWS_2km_GLORYS_hex_2013_2014_SSHBC.nc.*
    - Variables:

## Step 2: Fit tidal harmonics to short-time windows

Script(s):
    - `calc_sunharmonics_mpi.py`
    - `calc_sshbc_harmonics.ipynb`: SSHBC only. This also merges the data

Output file details:
    - File name(s): NWS_2km_$(TIMESTART)_$(TIMEEND)_3D_Harmonics.* 
    - Variables:

## Step 3: Fit vertical modes to harmonic data

Script(s):
   - `suntans_mode_fit_mpi.py`
   - `suntans_mode_fit_uv_mpi.py` 

Output file details:
   - File name(s): NWS_2km_$(TIMESTART)_$(TIMEEND)_3D_ModeAmp.*, NWS_2km_$(TIMESTART)_$(TIMEEND)_3D_UVModeAmp.*
   - Variables: `cn`, `alpha_n`, `N2`, `amp_b_re`, `amp_b_im`, `amp_u_re`, `amp_u_im`, `amp_v_re`, `amp_v_im`
   - Notes: harmonic variables (e.g. `amp_b_re`) have a time dimension

## Step 4: Merge the modal-harmonic data

Script(s):
    - `create_internaltide_atlas_ugrid.ipynb`: Buoyancy only
    - `create_internaltide_atlas_uv_ugrid.ipynb`: Buoyancy only


Output file details:
   - File name(s): NWS_2km_GLORYS_hex_2013_2014_Amplitude_Atlas.nc, NWS_2km_GLORYS_hex_2013_2014_UVAmplitude_Atlas.nc
   - Variables: `cn`, `alpha_n`, `N2`, `amp_b_re`, `amp_b_im`, `amp_u_re`, `amp_u_im`, `amp_v_re`, `amp_v_im`

## Step 5: Fit annual harmonics from the modal-harmonic data

**Note**: this step requires the inverse of the [iwatlas.harmonics.harmonic_to_seaonal][https://github.com/mrayson/iwatlas/blob/master/iwatlas/harmonics.py] function. 

Script(s): **TODO** (although see `calc_annual_harmonics.ipynb` and *Step 5a* for inspiration)

Output file details:
   - File name(s): (propose something like NWS_2km_GLORYS_hex_2013_2014_ModeAmp_Harmonics.nc)
   - Variables: 
   
## Step 5a: Fit nonstationary (tidal+seasonal) harmonics to the SSH data

**Note**: For this variable we did not fit tidal harmonics to short time windows first. The expanded non-stationary harmonic parameters, in the JGR paper, are computed directly from the hourly output data. We also used dask to do the harmonic analysis - not mpi+lapack as with the 3D data. 

Script(s):
    - `calc_sshbc_harmonics.ipynb`: 

Output file details:
    - File name(s): NWS_2km_GLORYS_hex_2013_2014_SSHBC_Harmonics.nc
    - Variables: `SSH_BC_var`,`SSH_BC_aa`, `SSH_BC_Aa`, `SSH_BC_Ba`

## Step 6: Merge mean variables into a climatology file

Script(s):
   - `merge_suntans_clim.ipynb`

Output file details:
   - File name(s): NWS_2km_GLORYS_hex_2013_2014_Climatology.nc
   - Variables: `rho`, `uc`, `vc`, `eta`

## Step 7: Fit a parametric profile to the climatological density and fit annual harmonics

Script(s): `create_density_clim.ipynb`

Output file details:
   - File name(s): NWS_2km_GLORYS_hex_2013_2014_Stratification_Atlas.nc
   - Variables: `N2_t`, `N2_mu`, `N2_re`, `N2_im`




