#!/usr/bin/env python

import csv
import urllib2
import StringIO

# The URL to the collection (as comma-separated values).
collection_url = "http://geoserver-123.aodn.org.au/geoserver/ows?typeName=soop_trv_trajectory_data&SERVICE=WFS&outputFormat=csv&REQUEST=GetFeature&VERSION=1.0.0&CQL_FILTER=INTERSECTS(geom%2CPOLYGON((107.8857421875%20-28.0380859375%2C107.8857421875%20-5.4501953125%2C133.1982421875%20-5.450195312499998%2C133.1982421875%20-28.0380859375%2C107.8857421875%20-28.0380859375)))"

# Fetch data...
response = urllib2.urlopen(collection_url)

# Iterate on data...
ii=0
for row in csv.reader(response):
    ii+=1
    print row

    if ii == 10:
        break
