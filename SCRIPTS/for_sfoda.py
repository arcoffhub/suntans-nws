"""
functions to go into sfoda
"""

from netCDF4 import Dataset
from sfoda.suntans.suntans_ugrid import ugrid
from datetime import datetime

# Write the grid variables
def write_nc_var(nc, var, name, dimensions, attdict, dtype='f8'):
    tmp=nc.createVariable(name, dtype, dimensions, fill_value=999999.)
    for aa in list(attdict.keys()):
        tmp.setncattr(aa,attdict[aa])
    nc.variables[name][:] = var

def write_nc(sun, outfile, VERBOSE=True):
    """
    Export the grid variables to a netcdf file
    """
    self = sun
    Nkmax = self.Nk.max()
    maxfaces = self.nfaces.max()

    nc = Dataset(outfile, 'w', format='NETCDF4_CLASSIC')
    nc.Description = 'SUNTANS History file'
    nc.Author = ''
    nc.Created = datetime.now().isoformat()

    nc.createDimension('Nc', self.Nc)
    nc.createDimension('Np', self.Np)
    if hasattr(self, 'Ne'):
        nc.createDimension( self.Ne)
    else:
        print('No dimension: Ne')

    nc.createDimension('Nk', Nkmax)
    nc.createDimension('Nkw', Nkmax+1)
    nc.createDimension('numsides', maxfaces)
    nc.createDimension('two', 2)
    nc.createDimension('time', 0) # Unlimited

    # Just write the bare-bones grid variables (everything else can be calculated)
    #gridvars = ['cells','xp','yp','xv','yv','nfaces','Nk','dv']
    gridvars = ['cells','xp','yp','xv','yv','lonv','latv','nfaces','Nk','dv','mark','edges','grad','neigh']


    for vv in gridvars:
        if vv in self.__dict__ and vv != 'time':
            if VERBOSE:
                print('Writing variables: %s'%vv)
            write_nc_var(nc, self[vv],vv,ugrid[vv]['dimensions'],ugrid[vv]['attributes'],dtype=ugrid[vv]['dtype'])


    nc.close()
