"""
Test sundask class
"""

import xarray as xr
import dask
import glob
from netCDF4 import Dataset
import numpy as np

import matplotlib.pyplot as plt
import pdb

from soda.dataio.ugrid.uplot import Plot as UPlot

class Sundask(UPlot):
    """
    Parallel SUNTANS reader
    Goal is to have the same functionality to the user as Sunxray
    i.e. not worry about the parallel io, dask, etc
    """

    def __init__(self, ncfiles, **kwargs):

        #print(ncfiles)

        # Get the files from the first file
        filenames = sorted(glob.glob(ncfiles))
        #print(filenames[0:10])
        

        # Load all of the files into list as xray objects
        #self._myfiles = [Sunxray(url, lazy=True) for url in filenames]
        def openfile(url):
            #print(url)
            try:
                ds = xr.open_dataset(url, chunks={'Nk':-1,'Nc':-1,'time':-1})
                #ds = xr.open_dataset(url,)

            except:
                print('Failed to open file %s'%url)
            
            return ds
        
        self._myfiles = [openfile(url) \
            for url in filenames]
        #self._myfiles=[]
        #for url in filenames:
        #    print(url)
        #    try:
        #        self._myfiles.append(xr.open_dataset(url, chunks={'Nk':-1,'Nc':-1,'time':-1}))
        #    except:
        #        print('File %s failed!!'%url)

        # Keep this for compatibility with methods from the superclass
        self._ds = self._myfiles[0]

        # Load the grid variables 

        ### Grid does not need re-sorting... but we need ghost points
        self.ghost = self.find_ghosts()
        
        # Each files stores all node coordinates (luckily)
        xp = self._myfiles[0]['xp']
        yp = self._myfiles[0]['yp']

        # Load the actual data
        cells = self.stack_var_2d('cells', axis=0)[self.ghost,...].compute()
        nfaces = self.stack_var_2d('nfaces', axis=0)[self.ghost].compute()
        
        # Finish initializing the class
        UPlot.__init__(self, xp, yp, cells, nfaces=nfaces,\
            _FillValue=-999999,\
                **kwargs)

        #self.xlims = [xp.min(), xp.max()]
        #self.ylims = [yp.min(), yp.max()]


    def find_ghosts(self):
        # find ghost cells in each dataset
        mnptr = self.stack_var_2d('mnptr',axis=0).compute()
        cells_unique = np.unique(mnptr)
        #Nc = cells_unique.shape[0]
        Nc = mnptr.shape[0]
        allghost = np.ones((Nc,),dtype=np.bool)
        allcells = set()

        for ii,cc in enumerate(mnptr):
            if cc in allcells:
                allghost[ii] = False
            else:
                allcells.add(cc)
                
        return allghost
    
    def find_ghosts_old(self):
        # find ghost cells in each dataset
        allcells = set()
        allghost = np.array((0,),dtype=np.bool)

        for myfile in self._myfiles:
            #positions = [myfile['mnptr'].values for myfile in myfiles]
            myghost = np.ones((myfile.dims['Nc'],), dtype=np.bool)
            for ii, cc in enumerate(myfile['mnptr'].values):
                if cc in allcells:
                    myghost[ii] = False
                else:
                    allcells.add(cc)

            #allghost.append(myghost)
            #allghost = np.hstack([np.array(myghost),allghost])
            allghost = np.hstack([allghost,np.array(myghost)])


        return allghost


    ### Loading functions
    def load_data(self, varname, tstep=slice(None,), klayer=slice(None,)):
        """
        Load the data from the xray.Dataset object
        """
        data = self.get_data(varname, tstep=tstep, klayer=klayer)
        return data.compute()[...,self.ghost]
    
    def get_data(self, varname, tstep=slice(None,), klayer=slice(None,)):
        """
        Get the data from the xray.Dataset object
        
        This does not load the actual data into memory see: `load_data`
        """
        myvar = self._myfiles[0][varname]
        ndim = myvar.ndim
        arrays=[]
        if ndim==1:
            for ii, data in enumerate(self._myfiles):
                mydata = data[varname].data[:]
                arrays.append(mydata)
        if ndim==2:
            for ii, data in enumerate(self._myfiles):
                mydata = data[varname].data[tstep,:]
                arrays.append(mydata)
        elif ndim==3:   
            for ii, data in enumerate(self._myfiles):
                mydata = data[varname].data[tstep,klayer,:]
                arrays.append(mydata)
        
        # Stack all small Dask arrays into one
        return dask.array.concatenate(arrays, axis=-1)#[...,ghost]
    
    def stack_var_3d(self, varname, klayer, axis=-1):
        arrays=[]
        for ii, data in enumerate(self._myfiles):
            mydata = data[varname].data[:,klayer,:]
            arrays.append(mydata)

        return dask.array.concatenate(arrays, axis=axis)

    def stack_var_2d(self, varname, axis=-1):
        arrays=[]
        for ii, data in enumerate(self._myfiles):
            mydata = data[varname].data[:]
            arrays.append(mydata)

        return dask.array.concatenate(arrays, axis=axis)
    
    #####
    # Old
    def loadfull(self, varname, axis=0):
        """
        Load a full array (no indexing) into a dask array
        """

        def localload(ds, varname):
            return ds[varname][:].values

        loadfull_d = dask.delayed(localload, pure=True) 
        all_files = self._myfiles

        # Lazily evaluate loadfull on each file
        lazy_data = [loadfull_d(url._ds, varname) for url in all_files]

        # Construct a Dask array
        arrays = [da.from_delayed(lazy_value,           
                      dtype=sun._ds[varname].dtype,   # for every lazy value
                      shape=sun._ds[varname].shape,
                      ) for sun, lazy_value in zip(all_files, lazy_data)]

        # Stack all small Dask arrays into one
        return dask.array.concatenate(arrays, axis=axis)

    def load(self, varname, tstep, klayer):
        """
        Load the data from the xray.Dataset object
        """
        myvar = self.loadfull(varname, axis=-1)
        ndim = myvar.ndim
        if ndim==1:
            return myvar[:].compute()
        if ndim==2:
            return myvar[tstep,:].compute().ravel()
        elif ndim==3:   
            return myvar[tstep,klayer,:].compute().ravel()
        
    
################
# Testing
#ncfiles = '../SCENARIOS/NWS_2km_hex_2013_2014_data6/NWS_2km_hex_20131201.*'
#ncfiles = '../SCENARIOS/NWS_2km_hex_2013_2014_data9/NWS_2km_hex_20140301.*'
#ncfiles = '../SCENARIOS/NWS_2km_hex_2013_2014_data11/NWS_2km_hex_20140501.*'

#sun = Sundask(ncfiles)