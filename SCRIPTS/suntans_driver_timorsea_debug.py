# -*- coding: utf-8 -*-
"""
Driver script for generating suntans input files

2D (barotropic application)

Created on Fri Oct 05 11:24:10 2012

@author: mrayson
"""

from soda.dataio.suntans.sunpy import Grid
from soda.dataio.suntans.sundepths import DepthDriver
from soda.dataio.suntans.sunboundary import modifyBCmarker, Boundary, InitialCond
from soda.dataio.suntans.sundriver import dumpinputs
from soda.dataio.suntans.metfile import SunMet

import numpy as np 
import scipy.io as io
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import os
from datetime import datetime, timedelta

import pdb

PI = 3.141592653589793

def main(suntanspath, starttime, endtime,\
        makebathy=True, makeinitial=True, makebnd=True):

    ######################################################################
    #   Input variables
    ######################################################################
    #makebathy = True
    #makebnd = True
    #makeinitial = True

    makewinds = False

    #suntanspath = 'rundata_200701'
    #suntanspath = 'rundata_200706'

    # Time parameters
    #starttime = '20090802.0000'
    #endtime = '20090901.0000'
    #starttime = '20070528.0000' # Start time of BC and wind file 'yyyymmdd.HHMM'
    #endtime = '20070701.0000' # End time of BC and wind file 'yyyymmdd.HHMM'

    dt = 3600.0 # time step of BC and wind file (seconds)

    plotdir = 'FIGURES/inputdata'
    ####
    # Bathymetry interpolation options
    ####
    basedir = '/home/suntans/Share/ScottReef/DATA'
    basedir2 = '/home/suntans/Share/TimorSea/DATA'
    #basedir = '/group/pawsey0106/mrayson/DATA'
    #basedir2 = '/group/pawsey0106/mrayson/DATA'

    #depthfile = '%s/BATHYMETRY/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir
    depthfile = '%s/BATHYMETRY/TimorSea_GAWEL_Multi_GEBCO_Combined_DEM.nc'%basedir
    #depthfile = '%s/BATHYMETRY/GEBCO_2014_TimorSea.nc'%basedir

    isDEM = True # Interpolate straight from DEM (other options are ignored)

    depthmax=10.
    depthmin=6000.

    # Interpolation method
    interpmethod='nn' # 'nn', 'idw', 'kriging', 'griddata'

    # Type of plot
    plottype=None # 'mpl', 'vtk2' or 'vtk3'

    # Interpolation options
    NNear=4
    p = 1.0 #  power for inverse distance weighting
    # kriging options
    varmodel = 'spherical'
    nugget = 0.1
    sill = 0.8
    vrange = 250.0
    interpnodes=False

    # Projection conversion info for input data
    convert2utm=True
    clip=False # Clip unused points in bathy grid
    projstr='merc'
    CS='WGS84' # Not used
    utmzone=51 # Not used
    isnorth=False # Not used
    vdatum = 'MSL'

    # Smoothing options
    smooth=False
    smoothmethod='gaussian' # USe kriging or idw for smoothing
    smoothnear=4 # No. of points to use for smoothing
    smoothrange=50.

    ####
    # Verical grid options
    ####
    Nkmax = 75 # number of layers
    r = 1.045 # vertical stretching parameter

    ####
    # Open boundary options
    ####
    # type-3 along northern boundary
    #bcpolygonfile = '%s/GIS/TimorSea_SUNTANS_BoundaryTypes_merc.shp'%basedir2 
    # split type-2-3 along northern edge
    bcpolygonfile = '%s/GIS/TimorSea_SUNTANS_BoundaryTypes_Type2_3_merc.shp'%basedir2
    # All type 3
    #bcpolygonfile = '%s/GIS/TimorSea_SUNTANS_BoundaryTypes_Type3_merc.shp'%basedir2

    bcfile = 'TimorSea_BC.nc'

    # Use the OTPS along the boundaries
    #otisfile = '%s/TIDES/TPXO7.2/Model_tpxo7.2'%basedir
    otisfile = '%s/TIDES/Ind2016/Model_Ind_2016'%basedir

    # Ocean model boundary and initial condition data
    #oceanfile = '%s/OCEAN/DAILY/nc4_classic/HYCOM_ScottReef_2015*.nc'%basedir
    #oceanfile = '%s/OCEAN/BRAN2016/BRAN2016_TimorSea_*.nc'%basedir2
    #oceanfiletype = 'BRAN_3p5'
    oceanfile = '%s/OCEAN/GLORYS/GLORYS_NWS_*.nc'%basedir2
    oceanfiletype = 'GLORYS'

    # Met file for plotting only
    metfile = None
    #metfile = '../MET/ERA_SUNTANS_20052015_norain.nc'

    ####
    # Initial condition options
    ####
    icfile = 'TimorSea_IC.nc'

    #agesourcepoly = '%s/GIS/scottreef_agesource_polygon_v2.shp'%basedir
    agesourcepoly=None

    # Sets initial temperature and salinity to T0 and S0, respectively

    ######################################################################
    # End of input variables - do not change anything below here.
    ######################################################################

    # Interpolate the depths onto the grid
    if makebathy:
        D = DepthDriver(depthfile,\
            isDEM=isDEM,\
            interpmethod=interpmethod,\
            plottype=plottype,\
            NNear=NNear,\
            p=p,\
            varmodel=varmodel,\
            nugget=nugget,\
            sill=sill,\
            vrange=vrange,\
            clip=clip,\
            convert2utm=convert2utm,\
            utmzone=utmzone,\
            isnorth=isnorth,\
            projstr=projstr,\
            vdatum=vdatum,\
            smooth=smooth,\
            smoothmethod=smoothmethod,\
            smoothnear=smoothnear,\
        )
        
        D(suntanspath, \
            depthmax=depthmax,\
            depthmin=depthmin,\
            interpnodes=interpnodes)


    # Load the model grid and bathymetry into the object 'grd' and initialise the vertical spacing
    grd = Grid(suntanspath)

    # Load the depth data into the grid object
    grd.loadBathy(suntanspath+'/depths.dat-voro')
    zmax = np.abs(grd.dv).max()

    # Set up the vertical coordinates
    dz = grd.calcVertSpace(Nkmax,r,zmax)
    grd.setDepth(dz)
    print( dz[0:10])

    # Save vertspace.dat
    grd.saveVertspace(suntanspath+'/vertspace.dat')

    # Modify the boundary markers and create the boundary condition file
    if makebnd:
        # This changes the edge types based on the polygons in the shapefile
        modifyBCmarker(suntanspath,bcpolygonfile)
        
        #Load the boundary object from the grid
        #   Note that this zeros all of the boundary arrays
        bnd = Boundary(suntanspath, (starttime,endtime,dt),\
                projstr=projstr, utmzone=utmzone, isnorth=isnorth)

        bnd.setDepth(grd.dv)

        
        ## Interpolate the otis data onto the boundaries
        #if not otisfile is None :
        #    # Need to set the projection attributes
        #    bnd.utmzone=utmzone
        #    bnd.isnorth=isnorth
        #    bnd.otis2boundary(otisfile,conlist=None)

        ## Set using an ocean model
        #bnd.oceanmodel2bdy(oceanfile,seth=False,setUV=True, name=oceanfiletype)
                
        
        # Write the boundary file
        bnd.write2NC(suntanspath+'/'+bcfile)
        
    if makeinitial:
        # Generates an initial condition netcdf file
        
        # Initialise the class
        IC = InitialCond(suntanspath,\
                starttime,\
                projstr=projstr,\
                utmzone=utmzone,\
                isnorth=isnorth,\
        )

        # the initial condition arrays are stored in the following fields in the class:
        #   h, uc, vc, T, S
        
        # Set using an ocean model
        #IC.oceanmodel2ic(oceanfile,seth=False, name=oceanfiletype)

        # Set the age source term
        if agesourcepoly is not None:
            IC.setAgeSource(agesourcepoly)
        
        # Write the initial condition file
        IC.writeNC(suntanspath+'/'+icfile,dv=grd.dv)

    if makewinds:
        # Create a meteorological input file
        xpt = grd.xv.mean()
        ypt = grd.yv.mean()
        zpt = 10.0
        met = SunMet(xpt,ypt,zpt,(starttime,endtime,dt))
        
        # Set a cyclic air temperature and all other variables constant (rain and cloud =0)
        omegaT = 2*PI/(24.0 * 3600.0) # diurnal frequency
        for ii in range(met.Tair.Npt):
            met.Tair.data[:,ii] = Tair_mean + Tair_amp * np.sin(omegaT * met.nctime)
            
        met.Uwind.data[:] = Uwind
        met.Vwind.data[:] = Vwind
        met.RH.data[:] = RH = RH
        met.cloud.data[:] = 0.0
        met.rain.data[:] = 0.0

        met.write2NC(suntanspath+'/'+metfile)

    ######
    #### Dump figures of the input data
    #dump = dumpinputs(suntanspath=suntanspath,bcfile=bcfile)
    ##dump = dumpinputs(suntanspath=suntanspath, metfile=metfile)
    ###
    ##dump(plotdir)

if __name__=='__main__':
    import sys

    sunpath = sys.argv[1]
    startstr = sys.argv[2] # yyyymm
    numdays = int(sys.argv[3])
    makebathy = bool(int(sys.argv[4]))
    makeinitial = bool(int(sys.argv[5]))
    makebnd = bool(int(sys.argv[6]))
    
    # Set the time for the BCs +/- 1 day to be safe
    t1 = datetime.strptime(startstr, '%Y%m%d') - timedelta(days=3)
    t2 = datetime.strptime(startstr, '%Y%m%d') + timedelta(days=numdays + 3)
    starttime = t1.strftime('%Y%m%d.%H%M')
    endtime   = t2.strftime('%Y%m%d.%H%M')

    print( sunpath, starttime, endtime, makebathy, makeinitial, makebnd)

    main(sunpath, starttime, endtime, makebathy=makebathy,
        makeinitial=makeinitial, makebnd=makebnd)
