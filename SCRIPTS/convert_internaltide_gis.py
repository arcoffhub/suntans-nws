"""
Convert the internal tide amp to GIS format
"""

#from mpl_toolkits.basemap import Basemap
import numpy as np
import xray
import pandas as pd
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from datetime import datetime

from soda.utils.otherplot import axcolorbar
from soda.utils.maptools import save_raster

from soda.utils import othertime
#import cmocean
from soda.utils.maptools import plotmap
from soda.dataio.suntans.sunpy import Spatial

import pdb

######

# SUNTANS harmonics folder
runfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex/'
ncfile = '%s/NWS_5kkm_20140101_20140131_3D_ModeAmp.nc'%runfolder
#ncfile = '%s/NWS_5kkm_20140814_20140913_3D_ModeAmp.nc'%runfolder

#ncfile = '%s/TimorSea_15_2k_tri_3D_sponge_20*_AVG_0000.nc'%runfolder
#outpath = 'FIGURES/SUNTANS_InternalTideAmp_%s_K101_Hex5k'
outpath = 'FIGURES/SUNTANS_InternalTideAmp_201401_M2S2_Hex5k'

con =0
mode=0

xlims = [107.5,142.5]
ylims = [-25.0,-5.0]

dx = 0.025


#######

# Output grid
x = np.arange(xlims[0],xlims[1],dx)
y = np.arange(ylims[0],ylims[1],dx)
X,Y = np.meshgrid(x,y)


# Load the suntans object
sun = Spatial(ncfile, projstr='merc',)

# Load the amplitude data
sun.frq = sun.nc.variables['omega'][:]
amp_b = sun.nc.variables['amp_b_re'][:] + 1j * sun.nc.variables['amp_b_im'][:]
sun.nc.variables.keys()

convert=True
if convert:
    # Convert the suntans coordinates to lat/lon
    sun.xp, sun.yp = sun.to_latlon(sun.xp, sun.yp)
    sun.xv, sun.yv = sun.to_latlon(sun.xv, sun.yv)
    sun.xy = sun.cellxy(sun.xp, sun.yp)

### SUNTANS data
#ax = plt.subplot(211)
amp = np.abs(amp_b[con,:,mode])
amp += np.abs(amp_b[con+1,:,mode])

# Interpolate onto a constant grid

print('Interpolating...')
amp_xy = sun.interpolate(amp, X, Y)
amp_xy[amp_xy.mask] = -9999

outfile = '%s.tif'%outpath
print('Saving to raster file %s'%outfile)

save_raster(outfile, amp_xy, x, y)
print('Done')
print('To convert to ascii grid run the following:')
print('\tgdal_translate -of AIIGrid %s %s'%(outfile, outfile[:-3]+'asc'))
