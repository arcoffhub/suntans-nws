"""
Convert the profile binary files into netcdf format
"""

#from distributed import LocalCluster
#cluster = LocalCluster(scheduler_port=8783)

from soda.dataio.suntans.sunprofile import save_profile_nc

import dask
dask.config.set(scheduler='processes')



def main(basedir, outfile,basetime):
    save_profile_nc(basedir, outfile, basetime)


if __name__=='__main__':
    import sys
    #basedir = 'sunhex100m'
    #outfile = '../PROFILES/ScottReef3D_hex100m_Profile.nc'
    basedir = sys.argv[1]
    outfile = sys.argv[2]
    basetime = sys.argv[3]

    #basetime = '20130701.0000'
    print(outfile)
    main(basedir, outfile, basetime)
#
#p = Profile('%s/%s'%(basedir, outfile))
#
#temp = p(5e5, 6e7, 100, 'temp')
