
# coding: utf-8

# # Richardson Number Damping Parameterization Tests
# 
# We propose a KdV dissipation parameterization basedon on the gradient Richardson number $Ri$ and a damping time scale.
# 
# The gradient Richardson number expanded to first-order is
# 
# $$
# Ri = \frac{N^2}{U_z^2} = \frac{N^2}{c_n^2 A_n^2\phi_{zz}^2}
# $$
# 
# A criticism of the weakly nonlinear theory is that solutions can become physically unstable to shear instability. To counteract this we propose that there is an upper bound on the wave amplitude, $A$ based on a critical $Ri$ (usually 0.25); we called this the critical amplitude and is given by
# 
# $$
# A_{cr}^2 =  \frac{N^2}{c_n^2 Ri_{cr}\phi_{zz}^2} = \frac{c^2}{N^2 Ri_{cr} \phi^2}
# $$
# where the last form arises from $\phi_{zz} + N^2/c^2 \phi=0$.
# 
# We propose a damping term into the KdV equation
# 
# $$
# A_t + \alpha A A_x + \beta A_{xxx} =  \frac{\Delta A}{\tau_d}
# $$
# 
# where $\Delta A$ is an operator 
# 
# $$ 
# \Delta A = \max\left[\ A(x,t) - A_{cr},\ 0\right],
# $$
# 
# and $\tau_d$ is a dissipation time scale.
# 
# ## Dissipation Time Scale
# 
# An appropriate dissipation time scale is 
# 
# $$
# \tau_d \sim \frac{A^2}{\nu_v}
# $$
# 
# where $nu_v$ is a vertical eddy viscosity that according to mixing length theory (Odier?) scales like $\nu_v \sim L^2 U_z$. Assuming the amplitude is an appropriate length scale we get
# 
# $$
# \tau_d \sim U_z^{-1} = k U_z^{-1},
# $$
# where $k$ is an order-one empirical coefficient. Our proposed dissipation parameterization is then
# 
# $$
# \frac{\Delta A}{\tau_d} = k c A \phi_{zz} \Delta A = -k \frac{N^2}{c} A \phi \Delta A
# $$
# 
# noting that all expansions of shear are to first-order only.

# In[16]:


import xray
import numpy as np
from datetime import datetime

import matplotlib.pyplot as plt

import mycurrents.oceanmooring as om
from iwaves.utils.density import FitDensity, InterpDensity
from iwaves import IWaveModes
from iwaves import kdv, solve_kdv 
from iwaves.utils.viewer import viewer

get_ipython().magic(u'matplotlib inline')


# In[3]:


def fullsine(x, a_0, L_w, x0=0.):
    
    k = 2*np.pi/L_w
    eta =  - a_0 * np.cos(k*x + k*x0 + np.pi/2)
    eta[x>x0+3*L_w/2] = 0.
    #eta[x<x0-4*L_w/2] = 0.
    eta[x<x0-L_w/2] = 0.

    return eta

def maxamp(mykdv):
    return np.abs(mykdv.B).max()/np.abs(mykdv.a0)


# In[51]:


# Inputs
t0 = datetime(2017,4,3)

rhofile = '/home/suntans/Share/ARCHub/DATA/FIELD/browse-basin-kissme/Data/NetCDF/KISSME_Density_Combined_BestFit_UnevenFilt.nc'


mode=0

kdvargs = dict(    verbose=True,    a0=-30.,    Lw=None,    mode=mode,
    #Cmax=0.8,\
    dt=20.,\
    nu_H=0.0,\
    ekdv=False,\
    wavefunc=fullsine,\
    L_d = 320000,
    Nx = 20000,
    alpha_10=0.0,\
    )

runtime = 1.50*86400.
ntout = 120.
dz = 2.5

outfile = 'SCENARIOS/KISSME_KdV_test.nc'
########



# In[52]:


# Load the density data and intialise the class
dsrho = xray.open_dataset(rhofile)
T = dsrho.rhobar.sel(timeslow=t0, method='nearest').values
z = dsrho.z.values
salt = None

# Use the wrapper class to compute density on a constant grid
#iw = IWaveModes(T, z, salt=salt, density_class=FitDensity, density_func=density_func)
iw = IWaveModes(T[::-1], z[::-1], salt=salt, density_class=InterpDensity)

phi, c1, he, Z = iw(-250,dz,mode)
#phi2, c1a, hea, Z2 = iw2(-250,dz,mode)

# Update the wavelength to represent an internal tide
omega = 2*np.pi/(12.42*3600)
k = omega/iw.c1
Lw = 2*np.pi/k
print Lw
kdvargs['Lw'] = Lw

## Test initialising the kdv class
mykdv0 = kdv.KdVImEx(iw.rhoZ, iw.Z, **kdvargs)
mykdv0.print_params()

print 'dx ', mykdv0.dx_s 

self = mykdv0


# In[53]:


## Call the KdV run function
#mykdv, Bda, output = solve_kdv(iw.rhoZ, iw.Z, runtime,\
#        ntout=ntout, outfile=outfile,\
#        myfunc=maxamp, **kdvargs)


# # Richardson number calculations

# In[59]:



Ricr = 0.1
phi_z = np.gradient(self.phi_1, -self.z)
phi_zz = np.gradient(phi_z, -self.z)
A_ri = self.N2 / (self.c1**2. * Ricr * phi_zz)

# We only want to include stratified regions
N2min = 1e-5
A_cr = np.sqrt(np.max(np.abs(A_ri[self.N2>N2min])))
print A_cr

plt.plot(self.N2, self.z)
#plt.plot(self.z, phi_zz)
#plt.plot(self.z, -self.N2/self.c1**2 *self.phi_1)

#plt.plot(A_ri, self.z)


# In[60]:


# Compute the terms in the scheme
dA = np.abs(self.B) - A_cr
dA[dA<0] = 0.
plt.plot(self.x, dA)



# In[61]:


tau_d = 3600.
nu_term = dA/tau_d


# In[ ]:




