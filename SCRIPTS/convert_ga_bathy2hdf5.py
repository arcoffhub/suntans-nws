"""
Convert different bathy sources into a hdf/netcdf file for easy
use
"""

import os
import pandas as pd
from glob import glob
import numpy as np
import rasterio
from soda.utils.myproj import MyProj

import pdb

######
basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/GA50m_multibeam_xyz/02_xyz'
targetfile = u'*.txt'

hdffile = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/GA_50m_multibeam_bathymetry.h5'
#####

#List the *.adf files
myfiles = glob('%s/%s'%(basedir,targetfile))

# Parse a single file
def parse_xyz(ff):
    fdir, fname = os.path.split(ff)

    print 'Reading file %s...'%fname

    vvs = ['C','D','E','F']
    instr=False
    for vv in vvs:
        if vv in fname:
            instr = True

    if instr == False:
        raise Exception

    group = fname[0:-4]
    utmzone = int(fname[2:4])

    data = pd.read_csv(ff) 
    P = MyProj(None, utmzone = utmzone, isnorth=False)

    lon,lat = P.to_ll(data['x'].values, data['y'].values)

    print '\tBounds: ', lon.min(), lon.max(), lat.min(), lat.max()
    xyzout = pd.DataFrame({
            'X':lon,
            'Y':lat,
            'Z':data['z'].values,
        })

    return xyzout, group

# Open a HDF file
h5 = pd.HDFStore(hdffile, mode='w',
        complevel=9, complib='blosc')

for ff in myfiles:
    try:
        xyzout, fname = parse_xyz(ff)
        print 'Saving to hdf5...'
        #h5[fname] = xyzout
        h5.put(fname, xyzout)
    except:
        'Failed on file %s!!'%ff


# Close the hdf file
print 'Closing HDF file: %s'%hdffile
h5.close()


"""
# Load the data
ff = myfiles[0]
def parse_adf(ff):
    print 'Loading file %s...'%ff

    group = ff.replace(basedir,'').replace(targetfile,'').strip('/')

    ds = rasterio.open(ff)

    # Get the Projection and coordinates
    projstr = ds.crs.to_string()
    P = MyProj(projstr)

    x0, dx,_,y0, _, dy = ds.transform
    nx, ny = ds.width, ds.height




    # Downsample if the grid is too large
    ff = 1
    if (ny > 10000) | (nx > 10000):
        print 'Downsampling large file...'
        ff = 2
        dx*= ff
        dy*= ff

        data = ds.read(1)[::ff,::ff]
        nx = data.shape[1]
        ny = data.shape[0]

    else:
        data = ds.read(1)

    x = np.arange(x0,x0+dx*nx, dx)
    y = np.arange(y0,y0+dy*ny, dy)

    # convert the coordinates
    X,Y = np.meshgrid(x,y)

    print X.shape, Y.shape

    #lon,lat = P.to_ll(X,Y)

    flag = ds.meta['nodata']
    idx = data <= flag

    lon,lat = P.to_ll(X[~idx],Y[~idx])

    xyzout = pd.DataFrame({
            'X':lon,
            'Y':lat,
            'Z':data[~idx],
        })

    return xyzout, group

"""



