"""
Test running a docker container
"""
print(72*'*')
from sfoda.suntans import sunxarray
from sfoda.utils import maptools
import emcee
import os

print('Finished loading modules')
print(72*'*')

print('OMP_NUM_THREADS=',os.environ.get('OMP_NUM_THREADS'))
print(72*'*')

print('Testing multiprocessing modules...')
from multiprocessing import Pool
