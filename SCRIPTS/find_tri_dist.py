def nearest(trid, xpt, ypt, nnear):

    # Find the cell idx
    cidx = trid.find_simplex([xpt,ypt])
    cidx = [cidx]
    nodes = set(trid.simplices[cidx])
    flag=True
    ii = 0
    while flag:
        ii+=1
        if ii > 20:
            
            flag=False
        # Loop through the neighbours
        for cc in cidx:
            neighs = trid.neighbors[cc]
            for ff in neighs:
                if ff == -1:
                    break
                newnodes = trid.simplices[ff]
                for nn in newnodes:
                    if nn not in nodes and nn != -1:
                        nodes.add(nn)

                        if len(nodes) == nnear:
                            flag=False
                            
            cidx = neighs

    return list(nodes)
    
def find_tri_nearest(xyin, xyout, nnear)
    trid = Delaunay(xyin)
    nx = xyout.shape[0]
    idx = np.zeros((nx,nnear), np.int32)
    for ii in range(nx):
        if ii%10000 == 0:
            print ii, nx
        xpt, ypt = xyout[ii,0], xyout[ii,1]
        mynodes = nearest(trid, xpt, ypt, nnear)

        idx[ii,:] = mynodes[0:nnear]
    
    # Compute the distance
    dx = xyout[:,0, np.newaxis] - xyin[idx,0]
    dy = xyout[:,1, np.newaxis] - xyin[idx,1]
    
    dist = np.abs(dx+1j*dy)
    
    return dist, idx
