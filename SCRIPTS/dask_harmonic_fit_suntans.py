
# coding: utf-8

# # Harmonic analysis of "big data" with Dask
# 
# See examples:
# 
#  - [http://matthewrocklin.com/blog/work/2017/01/17/dask-images](http://matthewrocklin.com/blog/work/2017/01/17/dask-images)

# In[1]:


# Use a dask client
from dask.distributed import Client, LocalCluster, worker, progress, wait
import dask
from dask_jobqueue import SLURMCluster

"""
cluster = LocalCluster(
    n_workers=8,
    threads_per_worker=1,
    memory_limit='4GB',
    #memory_spill_fraction=0.1,
    #local_dir='./TMPDATA/'
)
cluster
"""
'''
# Run on zeus
cluster = LocalCluster(
            #"146.118.38.127",
            #'127.0.0.1:37053',
            #workers=8, 
            #ncores=8,
            #threads_per_worker=1, 
            #memory_limit='4GB',
            #local_dir='/home/mrayson/scratch/tmp_dask/',
            #diagnostics_port="8787",
        )
'''



# In[2]:


547/27.

# Start the client
#client = Client(cluster)
client = Client(scheduler_file='/home/mrayson/scheduler.json')
#client = Client() # This asks for the whole node
client
# In[3]:


cluster = SLURMCluster()
client=Client(cluster)
cluster.scale(8)
client


# # Load some data
# 
# Load some unstructured grid data into a Sundask object. Data is written to different files for each processor so dask is used to concatenate back onto the spatial dimension (there is only one for this unstructured data).
# 
# The `loadfull()` method is used to load a variable into a dask.array object.

# In[4]:


#from soda.dataio.suntans.sunxray import Sundask, Sunxray
#from soda.dataio.suntans.sunpy import Spatial
import xarray as xr
import dask
import glob



import matplotlib.pyplot as plt
get_ipython().magic(u'matplotlib inline')


# In[5]:


#ncfiles = '/scratch/pawsey0106/mrayson/SUNTANS/NWS/SCENARIOS/NWS_2km_hex_data2/NWS_2km_hex_20140201.nc.*'
#ncfiles = '/scratch/pawsey0106/mrayson/SUNTANS/NWS/SCENARIOS/NWS_2km_hex_2013_2014_data1/NWS_2km_hex_20130701.*'
ncfiles = '../SCENARIOS/NWS_2km_hex_2013_2014_data1/NWS_2km_hex_20130701.nc.*'


#ds = Sundask(ncfile, chunks=None)
#ds


# In[6]:


## from the __init__ function for sundask

# Get the files from the first file
filenames = sorted(glob.glob(ncfiles))

#for ff in filenames:
#    print ff
# Load all of the files into list as xray objects
myfiles = [xr.open_dataset(url, chunks={'Nk':-1,'Nc':-1,'time':-1})         for url in filenames[0:12]]

myfiles[0]


# In[7]:


## From the loadfull method
varname = 'temp'
axis = -1


# In[8]:


# Load all files
myfiles[0][varname].data
# New attempt at concatenation
#arrays = [sun[varname].data for sun in all_files]
arrays=[]
for ii, data in enumerate(myfiles):
    #print ii
    #arrays.append(data[varname].data[:,0:50,:])
    mydata = data[varname].data
    arrays.append(mydata)


temp = dask.array.concatenate(arrays, axis=axis)
sz = temp.shape
temp = temp.reshape((sz[0],sz[1]*sz[2]))
temp


# In[9]:


# Load just one file
temp = myfiles[0][varname].data
temp

# Attempt to load the data as a single xarray object

ds = xr.open_mfdataset(ncfiles, concat_dim='Nc')
ds
# In[23]:


get_ipython().set_next_input(u'ds = xr.open_mfdataset');get_ipython().magic(u'pinfo xr.open_mfdataset')


# # 2D example
# loadfull loads the variable 'eta' from each file and concatenates along the axis specified by "axis"
eta = sun.loadfull('eta',axis=-1)
eta = dask.array.asarray(eta)

#eta = eta.rechunk((168,1))
eta%%time
etamax = eta.max(axis=-1).persist()progress(etamax)etamax.compute()%%time
etac.max(axis=0).compute()
# # Load a 3D variable

# In[12]:


# Load from the sundask object
#temp = ds.loadfull('temp',axis=-1)

#temp = dask.array.asarray(temp).rechunk([96,1,82164])
#temp[dask.array.isnan(temp)]=0.
#temp = ds.['temp']
#temp = ds._ds['temp']
#temp = temp[:,0,:]
temp


# In[13]:


print('ds size in GB {:0.2f}\n'.format(temp.nbytes / 1e9))

#temp = temp.chunk([temp.shape[0],1,temp.shape[-1]])
temp = temp.rechunk((673, 1, 1000))

temp
#temp.datatemp = temp.chunks# Load the data into memory
stack = client.persist(temp)progress(stack)
# In[11]:


get_ipython().run_cell_magic(u'time', u'', u'# Calculate the spatio-temporal average for all depths\n#tmax = temp.data.max(axis=0)\ntmax = temp.var(axis=0)\n#tmax = stack.var(axis=0)')

%%time

future = tmax.compute()
# In[12]:


get_ipython().run_cell_magic(u'time', u'', u'# This actually performs the operation out of memory (Maybe??)\nfuture = client.persist(tmax)\n#tmax.compute()\n#future = client.compute(tmax)\n#client.compute(future)\n#wait(future)')

temp_big = client.scatter(temp)
temp_big
#tmin = client.submit(dask.array.min, temp_big, axis=0)
def my_min(a, **kwargs):
    return dask.array.min(a,axis=0)tmins = client.map(my_min, temp)
type(tmins)
# In[21]:


big_temp = client.scatter(temp)
tmax = client.submit(my_min, big_temp)


# In[22]:


del(tmax)

# Show progress on single core only
with ProgressBar():
    tmax = eta.max(axis=0).compute()
tmax
# # Harmonic fit code
# 

# In[9]:


import numpy as np
from datetime import datetime
from soda.utils import othertime


# In[10]:


ds = myfiles[0]
dtime = othertime.datetime64todatetime(ds.time.values)
phsbase=datetime(1900,1,1)
axis = 0 

omega = 1.41e-4
frq = [omega, 2*omega, 3*omega]

###
# Convert the dtime to seconds since
t = othertime.SecondsSince(dtime, basetime=phsbase)


# In[11]:


#X = temp.data.reshape((96,100*82164))
X = temp

#X = temp[:,0:2,:].reshape((96,2*82164))
X[dask.array.isnan(X)] = 0.
#X = eta
X, omega, frq



# In[15]:



#t = np.asarray(t)

# Reshape the array sizes
#X = X.swapaxes(0, axis)
sz = X.shape
lenX = int(np.prod(sz[1:]))

#if not len(t) == sz[0]:
#    raise 'length of t (%d) must equal dimension of X (%s)'%(len(t),sz[0])

X = X.reshape((sz[0],lenX))


frq = np.array(frq)
Nfrq = frq.shape[0]


# In[16]:


def build_lhs_dask(t,frq):
    """
    Construct matrix A
    """
    nt=t.shape[0]
    nf=frq.shape[0]
    nff=nf*2+1
    A=np.ones((nt,nff))
    for ff in range(0,nf):
        A[:,ff*2+1]=np.cos(frq[ff]*t)
        A[:,ff*2+2]=np.sin(frq[ff]*t)

    return dask.array.from_array(A, chunks=(A.shape[0],A.shape[1]))

def lstsq_dask(A,y):    
    """    
    Solve the least square problem

    Return:
        the complex amplitude 
        the mean
    """
    N=A.shape[1]
    b,res,rank,s = dask.array.linalg.lstsq(A,y)
    
    #Apr = A.transpose().dot(A)
    #ypr = A.transpose().dot(y)
    #b = dask.array.linalg.solve(Apr, ypr)
    
    return b
    #A = b[0][1::2]
    #B = b[0][2::2]

    #return A+1j*B, b[0][0::N]



def phsamp(C):
    return np.abs(C), np.angle(C)



# Least-squares matrix approach
A = build_lhs_dask(t,frq)
b = lstsq_dask(A,X) # This works on all columns of X!!
#Amp, Phs= phsamp(C)


# reshape the array
#Amp = np.reshape(Amp,(Nfrq,)+sz[1:])
#Phs = np.reshape(Phs,(Nfrq,)+sz[1:])
#C0 = np.reshape(C0,sz[1:])

# Output back along the original axis
# Amplitude, phase, mean
#return Amp.swapaxes(axis,0), Phs.swapaxes(axis,0), C0#C0.swapaxes(axis,0)


# In[17]:


b = b.reshape((Nfrq*2+1,sz[1],sz[2]))
#b = b.reshape((Nfrq*2+1,sz[1]))

# Do the computation and bring it into memory
soln = b.persist()


# In[15]:


# Get the amplitute phase and mean
a0 = soln[0,...]
a_re = soln[1::2,...]
a_im = soln[2::2,...]

a_complex = a_re + 1j*a_im

amp = dask.array.absolute(a_complex)
phs = dask.array.angle(a_complex)

a0.shape

# Now return the solutions as numpy arrays
# return a0.compute(), amp.compute(), phs.compute()


# # Now try it as a function

# In[11]:


def build_lhs_dask(t,frq):
    """
    Construct matrix A
    """
    nt=t.shape[0]
    frq = np.array(frq)
    
    nf=frq.shape[0]
    nff=nf*2+1
    A=np.ones((nt,nff))
    for ff in range(0,nf):
        A[:,ff*2+1]=np.cos(frq[ff]*t)
        A[:,ff*2+2]=np.sin(frq[ff]*t)

    return dask.array.from_array(A, chunks=(A.shape[0],A.shape[1]))

def lstsq_dask(A,y):    
    """    
    Solve the least square problem

    Return:
        the complex amplitude 
        the mean
    """
    N=A.shape[1]
    b,res,rank,s = dask.array.linalg.lstsq(A,y)

    return b

A = build_lhs_dask(t,frq)

def harmonic_fit_dask(X):
    
    print('A: ',A.shape)
    
    # Remove NaNs
    X[dask.array.isnan(X)] = 0.

    # Compute the variance
    #Xvar = X.var(axis=0)
    
    #
    #sz = X.shape
    #lenX = int(np.prod(sz[1:]))

    #if not len(t) == sz[0]:
    #    raise 'length of t (%d) must equal dimension of X (%s)'%(len(t),sz[0])

    #X = X.reshape((sz[0],lenX))
    
    #nfrq = A.shape[1]
    #print('X :', sz, nfrq)




    # Least-squares matrix approach
    b = lstsq_dask(A,X) # This works on all columns of X!!
    print('b', b.shape)
    
    #return Xvar, b#.reshape((Nfrq*2+1,sz[1],sz[2]))
    return b#.reshape((nfrq,sz[1],sz[2]))
    
    


# In[35]:


# Test the function on a single file

Tb = harmonic_fit_dask(temp)
Tb.persist()

#Tvar_out = client.persist(Tvar)
#Tb_out = client.persist(Tb)


# In[20]:


# Try using mapblock
def test_func(X):
    return X[0:7,...]

#out = dask.array.map_blocks(test_func, temp, dtype=temp.dtype,
#            chunks=(7,temp.chunks[1],temp.chunks[2]))

#A = build_lhs_dask(t,frq)
out = dask.array.map_blocks(harmonic_fit_dask, temp, dtype=temp.dtype,
    chunks=(7,temp.chunks[1]))



# In[21]:


Tb = client.persist(out)


# In[21]:


temp.chunks[2]


# In[11]:


# Try calling it in a loop

var_arrays=[]
lsq_arrays=[]
A = build_lhs_dask(t,frq).persist()
for ii, data in enumerate(myfiles):
    #print ii

    Tvar_tmp, Tb_tmp = harmonic_fit_dask(data[varname].data, A)

    var_arrays.append(Tvar_tmp)
    lsq_arrays.append(Tb_tmp)


Tvar = dask.array.concatenate(var_arrays, axis=-1)
Tb = dask.array.concatenate(lsq_arrays, axis=-1)



# In[12]:


#stack = Tb.persist()
stack = client.persist(Tb)
stack


# In[22]:


get_ipython().run_cell_magic(u'time', u'', u"#Tvar.persist().to_hdf5('../SCENARIOS/OUTPUT_NWS_2km_hex/test_lsq.h5', '/temp/var3')")


# In[40]:


Tb.to_hdf5('../SCENARIOS/OUTPUT_NWS_2km_hex/test_lsq.h5', '/temp/b')


# In[20]:


def stack_mf_var(myfiles, varname, axis=-1):
    """
    Returns a stacked dask array of the variable, varname
    from a list of xarray files
    """

    var_arrays = [myf[varname].data for myf in myfiles]
    
    return dask.array.concatenate(var_arrays, axis=axis)
    


# In[21]:


# Prepare the output dataset as a dictionary

# Stack the grid variables

gridkeys = ['xv','yv','dv','cells', 'mnptr'] # grid variables to merge
output_ds = {}
for kk in gridkeys:
    print(kk)
    output_ds.update({kk:stack_mf_var(myfiles, kk, axis=0)})

# add the nodes - these are all stored in each file
output_ds.update({'xp':myfiles[0]['xp'].data})
output_ds.update({'yp':myfiles[0]['yp'].data})

# Create the dimensions and coordinates
dims = ['Nk','Np','numsides']
coords = {}
for dd in dims:
    coords.update({dd:myfiles[0]})

# Just need to update the number of cells
Nc = output_ds['xv'].shape[0]
coords.update({'Nc':Nc})

for kk in gridkeys:
    print(myfiles[0][kk].dims)


# # Use netcdf4 and old library only
# 

# In[10]:


from soda.utils.harmonic_analysis import harmonic_fit
from netCDF4 import Dataset
from dask import delayed
import numpy as np

varname = 'temp'
ds = myfiles[0]
omega = 1.41e-4
frq = [omega, 2*omega, 3*omega]


# In[21]:


@delayed
def fit_nc_var(args):
    ncfile, varname, time, frq = args
    with Dataset(ncfile) as nc:
        print ncfile
        # Load the data

        #print('loading data...')
        X = nc.variables[varname][:]
        X[np.isnan(X)] = 0.
        #Do the fit
        #print('doing filt...')
        xamp, xphs, xmean = harmonic_fit(time, X, frq)
        #print('done')
    
    #return (xamp, xphs, xmean)
    return xamp
    #return dask.array.from_delayed(xamp,
    #          shape=xamp.shape,
    #          dtype=xamp.dtype)
    
amp = []
phs = []
mean = []
for ncfile in filenames[0:144]:
    xamp = fit_nc_var((ncfile, varname, ds.time.values, frq))
    amp.append(xamp)
    #phs.append(xphs)
    #mean.append(xmean)

    
#amp = dask.array.concatenate(amp, axis=-1)


# In[24]:


stack = client.compute(amp)


# In[25]:


get_ipython().run_cell_magic(u'time', u'', u'print( type(stack[0].result()))\n\n# Join all of the arrays\nallamp = np.concatenate([ii.result() for ii in stack], axis=-1)\nallamp.shape')


# In[ ]:


get_ipython().run_cell_magic(u'time', u'', u"# Use multiprocssing\nfrom multiprocessing import Pool\n\ndef fit_nc_var_mp(args):\n    ncfile, varname, time, frq = args\n    with Dataset(ncfile) as nc:\n        print ncfile\n        X = nc.variables[varname][:]\n        X[np.isnan(X)] = 0.\n        #Do the fit\n        #print('doing filt...')\n        xamp, xphs, xmean = harmonic_fit(time, X, frq)\n    \n    return xamp\n\ntasks = [(ncfile, varname, ds.time.values, frq) for ncfile in filenames]\np = Pool(24)\n\namp = p.map(fit_nc_var_mp, tasks)\n")


# In[ ]:


# Join all of the arrays
allamp = np.concatenate([ii for ii in amp], axis=-1)
allamp.shape


# In[ ]:


ii


# In[ ]:




