"""
Plot the results of the energy calculations
"""

from soda.dataio.suntans.sunpy import Spatial
import matplotlib.pyplot as plt

import numpy as np

def quivergrid(sun, u, v, dx, xlims, ylims, **kwargs):
    """
    Create a quiverplot on a regular grid
    """
    # Create the grid
    x0, x1 = xlims[0], xlims[1]
    y0, y1 = ylims[0], ylims[1]
    X,Y = np.meshgrid( np.arange(x0, x1, dx),\
        np.arange(y0,y1, dx))

    # Interpolate
    ug = sun.interpolate(u, X, Y)
    vg = sun.interpolate(v, X, Y)

    qq = plt.quiver(X, Y, ug, vg, **kwargs)

    return qq


########
# Inputs
#ncfile = 'rundata3D/ScottReef3D_Jan2007_Energetics.nc'
#runfolder =  'SCENARIOS/sunhex100m'
#ncfile = '%s/ScottReef3D_hex100_NH_Energetics_M2M4M6.nc'%runfolder

#runfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex'
#ncfile = '%s/NWS_5kkm_20140101_20140131_3D_Energetics.nc'%runfolder
#outfile = 'FIGURES/EnergyFlux_20140101_20140131_NWS_dpi150.jpg'

runfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex'
ncfile = '%s/NWS_5kkm_20140630_20140730_3D_Energetics.nc'%runfolder
outfile = 'FIGURES/EnergyFlux_20140630_20140730_NWS_dpi150.jpg'


#ncfile = '%s/NWS_5kkm_20140131_20140302_3D_Energetics.nc'%runfolder
#outfile = 'FIGURES/EnergyFlux_20140131_20140302_NWS_dpi150.jpg'

conname='M_2'
con=0

#conname='M_4'
#con=1
#Elims=[0,1.5]

depthlevs = [10,100.,200.,300.,400.,500.,1000.,1500.,2000.,2500.]
depthlevs = [10,100.,200.,1000.,]

varname = 'E'
title = r'$%s$ Energy Density [kJ m$^{-2}$]'%conname
cmap = 'afmhot_r'

#varname = 'C'
#scale = 1.0
#Elims = [0,2]
#title = r'$%s$ IW Conversion [W m$^{-2}$]'%conname
#cmap = 'afmhot_r'

#xlims = [3.5e5, 6.5e5]
#ylims = [8.35e6, 8.6e6]

scale = 1e-3
#xlims = [3.5e5*scale, 4.0e5*scale]
#ylims = [8.42e6*scale, 8.48e6*scale]

xlims = [1.2e7*scale, 1.46e7*scale]
ylims = [-2.6e6*scale, -0.75e6*scale]


dxF = 25.0e3 * scale # Spacing of flux vectors


#xlims = [3.65e5, 3.9e5]
#ylims = [8.445e6, 8.455e6]
#
#dxF = 0.5e3 # Spacing of flux vectors
#
#outfile = 'FIGURES/EnergyFlux_M2M4_ScottReef_zoom.png'
#outfile = 'FIGURES/EnergyFlux_M2M4_ScottReef_zoom_dpi150.jpg'

#########

def plot_con(con, Elims, Cscale, Escale, vecscale, **kwargs):
    sun=Spatial(ncfile,variable='HKE',tstep=con)
    # Scale coordinates to m
    sun.xv *=scale
    sun.yv *=scale
    sun.xp *=scale
    sun.yp *=scale
    sun.xy = sun.cellxy(sun.xp, sun.yp)

    if varname == 'E':
        HKE = sun.loadData(variable='HKE')
        APE = sun.loadData(variable='APE')

        data = HKE+APE
    else:
        data = sun.loadData(variable=varname)

    Fu = sun.loadData(variable='Fu')
    Fv = sun.loadData(variable='Fv')
    #data = np.abs(Fu + 1j*Fv) # Flux magnitude
        
    ####
    # plot total energy = APE+HKE
    ####

    #plt.figure(figsize=(10,8))

    sun.clim=Elims
    sun.plot(z=data*Cscale, titlestr='',\
        cmap=cmap)

    #qq = plt.quiver(sun.xv[1::subsample], sun.yv[1::subsample],\
    #        Fu[1::subsample],Fv[1::subsample],\
    #        scale=scale,scale_units='xy', color='0.2')
    qq = quivergrid(sun, Fu, Fv, dxF,xlims,ylims,\
            scale=vecscale,scale_units='xy', color='0.5')

    plt.quiverkey(qq, 0.8, 0.1, Escale, r'[%3.1f kW m$^{-1}$]'%(Escale*1e-3))

    sun.contourf(z=sun.dv,titlestr='',\
            xlims=xlims, ylims=ylims,\
            clevs=depthlevs, filled=False, colors='k', linewidths=0.5)

    plt.xlabel('Easting [km]')
    plt.ylabel('Northing [km]')
    plt.tight_layout()

    #plt.show()
    return sun

####
#
plt.figure(figsize=(10,12))

# M2 scales
Elims=[0,2.0]
vecscale=0.1/scale
Escale = 2e3
Cscale = 1e-3

ax = plt.subplot(211)
sun = plot_con(0, Elims, Cscale, Escale, vecscale, color='0.5')
plt.text(0.9, 0.9, '(a)', transform=ax.transAxes)
sun.cb.ax.set_title('$E_{M2}$ [kJ m$^{-2}$]')

## K1
#Elims=[0,2.0]
#vecscale=0.1/scale
#Escale = 5e3
#Cscale = 1e-3

ax = plt.subplot(212)
sun = plot_con(3, Elims, Cscale, Escale, vecscale,color='0.5')

sun.cb.ax.set_title('$E_{K1}$ [kJ m$^{-2}$]')
plt.text(0.9, 0.9, '(b)', transform=ax.transAxes)

plt.tight_layout()

plt.savefig(outfile, dpi=150)
plt.show()

