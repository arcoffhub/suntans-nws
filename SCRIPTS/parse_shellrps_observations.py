"""
Parse the RPS observations into a suitable NetCDF format
"""

import os
import operator
import xray
import numpy as np 

from datetime import datetime, timedelta

import pdb

RPSvars = {
   'SeaWaterTemp':'watertemp',\
   'Salinity':'salinity',\
   'Pressure':'pressure',\
   #'Qual1':'qcflag',\
   'WaterDepth':'waterlevel',\
   'SensorDepth':'waterlevel',\
   'Height':'waterlevel',\
   'CurEastComp':'water_u',\
   'CurNorthComp':'water_v',\
   }

RPSattrs = {
   'DepthHeight':'height',
   'Longitude':'longitude',
   'Latitude':'latitude',\
}

############
# Functions
############
def process_depth_attr(nc):
    depthstr = 'Depth: %f %s %s'%\
        (nc.DepthHeight, nc.DepthHeight.attrs['units'], nc.DepthHeight.attrs['height_ref'])

    return depthstr

def get_station_name(nc, ctr):
    site = nc.attrs['location']
    site = site.replace(' ','_').replace('-','_')

    depthstr = '%s_%04d'%\
        (site,ctr)
         #int(np.abs(nc[varname].attrs[height])),\
         #nc[varname].attrs[height]['height_ref'])

    return depthstr



def convert_rps_time(time):
    """
    Convert the RPS time into a datetime object

    Returns in UTC time zone
    """
    tzone = time.attrs['timezone']
    dtzone = timedelta(hours=float(tzone))

    #days = map(str, time.values[:,0].astype(int))
    tint = time.values[:,0].astype(int)
    days= tint.astype(str)
    times = [datetime.strptime(xx, '%Y%m%d')-dtzone for xx in days]

    msec = time.values[:,1].tolist()
    dt = [timedelta(milliseconds=float(mm)) for mm in msec]

    return np.array([t+Dt for t,Dt in zip(times,dt)]).astype('<M8[ns]') # datetime64 object

def calculate_qc_mask(Q1, qg, lower, upper):
    shift = 2**(2*qg)
    idx = Q1 < 0.0
    
    hibit = np.zeros_like(Q1)
    Q1[idx] = Q1[idx] + 2**31
    hibit[idx] = 1

    qual = np.mod( np.floor(Q1/shift), 4)

    if qg == 15:
        qual[idx] = qual[idx]+2.

    mask = operator.or_(qual < lower, qual > upper)

    return mask


def convert_rps_variable(nc, varname, newvarname):
    """
    Convert a 1-D time variable 
    """
    print '\tConverting variable: %s -> %s...'%(varname, newvarname)

    timeout = convert_rps_time(nc.Time)

    # Calculate the QC flag
    qg = nc[varname].attrs['quality_group']
    Q1 = nc['Qual1'].values.astype(np.double)

    # Matt's advice was to use flag = 1 or 2
    mask = calculate_qc_mask(Q1, qg, 1, 2) 

    if np.any(mask):
        print 'Bad values found - masking'

    nc[varname][mask] = np.nan

    V = xray.DataArray( nc[varname].values, \
            dims=('time',),\
            name=newvarname,\
            attrs = nc[varname].attrs,\
            coords = {'time':timeout}
    )

    return V



def process_rps_file(ncfile, outfile, ctr):
    print 72*'#'
    print 'Processing file: %s'%ncfile

    nc = xray.open_dataset(ncfile)

    # Create an output dataset
    ncout = xray.Dataset(attrs=nc.attrs)


    # Convert the data variables and insert them into the output object
    for varname in RPSvars.keys():
        #if varname in nc.data_vars.keys():
        #print nc.data_vars.keys()
        if varname in nc.variables.keys():
            newvarname = RPSvars[varname]
            V = convert_rps_variable(nc, varname, newvarname)
            ncout.update({newvarname: V})


    # Put the attribute variables into the Dataset as well
    for attr in ['DepthHeight','Latitude','Longitude']:#RPSattrs.keys():
        if attr in nc.variables.keys():
            #if attr in nc.data_vars.keys():
            #print attr, nc.variables.keys()
            print attr
            ncout.update({attr:nc[attr]})

    # Get the station name
    name = get_station_name(nc,ctr)

    ncout.attrs.update({'stationname':name})
    ncout.attrs.update({'original_file':ncfile})

    print 'Done\n'
    return nc, ncout

####
# Input variables
#basedir = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellPreludeRPS/NetCDF'
#outfile = '%s/RPS_Shell_Prelude_20072010.nc'%basedir

basedir = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellCrux'
outfile = '%s/RPS_Shell_Crux_20162017.nc'%basedir

#basedir = '/home/suntans/Share/ARCHub/DATA/FIELD/ShellExmouth/'
#outfile = '%s/RPS_Shell_ExmouthPlateau.nc'%basedir

mode = 'w'
###
# Step 1: List all of the netcdf files under the base directory
###
ncfiles = []
for root, dirs, files in os.walk(basedir):
    for file in files:
        if file.endswith(".nc"):
            ncfiles.append(os.path.join(root, file))

#print ncfiles

###
# Step 2: Load each file and convert to my format
#
#       - Change variable names
#       - Convert time
###
for ctr, ncfile in enumerate(ncfiles):

    try:
        nc, ncout = process_rps_file(ncfile, outfile, ctr)
        # Save the data set to a group in the outfile
        name = ncout.attrs['stationname']
        print '\tSaving to group: %s'%name


        #if os.path.exists(outfile):
        #    mode = 'a'
        #else:
        #    mode = 'w'

        print mode

        ncout.to_netcdf(path=outfile, mode=mode,\
            format='NETCDF4', group=name)

        ncout.close()
        mode = 'a'

    except:
        print 'File: %s failed!\n'%ncfile
        #ncout.close()



