
# coding: utf-8

# # Convert the IMOS *.mat files into something useful...

# In[32]:


from scipy.io import loadmat
import numpy as np
import os

import matplotlib.pyplot as plt

from soda.utils.othertime import datenum2datetime
from mycurrents.oceanmooring import OceanMooring as om


# In[41]:


def parse_imos_mat(matfile):

    print 'Reading file: %s...'%matfile

    # Load the matfile
    data = loadmat(matfile)['T']

    # Derive a stationname and ID from the filename
    parts = matfile.split('/')
    stationname = '%s_%s_%s'%(parts[-5],parts[-3],parts[-2])
    stationid = '%s_%s'%(parts[-3],parts[-2])
    print stationname, stationid


    # Convert to numpy arrays
    time = data['tim'][0][0]
    temp = data['T'][0][0]
    Z = data['depth'][0][0]

    # Convert the time and put into an OceanMooring class
    t = datenum2datetime(time.ravel())

    T = om(t, temp, Z.ravel(), StationName=stationname, StationID=stationid,
        varname='watertemp',long_name='Seawater Temperature', units='degC')

    return T


def parse_all_groups(station, outfile, mode):

    ###
    # List all of the matfile files under the base directory
    ###
    matfiles = []
    for root, dirs, files in os.walk(basedir):
        for file in files:
            if file.endswith(".mat"):
                tmpfile = os.path.join(root, file)
                if station in tmpfile:
                    matfiles.append(tmpfile)

    print matfiles

    for ii, matfile in enumerate(matfiles):
        group = '%s_%02d'%(station,ii)
        
        T = parse_imos_mat(matfile)

        print T.to_xray()
        print 'Writing to group: ',group
        T.to_netcdf(outfile, group=group, mode=mode)
        mode='a'


####
# Input
basedir = r'/home/suntans/Share/NWS/DATA/FIELD/2014-2016 IMOS NWS data/'
stations = ['P200_CT', 'K200_CT']
outfile = r'/home/suntans/Share/NWS/DATA/FIELD/2014-2016 IMOS NWS data/IMOS_KIMPIL200_T.nc'
mode='w'

for station in stations:
    parse_all_groups(station, outfile, mode)
    mode='a'

print 'Done'


