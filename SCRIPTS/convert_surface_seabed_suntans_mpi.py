"""
Extract surface data from suntans output
"""

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

from soda.dataio.suntans.sunpy import Grid, Spatial
from soda.dataio.suntans.suntans_ugrid import ugrid

from copy import deepcopy
from glob import glob

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    print('MPI running: rank = %d, ncores = %d'%(rank,size))
    verbose=True
else:
    verbose=False


def get_nearbedvar(phi,Nk):
    """
    Compute the seabed value at k - (1+1/2)
    """
    zeros = np.zeros_like(Nk)
    klower = np.array([zeros,Nk-1]).max(axis=0)
    kupper = np.array([zeros,Nk-2]).max(axis=0)
    j = range(Nk.shape[0])
    return 0.5*(phi[klower,j] + phi[kupper,j])
 

def convert_seabed(ncfile, outfile):

    # Load the suntans object
    sun = Spatial(ncfile, klayer=[-99],_FillValue=999999, VERBOSE=verbose )

    # Keep a copy of these for later
    Nk = deepcopy(sun.Nk)
    Nkmax = deepcopy(sun.Nkmax)


    # Create a netcdf file
    varlist = ['uc','vc','temp','eta']
                
    # Create the output file 
    sun.Nk = np.zeros((sun.Nc,),dtype=np.int32)
    #sun.dv = np.ones((sun.Nc,),dtype=np.int32)
    sun.Nkmax = 1
    sun.z_r = 0.
    sun.z_w = [0,1.]
    sun.dz = 0.


    sun.writeNC(outfile)

    # Reset the Nk and Nkmax attributes
    sun.Nk = Nk
    sun.Nkmax = Nkmax
            
    # Create the output variables
    dims = ('time', 'Nc')
    attrs = {'vc':\
            {'long_name':'northward velocity',\
            'coordinates':'time Nc',\
            'units':'m s-1'},\
            'uc':\
            {'long_name':'eastward velocity',\
            'coordinates':'time Nc',\
            'units':'m s-1'},\
            'temp':\
            {'long_name':'surface temperature',\
            'coordinates':'time Nc',\
            'units':'degC'},\
            'eta':\
            {'long_name':'free-surface height',\
            'coordinates':'time Nc',\
            'units':'m'},\
        }

            
    for vv in varlist:
        if verbose:
            print( 'Creating variable: %s'%vv)
        sun.create_nc_var(outfile, vv,\
            dims,\
            attrs[vv],\
        )
            #dtype=ugrid[vv]['dtype'],\
            #zlib=ugrid[vv]['zlib'],\
            #complevel=ugrid[vv]['complevel'],\
            #fill_value=ugrid[vv]['fill_value'])
            
    sun.create_nc_var(outfile,'time',\
        ugrid['time']['dimensions'],\
        ugrid['time']['attributes'])

    # Create the solution
    #T = np.arange(tlim[0],tlim[1]+dt,dt)
    T = sun.timeraw
    nt = T.shape[0]

    nc = Dataset(outfile,'a')

    ## Do it in one hit
    #nc.variables['time'][:] = T 
    #sun.tstep = range(0,nt)
    #for vv in varlist:
    #    print('Loading variable %s...'%vv)
    #    data = sun.loadData(variable=vv)
    #    nc.variables[vv][:] = data

    ## Do it step-by-step
    for ii in range(nt):
        if verbose:
            print('Writing step: %d of %d...'%(ii,nt))
        #u, v = double_gyre(sun.xv, grd.yv, T[ii], eps, omega, A)
        sun.tstep = [ii]
        sun.klayer = [-99]
        u = sun.loadData(variable='uc')
        v = sun.loadData(variable='vc')
        temp = sun.loadData(variable='temp')
        eta = sun.loadData(variable='eta')
        #if verbose:
        #    print('u shape = ',u.shape)

        # Interpolate onto nodes
        #un = sun.cell2node(u)
        #vn = sun.cell2node(v)

        # Write the output
        nc.variables['time'][ii] = T[ii] 
        nc.variables['uc'][ii,...] = get_nearbedvar(u,Nk)
        nc.variables['vc'][ii,...] = get_nearbedvar(v,Nk)
        nc.variables['temp'][ii,...] = get_nearbedvar(temp,Nk)
        nc.variables['eta'][ii,...] = eta

    if verbose:
        print('Done!!')

    nc.close()

############
outfolder = '/scratch/pawsey0106/mrayson/SUNTANS/NWS/SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014'
numfiles = 144

for my_node in range(rank, numfiles, size): # Breaks up the list for MPI

    fn = my_node

    # Need to go through folder by folder...
    for ii in range(1,13):
        ncfile = glob('SCENARIOS/NWS_2km_hex_2013_2014_data%d/NWS_2km_hex_*.nc.%d'%(ii,fn))
        if rank==0:
            print( '\n', 72*'#','\n')
            #print(ncfile)

        #outfile = '%s/NWS_2km_2013_2014_Seabed.nc.%d'%(outfolder, fn)
        outfile = '%s.bed'%ncfile[0]
        
        convert_seabed(ncfile[0], outfile)



"""
if __name__=='__main__':
    #:########
    #:# Inputs

    #:#runfolder =  'SCENARIOS/TimorSea_15_5k_tri_3D_data1'
    #:#ncfile = '%s/TimorSea_15_5k_tri_3D_20090801_0*.nc'%runfolder
    #:#outfile = '%s/TimorSea_15_5k_200908_type3_surface.nc'%runfolder

    #:runfolder =  'SCENARIOS/TimorSea_15_2k_tri_3D_data1'
    #:ncfile = '%s/TimorSea_15_2k_tri_3D_20090801_0*.nc'%runfolder # Forgot to change the filename
    #:outfile = '%s/TimorSea_15_2k_200908_type3_surface.nc'%runfolder # Forgot to change the filename

    #:########

    import sys

    runfolder = sys.argv[1]
    ncfile = sys.argv[2]
    outfile = sys.argv[3]

    convert_surface(runfolder, '%s/%s'%(runfolder,ncfile), '%s/%s'%(runfolder, outfile))

"""
