"""
Convert ERA file to a SUNTANS netdf
"""

#from soda.dataio.datadownload.get_metocean_dap import get_metocean_local
from soda.dataio.suntans.metfile import SunMet, metfile
from soda.utils.maptools import ll2utm
from soda.utils import myairsea
from soda.utils.myproj import MyProj
from soda.utils.othertime import datetime64todatetime

import matplotlib.pyplot as plt

from datetime import datetime
import numpy as np
import xarray as xr

#######
datadir = '/group/pawsey0106/mrayson/DATA/ATMOSPHERE/ECMWF_NWS/'
ncfile = '{}/ECMWF_ROMS_forcing_NWS_2019.nc'.format(datadir)
#ncfile = 'MET/ECMWF_ROMS_forcing_NWS_2019.nc'
maskfile = '{}/land_mask.nc'.format(datadir)
metfile = 'MET/ECMWF_SUNTANS_NWS_2019_merc.nc'

projstr='merc'
utmzone = 51
isnorth = False
########

# Load the landmask
ds = xr.open_dataset(maskfile)
mask = ds.lsm == 0
mask = mask.squeeze()

nc = xr.open_dataset(ncfile)

# Convert the coordinates
X,Y = nc.lon.values,nc.lat.values
#ll = np.vstack([X.ravel(),Y.ravel()]).T
#xy = ll2utm(ll,utmzone,north=isnorth)
P = MyProj(projstr, utmzone=utmzone, isnorth=isnorth)
#P = MyProj(projstr,)
xpt, ypt = P(X[mask], Y[mask])

#xpt = xy[:,0]
#ypt = xy[:,1]

zpt = 10.0*np.ones_like(xpt)

nxy = xpt.shape[0]
nt = nc.time.shape[0]

# Get the time in string format
time = datetime64todatetime(nc.time.values)
dt = time[1]-time[0]
starttime = datetime.strftime(time[0],'%Y%m%d.%H%M%S')
#endtime = datetime.strftime(nc.time[-1]+dt,'%Y%m%d.%H%M%S')
endtime = datetime.strftime(time[-1],'%Y%m%d.%H%M%S')
# Load into a metfile
met = SunMet(xpt,ypt,zpt,(starttime,endtime,dt.total_seconds() ))

    
# Fill the arrays
met.Uwind.data[:] = nc['Uwind'].values[:,mask]
met.Vwind.data[:] = nc['Vwind'].values[:,mask]
met.Tair.data[:] = nc['Tair'].values[:,mask]
met.Pair.data[:] = nc['Pair'].values[:,mask]
met.RH.data[:] = nc['Qair'].values[:,mask]
met.cloud.data[:] = nc['cloud'].values[:,mask]
met.rain.data[:] = nc['rain'].values[:,mask]

met.write2NC(metfile)
print('Done')
