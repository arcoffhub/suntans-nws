"""
Plot the 3D dem data using mayavi
"""


import matplotlib as mpl
mpl.use('Qt5Agg')

import matplotlib.pyplot as plt
import numpy as np
from soda.dataio.conversion.dem import DEM, blendDEMs
#from mayavi import mlab
#from soda.dataio.suntans.suntvtk import SunTvtk

#############

#basedir = u'/home/suntans/Share/ARCHub/DATA/BATHYMETRY/'
#infile = '%s/Woodside_NWS_250m_DEM.nc'%basedir


#basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'
basedir = '/home/mrayson/group/mrayson/DATA/BATHYMETRY/'
#infile = '%s/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir
#infile = '%s/Browse_Blended_250m_DEM.nc'%basedir
#infile = '%s/Browse_GeoOzMultibeam_WEL_Blended_250m_DEM.nc'%basedir
#infile = '%s/GA_Browse_MultiBeam_250m_DEM.nc'%basedir
#infile = '%s/GA_Browse_SingleBeam_250m_DEM.nc'%basedir
#infile = '%s/WEL_Browse_Composite_250m_DEM.nc'%basedir
#infile = '%s/GA_WEL_Multi_Merged_w_GA250_250m_DEM.nc'%basedir # old with GA multibeam
#infile ='%s/GA_WEL_NoGAMultibeam_w_GA250_250m_DEM.nc'%basedir # New data
#infile ='%s/GEBCO_WEL_Browse_250m_DEM.nc'%basedir 
#infile = '%s/GA_WEL_NWS_250m_DEM.nc'%basedir
#infile = '%s/dem_GA2009_NWS.nc'%basedir
#infile = '%s/dem_GEBCO_NWS.nc'%basedir
#infile = '%s/GA_Multibeam_NWS_250m_DEM.nc'%basedir
#infile = '%s/WEL_Multibeam_Browse_250m_DEM.nc'%basedir
#infile = '%s/WEL_Multibeam_NWS_250m_DEM.nc'%basedir
#infile = '%s/GA_WEL_MultiBeam_NWSBrowse_DEM.nc'%basedir #
#infile = '%s/NGDC_Singlebeam_NWS_250m_DEM.nc'%basedir
#infile = '%s/GA_WEL_NWS_250m_DEM.nc'%basedir
#infile = '%s/NGDC_GA_WEL_MultiBeam_NWSBrowse_DEM.nc'%basedir
#infile = '%s/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir
infile = '%s/TimorSea_GAWEL_Multi_GEBCO_Combined_DEM.nc'%basedir


vmin = -2500
vmax = -1000

ff=10
#warp = 100.
warp = 0.001

# Scott Reef
x0 = 121.5
x1 = 124.5
y0 = -14.5
y1 = -12.0

## Rowley Shals
#x0 = 118.5
#x1 = 120.5
#y0 = -18.0
#y1 = -14.8

bbox = [x0, x1, y0, y1]

########

#sunfile = 'SCENARIOS/NWS_test/TimorSea_IC.nc'
#
#sun = SunTvtk(sunfile)
#
#sun.plotbathy3d()

# 3D plot of the DEM
dem1 = DEM(infile, meshgrid=True, utmzone=50, isnorth=False)
#dem1.clip(bbox[0], bbox[1], bbox[2], bbox[3])
#dem1.to_ll()

print( dem1.x0, dem1.x1, dem1.y0, dem1.y1)

print( 'Plotting...')
dem1.plot(ve=5.00, vmin=vmin, vmax=vmax, shading=True)
##dem1.contourf(vv=np.arange(-1000,100,10), cmap='gist_earth')
plt.show()
#mlab.imshow(dem1.Z, colormap='gist_earth', vmin=-2000, vmax=-100)#, extent=[dem1.x0, dem1.x1, dem1.y0, dem1.y1, 0., 0.])

# Surface plot
#fig=mlab.figure(bgcolor=(0.,0.,0.),size=(800,700))
#mlab.surf(dem1.x[::ff], dem1.y[::ff], dem1.Z[::ff,::ff].T, colormap='gist_earth',\
#        warp_scale=warp, vmin=vmin, vmax=vmax)
#
#fig.scene.z_plus_view()
#del dem1
#mlab.show()
