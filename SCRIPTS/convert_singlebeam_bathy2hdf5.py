"""
Convert different bathy sources into a hdf/netcdf file for easy
use
"""

import os
import pandas as pd
from glob import glob
import numpy as np
import rasterio
from soda.utils.myproj import MyProj

import pdb

######
basedir = u'/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'
targetfile = u'*.m77t'

hdffile = 'NGDC_SingleBeam_NWS_raw_bathymetry.h5'

#List the *.m77 files
myfiles = glob('%s/NGDC_SingleBeam_all/MGD77_610361/%s'%(basedir,targetfile))
#####


# Grab the directory names for the 
for ff in myfiles:
    print ff.replace(basedir,'').replace(targetfile,'').strip('/')

# Open a HDF file
outfile = '%s/%s'%(basedir,hdffile)
h5 = pd.HDFStore(outfile, mode='w',
        complevel=9, complib='blosc')


# Process the GA files
def parse_mgd77(datafile):
    fname, ext = os.path.splitext(os.path.basename(datafile)) 
    fname = fname.replace('-','')
    fname = fname.replace('_','')

    print 'Reading %s...'%datafile
    xyz = pd.read_csv(datafile, sep='\t')

    idx = xyz['CORR_DEPTH'].notnull()
    xyzout = pd.DataFrame({
            'X':xyz['LON'][idx],
            'Y':xyz['LAT'][idx],
            'Z':xyz['CORR_DEPTH'][idx],
            })

    return xyzout, '%s_m77t'%fname


for ga_file in myfiles:
    
    xyzout, fname = parse_mgd77(ga_file)
    print 'Saving to hdf5...'
    h5[fname] = xyzout

# Close the hdf file
print 'Closing HDF file: %s'%outfile
h5.close()




"""
######
basedir = '/home/suntans/Share/Projects/ScottReef/DATA/BATHYMETRY'

ga_files = [\
    'trackline-item-54058/MGD77_986808/ga-0175.m77t',\
    'trackline-item-54060/MGD77_263534/ga-0119.m77t',\
    'trackline-item-54078/MGD77_058091/ga-0168.m77t',\
    ]

wel_files = [\
    'ScottReef2010Composition_WGS84.xyz'
    ]

hdffile = 'BrowseRawBathymetry_xyz.h5'
####

# Open a HDF file
outfile = '%s/%s'%(basedir,hdffile)
h5 = pd.HDFStore(outfile, mode='a',
        complevel=9, complib='blosc')

####
## Process the woodside xyz data
####
datafile = '%s/%s'%(basedir,wel_files[0])
fname, ext = os.path.splitext(os.path.basename(datafile)) 

print 'Reading %s...'%datafile
xyz = pd.read_csv(datafile, names=['X','Y','Z'])

print 'Saving to hdf5...'
h5[fname] = xyz

# Process the GA files
def parse_mgd77(datafile):
    fname, ext = os.path.splitext(os.path.basename(datafile)) 
    fname = fname.replace('-','_')

    print 'Reading %s...'%datafile
    xyz = pd.read_csv(datafile, sep='\t')

    idx = xyz['CORR_DEPTH'].notnull()
    xyzout = pd.dataframe({
            'x':xyz['lon'][idx],
            'y':xyz['lat'][idx],
            'z':xyz['corr_depth'][idx],
            })

    return xyzout, fname


for ga_file in ga_files:
    datafile = '%s/%s'%(basedir,ga_file)
    xyzout, fname = parse_mgd77(datafile)
    print 'Saving to hdf5...'
    h5[fname] = xyzout

# Close the hdf file
print 'Closing HDF file: %s'%outfile
h5.close()
"""
