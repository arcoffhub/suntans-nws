"""
Extract surface data from suntans output
"""

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

from soda.dataio.suntans.sunpy import Grid, Spatial
from soda.dataio.suntans.suntans_ugrid import ugrid


def convert_surface(runfolder, ncfile, outfile):

    # Load the suntans object
    sun = Spatial(ncfile, K=[0],_FillValue=999999 )

    # Create a netcdf file
    varlist = ['uc','vc','temp','eta']
                
    # Create the output file 
    sun.Nk = np.zeros((sun.Nc,),dtype=np.int32)
    #sun.dv = np.ones((sun.Nc,),dtype=np.int32)
    sun.Nkmax = 1
    sun.z_r = 0.
    sun.z_w = [0,1.]
    sun.dz = 0.

    print sun.dv.max()

    sun.writeNC(outfile)
            
    # Create the output variables
    dims = ('time', 'Nc')
    attrs = {'vc':\
            {'long_name':'northward velocity',\
            'coordinates':'time Nc',\
            'units':'m s-1'},\
            'uc':\
            {'long_name':'eastward velocity',\
            'coordinates':'time Nc',\
            'units':'m s-1'},\
            'temp':\
            {'long_name':'surface temperature',\
            'coordinates':'time Nc',\
            'units':'degC'},\
            'eta':\
            {'long_name':'free-surface height',\
            'coordinates':'time Nc',\
            'units':'m'},\
        }

            
    for vv in varlist:
        print 'Creating variable: %s'%vv
        sun.create_nc_var(outfile, vv,\
            dims,\
            attrs[vv],\
        )
            #dtype=ugrid[vv]['dtype'],\
            #zlib=ugrid[vv]['zlib'],\
            #complevel=ugrid[vv]['complevel'],\
            #fill_value=ugrid[vv]['fill_value'])
            
    sun.create_nc_var(outfile,'time',\
        ugrid['time']['dimensions'],\
        ugrid['time']['attributes'])

    # Create the solution
    #T = np.arange(tlim[0],tlim[1]+dt,dt)
    T = sun.timeraw
    nt = T.shape[0]

    nc = Dataset(outfile,'a')

    ## Do it in one hit
    nc.variables['time'][:] = T 
    sun.tstep = range(0,nt)
    for vv in varlist:
        print 'Loading variable %s...'%vv
        data = sun.loadData(variable=vv)
        nc.variables[vv][:] = data

    ### Do it step-by-step
    #for ii in range(nt):
    #    print 'Writing step: %d of %d...'%(ii,nt)
    #    #u, v = double_gyre(sun.xv, grd.yv, T[ii], eps, omega, A)
    #    sun.tstep = [ii]
    #    u = sun.loadData(variable='uc')
    #    v = sun.loadData(variable='vc')
    #    temp = sun.loadData(variable='temp')
    #    eta = sun.loadData(variable='eta')

    #    # Interpolate onto nodes
    #    #un = sun.cell2node(u)
    #    #vn = sun.cell2node(v)

    #    # Write the output
    #    nc.variables['time'][ii] = T[ii] 
    #    nc.variables['uc'][ii,...] = u
    #    nc.variables['vc'][ii,...] = v
    #    nc.variables['temp'][ii,...] = temp
    #    nc.variables['eta'][ii,...] = eta

    print 'Done!!'

    nc.close()

if __name__=='__main__':
    #:########
    #:# Inputs

    #:#runfolder =  'SCENARIOS/TimorSea_15_5k_tri_3D_data1'
    #:#ncfile = '%s/TimorSea_15_5k_tri_3D_20090801_0*.nc'%runfolder
    #:#outfile = '%s/TimorSea_15_5k_200908_type3_surface.nc'%runfolder

    #:runfolder =  'SCENARIOS/TimorSea_15_2k_tri_3D_data1'
    #:ncfile = '%s/TimorSea_15_2k_tri_3D_20090801_0*.nc'%runfolder # Forgot to change the filename
    #:outfile = '%s/TimorSea_15_2k_200908_type3_surface.nc'%runfolder # Forgot to change the filename

    #:########

    import sys

    runfolder = sys.argv[1]
    ncfile = sys.argv[2]
    outfile = sys.argv[3]

    convert_surface(runfolder, '%s/%s'%(runfolder,ncfile), '%s/%s'%(runfolder, outfile))

