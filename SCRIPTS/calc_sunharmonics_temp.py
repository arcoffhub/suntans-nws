"""
Perform a harmonic analysis on SUNTANS output
"""

from soda.dataio.suntans.suntides import suntides
from datetime import datetime

###
# Input variables

# 5k run
runfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex/'
ncfile = '%s/NWS_5km_hex_20*_surface.nc'%runfolder # Forgot to change the filename
outfile = '%s/NWS_5km_hex_temp_Harmonics.nc'%runfolder



#frqnames = ['M2','S2','N2','K1','M4','O1','M6','MK3','S4','MN4','NU2','S6','MU2','2N','O0','LAM2','S1','M1','J1','MM','SSA','SA','MSF','MF','RHO1','Q1','T2','R2','2Q','P1','2SM','M3','L2','2MK3','K2','M8','MS4']
#frqnames = ['M2','S2','N2','K1','O1']
frqnames = ['S1','MSF','MM','120d','SSA','SA']

#varnames = ['eta','ubar','vbar','uc','vc','rho','temp']
varnames = ['temp']

tstart='20140101.0000'
tend='20150101.0000'
#tstart=datetime(2009,8,9)
#tend=datetime(2009,9,9)

###
sun=suntides(ncfile,frqnames=frqnames)
sun(tstart,tend,varnames=varnames)
sun.tides2nc(outfile)
