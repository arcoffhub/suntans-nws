"""
Calculate the vertical grid spacing in the seabed cell
"""

from soda.dataio.suntans import sunpy
from soda.dataio.suntans.sunprofile import Profile

from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt

def get_nearbedvar(phi,Nk):
     """
     Compute the seabed value at k - (1+1/2)
     by averaging the bottom two layers
     """
     zeros = np.zeros_like(Nk)
     klower = np.array([zeros,Nk-1]).max(axis=0)
     kupper = np.array([zeros,Nk-2]).max(axis=0)

     j = range(Nk.shape[0])

     return 0.5*(phi[klower,j] + phi[kupper,j])


def extract_seabed_dz(ncfile, outfile):
    sun = sunpy.Spatial(ncfile, klayer=[-99])


    ###
    # Calculate the seabed layer thickness
    ###
    z = np.cumsum(sun.dz)
    #Z = z[:,np.newaxis] * np.ones((1,sun.Nc))
    zeros = np.zeros_like(sun.Nk)
    klower = np.array([zeros,sun.Nk-1]).max(axis=0)

    dzbot = sun.dv - z[klower]  


    ######
    # Save the output
    ######
    print('Writing to outfile {}'.format(outfile))
    sun.writeNC(outfile)

    sun.create_nc_var(outfile, 'dzbed', ('Nc',), \
            {'long_name':'seabed layer thickness', 'units':'m'}
    )

    nc = Dataset(outfile, 'a')

    nc.variables['dzbed'][:] = dzbot
    nc.close()

    print('Done')


#####################
#ncfile = 'rundata_200701/ScottReef3D_200701_0050.nc'
basedir = '/scratch/pawsey0106/mrayson/SUNTANS/NWS/SCENARIOS/NWS_2km_hex_2013_2014_data1'
filestr = 'NWS_2km_hex_20130701'
outfilestr = 'NWS_2km_hex_seabed_thickness'
nfiles = 144
#####################

for ii in range(nfiles):
    print('Loading dz for file {}...'.format(ii))
    ncfile = '{}/{}.nc.{}'.format(basedir,filestr,ii)
    outfile = '{}/{}.nc.{}'.format(basedir, outfilestr, ii)

    extract_seabed_dz(ncfile, outfile)
