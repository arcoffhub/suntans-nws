import numpy as np
from soda.utils.fkriging import kriging as fkrig
from soda.utils.kriging import kriging as krig


N = 10
xyin = np.random.uniform(size=(40,2))
zin = np.random.uniform(size=(40,))
xyout = np.random.uniform(size=(100,2))

F = fkrig(xyin, xyout)

zout = F(zin)

F2 = krig(xyin, xyout)
zout2 = F2(zin)

print np.linalg.norm(zout-zout2)


