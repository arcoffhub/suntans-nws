"""
Convert different bathy sources into a hdf/netcdf file for easy
use
"""

import os
import pandas as pd
from glob import glob
import numpy as np
import rasterio
from soda.utils.myproj import MyProj

import pdb

######
basedir = u'/home/suntans/Share/ARCHub/DATA/BATHYMETRY/Woodside_Bathymetry_to2011'
targetfile = u'w001001.adf'

hdffile = 'WEL_NWS_raw_bathymetry.h5'

#####

#List the *.adf files
myfiles = glob('%s/*/%s'%(basedir,targetfile))

# Grab the directory names for the 
for ff in myfiles:
    print ff.replace(basedir,'').replace(targetfile,'').strip('/')

# Load the data
ff = myfiles[0]
def parse_adf(ff):
    print 'Loading file %s...'%ff

    group = ff.replace(basedir,'').replace(targetfile,'').strip('/')

    ds = rasterio.open(ff)

    # Get the Projection and coordinates
    projstr = ds.crs.to_string()
    P = MyProj(projstr)

    x0, dx,_,y0, _, dy = ds.transform
    nx, ny = ds.width, ds.height




    # Downsample if the grid is too large
    ff = 1
    if (ny > 10000) | (nx > 10000):
        print 'Downsampling large file...'
        ff = 2
        dx*= ff
        dy*= ff

        data = ds.read(1)[::ff,::ff]
        nx = data.shape[1]
        ny = data.shape[0]

    else:
        data = ds.read(1)

    x = np.arange(x0,x0+dx*nx, dx)
    y = np.arange(y0,y0+dy*ny, dy)

    # convert the coordinates
    X,Y = np.meshgrid(x,y)

    print X.shape, Y.shape

    #lon,lat = P.to_ll(X,Y)

    flag = ds.meta['nodata']
    idx = data <= flag

    lon,lat = P.to_ll(X[~idx],Y[~idx])

    xyzout = pd.DataFrame({
            'X':lon,
            'Y':lat,
            'Z':data[~idx],
        })

    return xyzout, group

# Open a HDF file
outfile = '%s/%s'%(basedir,hdffile)
h5 = pd.HDFStore(outfile, mode='w',
        complevel=9, complib='blosc')

for ff in myfiles:

    xyzout, fname = parse_adf(ff)
    #try:
    #except:
    #    'Failed on file %s!!'%ff

    print 'Saving to hdf5...'
    h5[fname] = xyzout

# Close the hdf file
print 'Closing HDF file: %s'%outfile
h5.close()



"""
######
basedir = '/home/suntans/Share/Projects/ScottReef/DATA/BATHYMETRY'

ga_files = [\
    'trackline-item-54058/MGD77_986808/ga-0175.m77t',\
    'trackline-item-54060/MGD77_263534/ga-0119.m77t',\
    'trackline-item-54078/MGD77_058091/ga-0168.m77t',\
    ]

wel_files = [\
    'ScottReef2010Composition_WGS84.xyz'
    ]

hdffile = 'BrowseRawBathymetry_xyz.h5'
####

# Open a HDF file
outfile = '%s/%s'%(basedir,hdffile)
h5 = pd.HDFStore(outfile, mode='a',
        complevel=9, complib='blosc')

####
## Process the woodside xyz data
####
datafile = '%s/%s'%(basedir,wel_files[0])
fname, ext = os.path.splitext(os.path.basename(datafile)) 

print 'Reading %s...'%datafile
xyz = pd.read_csv(datafile, names=['X','Y','Z'])

print 'Saving to hdf5...'
h5[fname] = xyz

# Process the GA files
def parse_mgd77(datafile):
    fname, ext = os.path.splitext(os.path.basename(datafile)) 
    fname = fname.replace('-','_')

    print 'Reading %s...'%datafile
    xyz = pd.read_csv(datafile, sep='\t')

    idx = xyz['CORR_DEPTH'].notnull()
    xyzout = pd.dataframe({
            'x':xyz['lon'][idx],
            'y':xyz['lat'][idx],
            'z':xyz['corr_depth'][idx],
            })

    return xyzout, fname


for ga_file in ga_files:
    datafile = '%s/%s'%(basedir,ga_file)
    xyzout, fname = parse_mgd77(datafile)
    print 'Saving to hdf5...'
    h5[fname] = xyzout

# Close the hdf file
print 'Closing HDF file: %s'%outfile
h5.close()
"""
