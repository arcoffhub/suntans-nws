"""
Calculates the dual of a grid
"""

import matplotlib.pyplot as plt
import numpy as np
import os


from soda.dataio.suntans.sunpy import Grid
from soda.dataio.ugrid.hybridgrid import HybridGrid


        
###

ncfile = 'GRIDS/NWS_15_2k'

outpath= 'GRIDS/NWS_15_2k_hex'

try:
    os.mkdir(outpath)
except:
    print 'Path exists.'

sun = Grid(ncfile)
sun.neigh = sun.neigh.astype(int)

# Load the data into a hybrid grid class
grd = HybridGrid(sun.xp,sun.yp,sun.cells,nfaces=sun.nfaces)

grd.create_dual_grid(minfaces=5,outpath=outpath)


sun = Grid(outpath)

plt.figure()
sun.plotmesh(facecolors='none',linewidths=0.2)
plt.plot(sun.xv,sun.yv,'m.')


plt.figure()
sun.plothist()
plt.show()

