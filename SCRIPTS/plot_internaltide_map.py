"""
Extract the OTPS tides over the domain
"""

from mpl_toolkits.basemap import Basemap
import numpy as np
import xray
import pandas as pd
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from datetime import datetime

from soda.utils.otherplot import axcolorbar

from soda.utils import othertime
#import cmocean
from soda.utils.maptools import plotmap
from soda.dataio.suntans.sunpy import Spatial

import pdb

######

# SUNTANS harmonics folder
runfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex/'
#ncfile = '%s/NWS_5kkm_20140101_20140131_3D_ModeAmp.nc'%runfolder
ncfile = '%s/NWS_5kkm_20140814_20140913_3D_ModeAmp.nc'%runfolder

#ncfile = '%s/TimorSea_15_2k_tri_3D_sponge_20*_AVG_0000.nc'%runfolder
#outpath = 'FIGURES/SUNTANS_InternalTideAmp_%s_K101_Hex5k'
outpath = 'FIGURES/SUNTANS_InternalTideAmp_201408_%s_M2S2_Hex5k'

con =0
mode=0


#cmap = cmocean.cm.amp
#cmap = 'Spectral_r'
#cmap = 'afmhot_r'
#cmap = 'gist_ncar'
cmap = 'magma_r'

# Depth plotting
basedir = r'/home/suntans/Share/ScottReef/DATA'
#basedir = r'/group/pawsey0106/mrayson/DATA'

bathyfile = r'%s/BATHYMETRY/ETOPO1/ETOPO1_Bed.nc'%basedir
#bathylevs= [-1000.,-100.]
bathylevs= [100.,1000.]

#figfile = '../../FIGURES/TimorSea_SUNTANS_TideComparison'

xlims = [107.5,142.5]
ylims = [-25.0,-5.0]

clevs = np.arange(0,52.5,2.5)

ss = 6 # subsample interval for vectors
scalefac = 25.
dxvec = 0.6 #  degrees

klayer = [0]

dx = 0.025

#coastfile = r'%s/COAST/GSHHS_shp/i/GSHHS_i_L1.shp'%basedir

#######

# open the bathy
dsz = xray.open_dataset(bathyfile)

# Bathymetry plotting function
def plot_bathy(ax):
    
    #xlims = [da.lon.min(),da.lon.max()]
    #ylims = [da.lat.min(),da.lat.max()]
    z = dsz.topo.sel(lon=slice(xlims[0],xlims[1]), lat=slice(ylims[1],ylims[0]))
    #plt.contour(z.lon.values, z.lat.values, z.values,\
    #    bathylevs,
    #    linewidths=0.3,
    #    linestyles='-', colors='k')
    clevs = np.arange(-6000,200,100)
    x,y = np.meshgrid(z.lon.values, z.lat.values)
    ax.contour(z.lon.values, z.lat.values, z.values,\
        bathylevs,
        linewidths=0.3,
        linestyles='-', colors='k',
        latlon=True)

    return C



#da = xray.open_dataset(ncfile)

#xlims = [da.lon.min(),da.lon.max()]
#xlims = [112., 135.]
#ylims = [da.lat.min(),da.lat.max()]
#X, Y = np.meshgrid(da.lon.values, da.lat.values)
x = np.arange(xlims[0],xlims[1],dx)
y = np.arange(ylims[0],ylims[1],dx)
X,Y = np.meshgrid(x,y)

def plot_map(ncfile, outpath):

    # Load the suntans object
    sun = Spatial(ncfile, projstr='merc',)

    # Load the amplitude data
    sun.frq = sun.nc.variables['omega'][:]
    amp_b = sun.nc.variables['amp_b_re'][:] + 1j * sun.nc.variables['amp_b_im'][:]
    sun.nc.variables.keys()



    ######
    # Plot it up
    ######
    fig = plt.figure(figsize=(12,8))

    ax = Basemap(projection='merc',\
            llcrnrlat=ylims[0],\
            urcrnrlat=ylims[1],\
            llcrnrlon=xlims[0],\
            urcrnrlon=xlims[1],\
            lat_0=ylims[0],\
            lon_0=xlims[0],\
                resolution='i')

    convert=True
    if convert:
        # Convert the suntans coordinates to lat/lon
        sun.xp, sun.yp = sun.to_latlon(sun.xp, sun.yp)
        sun.xv, sun.yv = sun.to_latlon(sun.xv, sun.yv)
        # Convert latlon to map coordinate
        sun.xp, sun.yp = ax(sun.xp, sun.yp)
        sun.xv, sun.yv = ax(sun.xv, sun.yv)
        sun.xy = sun.cellxy(sun.xp, sun.yp)



    ### SUNTANS data
    #ax = plt.subplot(211)
    temp = np.abs(amp_b[con,:,mode])
    temp += np.abs(amp_b[con+1,:,mode])

    #sun.plot(z=temp, xlims=xlims, ylims=ylims, cmap=cmap)
    #plt.colorbar(sun.patches)
    cf = sun.contourf(z=temp, clevs=clevs, xlims=xlims, ylims=ylims,\
            colorbar=False, cmap=cmap,\
            extend='max', titlestr='')



    # Plot the bathymetry
    cf2 = sun.contourf(z=sun.dv, clevs=bathylevs,\
            xlims=xlims, ylims=ylims,\
            colorbar=False, filled=False, \
            colors='k', linewidths=0.5,\
            titlestr='',
        )



    #plt.ylabel('Latitude [$^{\circ}$N]')
    #plt.xlabel('Longitude [$^{\circ}$E]')
    ##plt.text(0.7,0.1,'(a) SUNTANS $\eta_{%s}$'%con, transform=ax.transAxes)

    plt.title('')


    # Put this last to reset the axis
    ax.drawcoastlines()
    ax.fillcontinents()
    parallels = np.arange(np.ceil(ylims[0]), np.floor(ylims[1]),4.)
    meridians = np.arange(np.ceil(xlims[0]), np.floor(xlims[1]),4.)

    ax.drawparallels(parallels,labels=[1,0,0,0], linewidth=0.2, dashes=[1,0])
    ax.drawmeridians(meridians,labels=[0,0,0,1],rotation=17, linewidth=0.2, dashes=[1,0])
    ax.drawmapboundary(fill_color='w')
    ax.drawmapscale(128,-20, 130, -19.5, 500,barstyle='fancy',fontsize=9)

    cb=axcolorbar(cf, pos=[0.4,0.15,0.3,0.04])
    cb.ax.set_title('Internal Tide Amplitude [m]')

    plt.tight_layout(pad=3.)

    outfile = outpath%('mode%d'%(mode+1))
    plt.savefig(outfile+'.png', dpi=150)
    plt.savefig(outfile+'.pdf', dpi=150)

    print( 'Figure saved to %s.'%outfile)
    plt.show()
    print( 12*'#')


plot_map(ncfile, outpath)


