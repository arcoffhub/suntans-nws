from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

vectorsize=144

if rank == 0:
    print('MPI running: rank = %d, ncores = %d'%(rank,size))
    print('Done')
    my_elements = range(rank, vectorsize,size)
    for ii,n in enumerate(my_elements):
        print(ii,my_elements)
