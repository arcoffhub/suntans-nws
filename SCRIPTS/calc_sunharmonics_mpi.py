"""
Perform a harmonic analysis on SUNTANS output
"""

from soda.dataio.suntans.suntides import suntides
from datetime import datetime

import pandas as pd
from glob import glob
from natsort import natsorted

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


if rank == 0:
    print('MPI running: rank = %d, ncores = %d'%(rank,size))
    verbose=True
else:
    verbose=False

###
# Input variables
res='2'

outfolder = 'SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2013_2014/'
scenario = 'NWS_2km_GLORYS_hex_Nk80dt60_2013_2014'
numfiles = 144
times = pd.date_range('2013-07-01','2014-07-01',freq='15D')

#outfolder = 'SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/'
##scenario = 'NWS_2km_hex_2013_2014'
#scenario = None
#infolder = 'SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/NETCDF4-CLASSIC'
#numfiles = 144
#times = pd.date_range('2013-07-01','2014-06-16',freq='15D')

#outfolder = 'SCENARIOS/OUTPUT_NWS_2km_hex_2019/'
#scenario = 'NWS_2km_hex_2019'
#numfiles = 8*24
#times = pd.date_range('2019-02-01','2019-06-01',freq='15D')

# Use glob to get the files from multiple directories

#frqnames = ['M2','S2','N2','K1','M4','O1','M6','MK3','S4','MN4','NU2','S6','MU2','2N','O0','LAM2','S1','M1','J1','MM','SSA','SA','MSF','MF','RHO1','Q1','T2','R2','2Q','P1','2SM','M3','L2','2MK3','K2','M8','MS4']
frqnames = ['M2','S2','N2','K1','O1']
varnames = ['eta', 'uc', 'vc','w','rho','temp']

#frqnames = ['M2','S2','K1','O1','MSF','MM']
#varnames = ['temp']

#varnames = ['eta','ubar','vbar','uc','vc','rho','temp']



###

#times = pd.date_range('2013-07-15','2013-07-19',freq='1D')
#times = pd.date_range('2013-08-01','2014-05-01',freq='15D')
#times = pd.date_range('2014-01-01','2014-05-01',freq='15D')


t1s = times[:-2]
t2s = times[2:]
for t1, t2 in zip(t1s[:], t2s[:]):
    for my_node in range(rank, numfiles, size): # Breaks up the list for MPI

        fn = my_node

        if scenario is None:
            ncfile = glob('%s/NWS_2km_*.nc.%d'%(infolder,fn))
        else:
            ncfile = glob('SCENARIOS/%s_data*/NWS_2km_*.nc.%d'%(scenario,fn))
        if rank==0:
            print(ncfile)

        if rank==0:
            print( '\n', 72*'#','\n')
            print( t1, t2)
        
        tstart = t1.strftime('%Y%m%d.%H%M')
        tend = t2.strftime('%Y%m%d.%H%M')

        outfile = '%s/NWS_%skm_%s_%s_3D_Harmonics.nc.%d'%(outfolder,res,\
            t1.strftime('%Y%m%d'), t2.strftime('%Y%m%d'), fn)

        sun=suntides(natsorted(ncfile),frqnames=frqnames, fit_all_k=True,\
                verbose=verbose,_FillValue=999999)

        sun(tstart, tend, varnames=varnames)
        sun.tides2nc(outfile)

        # Clear the memory
        del sun
        sun=None
    
    # Wait for the other nodes before moving on...
    print('Waiting for the other nodes...')
    comm.barrier()
