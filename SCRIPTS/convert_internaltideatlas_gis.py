"""
Convert the internal tide amp to GIS format
"""

#from mpl_toolkits.basemap import Basemap
import numpy as np
import xarray as xr
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from datetime import datetime

from soda.utils.maptools import save_raster

import pdb

######

# SUNTANS harmonics folder
infile = 'SCENARIOS/OUTPUTS_2km_hex_2013_2014/NWS_internal_tide_atlas.nc'

outpath = 'GIS/SUNTANS_InternalTideAmp_%s_M2S2_Hex3k.tif'

con =0
mode=0


#######

# Load the suntans object
sun = xr.open_dataset(infile)

x = sun.longitude.values
y = sun.latitude.values

for tt, time in enumerate(sun.time):
    amp_xy = sun.amp_b[tt,con,mode,...].values
    amp_xy += sun.amp_b[tt,con+1,mode,...].values

    outfile = outpath%(str(time.values)[0:7])	
    print(outfile)

    
    print('Saving to raster file %s'%outfile)
    
    save_raster(outfile, amp_xy, x, y)

    print('Done')
    print('To convert to ascii grid run the following:')
    print('\tgdal_translate -of AIIGrid %s %s'%(outfile, outfile[:-3]+'asc'))
