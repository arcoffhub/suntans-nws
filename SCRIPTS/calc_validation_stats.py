"""
Calculate the validation statistics for the mooring temp and velocity
"""

import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import xray
from datetime import datetime
import pandas as pd
import os

import soda.utils.mysignal as sp
from soda.utils.timeseries import timeseries
from soda.dataio.suntans.sunprofile import Profile
from soda.utils.maptools import ll2utm
from soda.utils.modvsobs import ModVsObs
from soda.dataio import netcdfio
import soda.dataio as io

#from octant.tools import isoslice
from mycurrents import oceanmooring as om

from matplotlib import rcParams

rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Bitstream Vera Sans']
rcParams['font.serif'] = ['Bitstream Vera Sans']
rcParams["font.size"] = "14"
rcParams['axes.labelsize']='large'

def main(scenario):
    print 'Extracting scenario %s...'%scenario
    ##########
    # Inputs
    ##########

    # 400 m station details
    stationT400 = 'SCR400_TP'
    stationUV = 'SCR400_RDI75'
    stationUV2 = 'SCR400_RDI300'

    # 200 m station details
    stationT200 = 'SCR200_TP'
    stationUV200 = 'SCR200_RDI150'

    basedir='/home/suntans/Share/ScottReef/DATA/FIELD'
    Tfile = '%s/FALKOR/ProcessedData/FK150410_Gridded_Mooring_New_TP.nc'%basedir
    UVfile = '%s/FALKOR/ProcessedData/FK150410_RDIADCP.nc'%basedir
    Tfile40 = '%s/FALKOR/ProcessedData/FK150410_Gridded_Mooring_SBE56.nc'%basedir

    # Base case
    #scenario = 'ScottReef3D_tri250m'

    # Comparison case
    #scenario = 'ScottReef3D_Hex100m_NH_Sponge' # THis is in the paper as it stands...
    #plotdir = '../FIGURES/OceanModFigs/'

    #scenario = 'ScottReef3D_Hex100m_wh_hyd' # Hydrostatic w/ HYCOM (probably use this one)
    #plotdir = '../FIGURES/OceanModFigs/'

    #scenario = 'ScottReef3D_Hex100m_wh_nh' # NonHydrostatic w/ HYCOM 
    #plotdir = '../FIGURES/OceanModFigs_nh/'

    #scenario = 'ScottReef3D_Hex100m_Hyd'
    #scenario = 'ScottReef3D_Hex100m_NH_wh'
    #plotdir = '../FIGURES/OceanModFigs/'

    # CARS examples
    #scenario = 'ScottReef3D_Hex100m_wh_CARS'
    #plotdir = '../FIGURES/OceanModFigs_CARS/'

    # BRAM example
    #scenario = 'ScottReef3D_Hex100m_wh_BRAN2016'
    #plotdir = '../FIGURES/OceanModFigs_BRAN2016/'

    #scenario = 'ScottReef3D_Hex100m_wh_CARSbias_v2'
    #plotdir = '../FIGURES/OceanModFigs_CARSbias/'

    #
    #scenario = 'ScottReef3D_Hex100m_wh_CARSbias_zo004'
    #plotdir = '../FIGURES/OceanModFigs_CARSbias_highdrag/'

    #scenario = 'ScottReef3D_Hex100m_NH_NoTurb_Sponge'
    #scenario = 'ScottReef3D_hex100m'

    #scenario = 'ScottReef3D_tri100m_Hyd'
    #scenario = 'ScottReef3D_tri100m_NH'

    # SUNTANS profile file
    sunfile = 'PROFILES/%s_Profile.nc'%scenario

    utmzone = 51
    isnorth = False

    #t1 = datetime(2015,4,17)
    #t2 = datetime(2015,4,21)

    # Full mooring period
    t1 = datetime(2015,4,12).strftime('%Y-%m-%d')
    t2 = datetime(2015,4,27).strftime('%Y-%m-%d')


    #####
    # database details
    basedir = '/home/suntans/Share/ScottReef/DATA/FIELD'
    dbfile = '%s/ScottReef_AllObs.db'%basedir
    # End Inputs
    #############

    print 'Loading the suntans profile...'
    sunTS = Profile(sunfile)

    ##########
    # Load the 400 and 200 m velocity and temperature data
    ##########
    print 'Loading mooring data...'

    Tobs400 = om.from_netcdf(Tfile, 'Temperature', group=stationT400)
    Tobs200 = om.from_netcdf(Tfile, 'Temperature', group=stationT200)

    # Get the station UTM location
    XY400 = ll2utm(np.array([Tobs400.X,\
                Tobs400.Y]), utmzone, north=isnorth)
    XY200 = ll2utm(np.array([Tobs200.X,\
                Tobs200.Y]), utmzone, north=isnorth)

    ### Velocity data

    # 75 kHz Data
    UV75_xray = xray.open_dataset(UVfile, group=stationUV)

    U400a = om.OceanMooring(UV75_xray.time.values, UV75_xray['u'].values, \
        -UV75_xray['zhat'].values, zvar=-UV75_xray['zhat'].values)

    # 300 kHz Data
    UV300_xray = xray.open_dataset(UVfile, group=stationUV2)

    U400b = om.OceanMooring(UV300_xray.time.values, UV300_xray['u'].values, \
        -UV300_xray['zhat'].values, zvar=-UV300_xray['zhat'].values)

    # 150 kHz Data (200 m mooring)
    UV150_xray = xray.open_dataset(UVfile, group=stationUV200)

    U200a = om.OceanMooring(UV150_xray.time.values, UV150_xray['u'].values, \
        -UV150_xray['zhat'].values, zvar=-UV150_xray['zhat'].values)

    ###########
    # Interpolate the U and T data onto constant depth levels
    ###########

    print 'Interpolating station data onto constant depths...'

    def interp_station(Uobs, zlevs, Uobs_z=None):
        """
        General function for stacking data
        """
        # Stack the data
        for zz in zlevs:
            if Uobs_z is None:
                Uobs_z = om.OceanMooring(Uobs.t, Uobs.interp_z(zz), zz)
            else:
                Utmp = om.OceanMooring(Uobs.t, Uobs.interp_z(zz), zz)
                Uobs_z.vstack(Utmp)
        
        return Uobs_z

    ### 400m UV
    # Extract the velocity at different depth levels for direct model comparison

    zlevs = np.arange(-375,-175.,25.)
    Uobs400_z = interp_station(U400a, zlevs)
    zlevs = np.arange(-175,-25,25.)
    Uobs400_z = interp_station(U400b, zlevs, Uobs_z=Uobs400_z)

    ### Temp
    Tobs400_z = interp_station(Tobs400, Uobs400_z.Z)

    ### 200 m data
    zlevs = np.arange(-190,-20.,20.)
    Uobs200_z =  interp_station(U200a, zlevs )
    Tobs200_z =  interp_station(Tobs200, zlevs )



    ##########
    # Create the ModVsObs objects
    ##########
    def get_suntans_mo(XY, Uobs, sunTS, sunvar, units):
        """
        Get the suntans evuivalent points
        """
        Umod_xray = sunTS(XY[0,0], XY[0,1], -Uobs.Z, sunvar)
        # Convert SUNTANS timeseries to an ocean mooring object
        Umod = om.OceanMooring(Umod_xray.time.values, Umod_xray.values.squeeze(),\
            Uobs.Z, zinterp='linear')

        return ModVsObs(Umod.t, Umod.y, Uobs.t, Uobs.y, \
                Z=Uobs.Z, units=units, varname=sunvar)

    print 'Grabbing suntans points...'
    mo_T200 = get_suntans_mo(XY200, Tobs200_z, sunTS, 'temp', 'degC')
    mo_T400 = get_suntans_mo(XY400, Tobs400_z, sunTS, 'temp', 'degC')

    mo_U200 = get_suntans_mo(XY200, Uobs200_z, sunTS, 'uc', 'm/s')
    mo_U400 = get_suntans_mo(XY400, Uobs400_z, sunTS, 'uc', 'm/s')

    return mo_T200.clip(t1,t2), mo_T400.clip(t1,t2), \
        mo_U200.clip(t1,t2), mo_U400.clip(t1,t2), 

##########
# Run all scenarios
##########
scenarios = [\
     'ScottReef3D_Hex100m_wh_hyd',\
     'ScottReef3D_Hex100m_wh_CARS',\
     'ScottReef3D_Hex100m_wh_BRAN2016',\
     'ScottReef3D_Hex100m_wh_CARSbias_v2',\
     'ScottReef3D_Hex100m_wh_CARSbias_zo004',\
     ]

outfile = 'DATA/ScottReef_SUNTANS_Intercomparison.nc'
mode='w'

for scenario in scenarios:
    T200, T400, U200, U400 = main(scenario)

    T200.to_netcdf(outfile, ncgroup='%s_T200'%scenario, mode=mode)
    mode = 'a'
    T400.to_netcdf(outfile, ncgroup='%s_T400'%scenario, mode=mode)
    U200.to_netcdf(outfile, ncgroup='%s_U200'%scenario, mode=mode)
    U400.to_netcdf(outfile, ncgroup='%s_U400'%scenario, mode=mode)


