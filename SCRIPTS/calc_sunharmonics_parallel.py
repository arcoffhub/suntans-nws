"""
Perform a harmonic analysis on SUNTANS output
"""

from soda.dataio.suntans.suntides import suntides
from datetime import datetime

import pandas as pd
from glob import glob

###
# Input variables
res='2'

#tstart='20090802.0000'
#tend='20090902.0000'

outfolder = 'SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/'

# Use glob to get the files from multiple directories
numfiles = 144

#frqnames = ['M2','S2','N2','K1','M4','O1','M6','MK3','S4','MN4','NU2','S6','MU2','2N','O0','LAM2','S1','M1','J1','MM','SSA','SA','MSF','MF','RHO1','Q1','T2','R2','2Q','P1','2SM','M3','L2','2MK3','K2','M8','MS4']
frqnames = ['M2','S2','N2','K1','O1']
varnames = ['eta', 'uc', 'vc','w','rho','temp']

#frqnames = ['M2','S2','K1','O1','MSF','MM']
#varnames = ['temp']

#varnames = ['eta','ubar','vbar','uc','vc','rho','temp']

###

times = pd.date_range('2013-07-01','2014-05-01',freq='15D')

istart = 0
ii=-1

t1s = times[:-2]
t2s = times[2:]
for t1, t2 in zip(t1s[:], t2s[:]):
    for fn in range(numfiles):


        ncfile = glob('SCENARIOS/NWS_2km_hex_2013_2014_data*/NWS_2km_hex_*.nc.%d'%fn)
        print(ncfile)
        ii+=1
        if ii < istart:
            continue

        print( '\n', 72*'#','\n')
        print( t1, t2)
        tstart = t1.strftime('%Y%m%d.%H%M')
        tend = t2.strftime('%Y%m%d.%H%M')
        outfile = '%s/NWS_%skm_%s_%s_3D_Harmonics.nc.%d'%(outfolder,res,\
            t1.strftime('%Y%m%d'), t2.strftime('%Y%m%d'), fn)

        sun=suntides(sorted(ncfile),frqnames=frqnames, fit_all_k=True)
        sun(tstart, tend, varnames=varnames)
        sun.tides2nc(outfile)

        # Clear the memory
        del sun
        sun=None
