"""
Merge two datasets together by filling in the gaps in one
using data from another grid and itself
"""
from soda.dataio.conversion.demBuilder import demBuilder
from soda.dataio.conversion.dem import blendDEMs, DEM
from soda.utils.maptools import Contour2Shp, utm2ll
from soda.dataio.suntans.sunpy import Grid
from soda.utils.interpXYZ  import interpXYZ
from soda.utils.myproj import MyProj

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import spatial
from scipy.ndimage import filters
import copy
import operator

####
#
basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY'

# This is the file with the gaps
#infile1 = '%s/Browse_GeoOzMultibeam_WEL_Blended_250m_DEM.nc'%basedir
infile1 = '%s/WEL_Browse_Composite_250m_DEM.nc'%basedir

# This is the "background" data
#infile2 ='%s/GeoScience_Woodside_Browse_250m_DEM.nc'%basedir 
#infile2= '%s/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir
#infile2 = '%s/dem_GA2009_NWS.nc'%basedir
infile2 = '%s/dem_GEBCO_NWS.nc'%basedir

nth = 17 # Use only every nth point

bufferdist = 0.65e4 # buffer distance to interpolate in to
zcutoff = -100.

#outfile ='%s/GA_WEL_NoGAMultibeam_w_GA250_250m_DEM.nc'%basedir 
outfile ='%s/GEBCO_WEL_Browse_250m_DEM.nc'%basedir 

bbox = [118.67, 125.76, -18.34, -11.5]
niter = 501 # Smoothing iterations
###

# Load the data
dem1 = DEM(infile1, utmzone=51, isnorth=False)
dem_bg = DEM(infile2,)

####
# Project the first grid to lat/lon
#dem1.to_ll()
dem_bg.clip(bbox[0], bbox[1], bbox[2], bbox[3])

## Clip the background DEM
P = MyProj(None, utmzone=51, isnorth=False)
#dem_bg.clip(dem1.x0,dem1.x1,dem1.y0,dem1.y1)
#dem_bg.regrid(dem1.x, dem1.y, kind='linear')
#dem1.regrid(dem_bg.x, dem_bg.y, kind='linear')

# Convert them both back to UTM
dem_bg.to_xy(P)
#dem_bg.regrid(dem1.x, dem1.y, kind='linear')
#dem1.to_xy(P)
dem1.regrid(dem_bg.X, dem_bg.Y, )

###

M,N = dem1.X.shape

###
# Nan out some regions of the wel data
dem1.Z[operator.and_(dem1.Z>=0.,dem1.X < 2e5) ] = np.nan
# Find the gaps - these are all of the background points within x-distance
idx = ~np.isnan(dem1.Z)
        
## Test the filter out

###
# Find the buffer zone

print 'Finding the buffer zone...'
#idx = ~np.isnan(dem1.Z)
XY_wel = np.column_stack([dem1.X[idx], dem1.Y[idx]])

# compute the spatial tree
kd = spatial.cKDTree(XY_wel)                                                            

# Distance of the GA data to WEL points
print 'spatial querying...'
XY_ga = np.column_stack([dem_bg.X.ravel(), dem_bg.Y.ravel()])
dist, ind = kd.query(XY_ga)

# These are the points to blank out
#idx_ga = operator.or_(dist < bufferdist, dem_bg.Z.ravel() > zcutoff )
idx_buffer = dist < bufferdist
idx_buffer = np.reshape(idx_buffer,(M,N))

## We want to keep points shallow than a certain depth
#idx_z = dem_bg.Z > zcutoff
#idx_ga[idx_z] = False # Unmark these points for masking

# Remove the interior points
#idx_buffer[idx] = False

# Re-fill in the background dem with the filtered points
dem_f = copy.deepcopy(dem_bg)
dem_f.Z[np.isnan(dem_f.Z)]=0.

print 'Filtering the buffer zone...'
for ii in range(niter):
    print ii
    #dem_f.Z = filters.gaussian_filter(dem_f.Z, 4)
    #dem_f.Z = filters.generic_filter(dem_f.Z, np.mean, size=(3,3))
    dem_f.Z = filters.uniform_filter(dem_f.Z, size=(3,3 ))
    #dem_f.Z = filters.laplace(dem_f.Z)
    # Re-fill the non buffer  region
    dem_f.Z[~idx_buffer] = dem_bg.Z[~idx_buffer]
    dem_f.Z[idx] = dem1.Z[idx]
#dem_f.Z[idx] = dem1.Z[idx]

#dem_bg.Z[idx_buffer] = dem_f.Z[idx_buffer]
#dem_bg.Z[idx] = dem1.Z[idx]

dem_f.savenc(outfile=outfile)

#clevs = np.arange(-700,10,10.)
#dem_f.contourf(cmap = 'gist_earth',vv=clevs,extend='both')
dem_f.plot(cmap='gist_earth')
plt.show()


'''
## These are the points we want to interpolate on to
XY_i = np.column_stack([dem1.X[idx_ga], dem1.Y[idx_ga]])

###
# Create a new dem that has gaps
dem_gaps = copy.deepcopy(dem_bg)

# Fill in with GA outside of the "exclusion" zone
dem_gaps.Z[~idx_ga] = dem_bg.Z[~idx_ga]
# Fill in with woodside data where it exists
dem_gaps.Z[idx] = dem1.Z[idx]
dem_gaps.Z[idx_ga] = np.nan

dem_gaps.contourf(vv=30);plt.show()

XYin = np.column_stack([dem_gaps.X[~idx_ga][::nth], dem_gaps.Y[~idx_ga][::nth]])
Zin = dem_gaps.Z[~idx_ga][::nth]

XYout = np.column_stack([dem_gaps.X[idx_ga], dem_gaps.Y[idx_ga]])

###
# Build an interpolation object for the non-gap points
print 'Building interpolant...'
F = interpXYZ(XYin, XYout,
        method = 'curvmin',\
        #method = 'kriging',\
        #method = 'idw',\
        maxdist=np.inf,\
        NNear = 6,
        #NNear = 3,\
        p = 1.0,\
        varmodel = 'spherical',\
        nugget = 0.1,\
        sill = 0.8,\
        vrange = 15000.0,\
)

###
# Interpolate
print 'Interpolating...'
Zout = F(Zin)

####
## Put the results into the output dataset
dem_gaps.Z[idx_ga] = Zout

## Nan-out non-index regions
dem_gaps.Z[~idx_ga]= np.nan

# Put the original woodside data back over the top
dem_gaps.Z[idx] = dem1.Z[idx]

dem_gaps.savenc(outfile=outfile)

clevs = np.arange(-700,10,10.)
dem_gaps.contourf(cmap = 'gist_earth',vv=clevs,extend='both')
plt.show()
'''
