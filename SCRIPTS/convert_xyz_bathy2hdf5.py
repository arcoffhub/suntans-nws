"""
Convert different bathy sources into a hdf/netcdf file for easy
use
"""

import os
import pandas as pd

from soda.utils.myproj import MyProj

######
basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY'


#ga_files = [\
#    'trackline-item-54058/MGD77_986808/ga-0175.m77t',\
#    'trackline-item-54060/MGD77_263534/ga-0119.m77t',\
#    'trackline-item-54078/MGD77_058091/ga-0168.m77t',\
#    ]

basedir2 = '/home/suntans/Share/ARCHub/DATA/BATHYMETRY/OffshoreRowleyShoals_JPP'
wel_files = [\
    '%s/ScottReef2010Composition_WGS84.xyz'%basedir,
    '%s/BRO063_SAND_SEARCH_BATHY.xyz'%basedir2,
    '%s/CURT3D_TR1TR2_BATHY.xyz'%basedir2
    ]

projs = [
    None,
    MyProj('+proj=utm +zone=50 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'),
    MyProj('+proj=utm +zone=50 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'),
    ]

hdffile = 'BrowseRawBathymetry_xyz.h5'
####

# Open a HDF file
outfile = '%s/%s'%(basedir,hdffile)
h5 = pd.HDFStore(outfile, mode='w',
        complevel=9, complib='blosc')

####
## Process the woodside xyz data
####
for P, wel_file in zip(projs, wel_files):
    datafile = wel_file
    fname, ext = os.path.splitext(os.path.basename(datafile)) 

    print 'Reading %s...'%datafile
    xyz = pd.read_csv(datafile, names=['X','Y','Z'])

    if P is not None:
        print 'Reprojecting...'
        lon,lat = P.to_ll(xyz['X'].values, xyz['Y'].values)
        xyz['X'][:] = lon
        xyz['Y'][:] = lat

    print 'Saving to hdf5...'
    h5.put(fname, xyz)

## Process the GA files
#def parse_mgd77(datafile):
#    fname, ext = os.path.splitext(os.path.basename(datafile)) 
#    fname = fname.replace('-','_')
#
#    print 'Reading %s...'%datafile
#    xyz = pd.read_csv(datafile, sep='\t')
#
#    idx = xyz['CORR_DEPTH'].notnull()
#    xyzout = pd.DataFrame({
#            'X':xyz['LON'][idx],
#            'Y':xyz['LAT'][idx],
#            'Z':xyz['CORR_DEPTH'][idx],
#            })
#
#    return xyzout, fname
#
#
#for ga_file in ga_files:
#    datafile = '%s/%s'%(basedir,ga_file)
#    xyzout, fname = parse_mgd77(datafile)
#    print 'Saving to hdf5...'
#    h5[fname] = xyzout

# Close the hdf file
print 'Closing HDF file: %s'%outfile
h5.close()
