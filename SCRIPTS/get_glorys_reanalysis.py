"""
Download the GLORYS reanalysis from the copernicus server
"""

import pandas as pd
import subprocess
import os

####
# Variables
output_dir = '/home/suntans/Share/ScottReef/DATA/OCEAN/GLORYS'
#output_dir = '~/group/mrayson/DATA/OCEAN/GLORYS'
outfile_file = 'GLORYS_NWS_{}.nc'
username = 'mrayson'
pwd = 'UWA0cean$$'

#service = 'GLOBAL_REANALYSIS_PHY_001_030-TDS'
#product = 'global-reanalysis-phy-001-030-daily'
#motuurl = 'http://my.cmems-du.eu/motu-web/Motu'

service = 'GLOBAL_ANALYSIS_FORECAST_PHY_001_024-TDS'
product = 'global-analysis-forecast-phy-001-024'
motuurl = 'http://nrt.cmems-du.eu/motu-web/Motu'

t1,t2 = "2019-01-31 00:00:00", "2019-06-01 00:00:00"
####

downloadstr = \
    "python -m motuclient --motu {} --service-id {} --product-id {}\
 --longitude-min 105 --longitude-max 144 --latitude-min -26 --latitude-max -3 --date-min '{}' --date-max '{}' --depth-min 0.493 --depth-max\
 5727. --variable thetao --variable so --variable zos --variable uo --variable vo --out-dir {} --out-name {} --user {} --pwd '{}'"

#times = pd.date_range("2011-12-31 00:00:00", "2014-01-01 00:00:00", freq="D")
#times = pd.date_range("2013-12-31 00:00:00", "2014-07-01 00:00:00", freq="D")
times = pd.date_range(t1,t2, freq="D")
print(times)
for t1,t2 in zip(times[::1],times[1::]):
    #print(t1.strftime('%Y-%m-%d 12:00:00'))
    myfile = outfile_file.format(t2.strftime('%Y%m%d'))
    print(myfile)

    getstr = downloadstr.format(motuurl, service, product,\
        t1.strftime('%Y-%m-%d 12:00:00'),\
        t2.strftime('%Y-%m-%d 12:00:00'),\
        output_dir,myfile, username, pwd)

    if os.path.exists('%s/%s'%(output_dir, myfile)):
        print('#### Files {} -- mmmoving on...'.format(myfile))
        continue


    print(getstr)
    subprocess.call(getstr, shell=True)


