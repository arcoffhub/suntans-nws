"""
Download 1hr CFSR data from opendap server
"""

from soda.dataio.datadownload.get_metocean_dap import get_cfsr_tds

xrange = [106.0,144.0]
yrange = [-26.0,-426]

trange = ['20131201.000000','20150215.000000']
trange = ['20131201.000000','20131202.000000']
#trange = ['20081201.000000','20090815.000000']
outfile = '~/Share/TimorSea/DATA/ATMOSPHERE/CFSR_2014_NWS.nc'
outfile_pair = '~/Share/TimorSea/DATA/ATMOSPHERE/CFSR_2014_pair_NWS.nc'


get_cfsr_tds(xrange, yrange, trange, outfile, outfile_pair)
