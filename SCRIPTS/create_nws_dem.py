"""
Create DEMs for the entire NWS/Northern Australia region
"""
import numpy as np
from glob import glob
import matplotlib.pyplot as plt

import pandas as pd
import xray

from soda.utils.cartgrid import RegGrid
from soda.dataio.conversion.demBuilder import demBuilder
from soda.dataio.conversion.dem import DEM, blendDEMs

#####
#basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'
basedir = '/home/mrayson/group/mrayson/DATA/BATHYMETRY/'

gafile = '%s/GA2009_250m_fullgrid.nc'%basedir
gebcofile = '%s/GEBCO_2014_2D_90.0_-40.0_160.0_0.0.nc'%basedir
#otherfile = '%s/GA_WEL_Multi_Merged_w_GA250_250m_DEM.nc'%basedir
otherfile = '%s/GA_WEL_NWS_250m_DEM.nc'%basedir

outfile1 = '%s/dem_GA2009_NWS.nc'%basedir
#outfile1 = '%s/dem_GA2009_WEL_multi_NWS.nc'%basedir
outfile2 = '%s/dem_GEBCO_NWS.nc'%basedir
#outfile3 = '%s/dem_GAWEL_merged_NWS.nc'%basedir
outfile3 = '%s/dem_GAWEL_multi_merged_NWS.nc'%basedir

outfile = '%s/TimorSea_GAWEL_Multi_GEBCO_Combined_DEM.nc'%basedir

weights = [2., 1., 100.]
maxdist = [.5, .5, .5] # Degrees

x0 = 106.
x1 = 144.
y0=-26.
y1=-4.

## Testing
#x0 = 120.
#x1 = 126.
#y0=-16.
#y1=-8.

dx = 0.0025

#######


# Create a grid object (used for finding indices)
grd = RegGrid([x0,x1], [y0,y1], dx, dx, meshgrid=False)

# Create a dem with the GA
print 'Regridding file: %s'%gafile
dem1 = DEM(gafile, meshgrid=False)
dem1.clip(x0,x1,y0,y1)
dem1.regrid(grd.x, grd.y, meshgrid=True)
dem1.savenc(outfile=outfile1)

## Clip the gebco bathy and interpolate onto the new GA grid
print 'Regridding file: %s'%gebcofile
dem2 = DEM(gebcofile, meshgrid=False)
dem2.clip(x0,x1,y0,y1)
dem2.regrid(grd.x, grd.y, meshgrid=True)
dem2.savenc(outfile=outfile2)

##
print 'Regridding file: %s'%otherfile
dem3 = DEM(otherfile, meshgrid=False)
dem3.regrid(grd.x, grd.y, meshgrid=True)
dem3.savenc(outfile=outfile3)

#### 
# Blend all of the DEMs together
####
blendDEMs([outfile1, outfile2, outfile3], outfile,\
     weights, maxdist)   

# Testing...
#blendDEMs([outfile1, outfile2 ], outfile,\
#     weights[0:2], maxdist[0:2])   



print('Done')

"""

## Clip the gebco bathy and interpolate onto the new GA grid
#dem1 = DEM(gebcofile, meshgrid=False)
#dem1.clip(x0,x1,y0,y1)

# Subset the GA data
#ds = xray.open_dataset(gafile)
#ga = ds.sel(X=slice(x0,x1), Y=slice(y0,y1))

bbox_xy = [x0,x1,y0,y1]

#dem0 = demBuilder(infile=gafile,\
#        h5groups = None,\
#        convert2utm=False,\
#        interptype='blockavg',\
#        #interptype='kriging',\
#        #interptype = 'nn',\
#	bbox=bbox_xy, \
#        dx=dx, \
#       )
#
#dem0.build()
##dem0.save(outfile='%s/%s'%(basedir, 
outfile2 = '%s/dem_GEBCO_NWS.nc'%basedir
outfile3 = '%s/dem_GAWEL_merged_NWS.nc'%basedir

outfile = '%s/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir

weights = [1000., 1, 1e6]
maxdist = [.1, .1, .1] # Degrees

x0 = 106.
x1 = 144.
y0=-26.
y1=-4.

dx = 0.0025

#######


# Create a grid object (used for finding indices)
grd = RegGrid([x0,x1], [y0,y1], dx, dx, meshgrid=False)

# Create a dem with the GA
print 'Regridding file: %s'%gafile
dem1 = DEM(gafile, meshgrid=False)
dem1.clip(x0,x1,y0,y1)
dem1.regrid(grd.x, grd.y)
dem1.savenc(outfile=outfile1)

## Clip the gebco bathy and interpolate onto the new GA grid
print 'Regridding file: %s'%gebcofile
dem2 = DEM(gebcofile, meshgrid=False)
dem2.clip(x0,x1,y0,y1)
dem2.regrid(grd.x, grd.y)
dem2.savenc(outfile=outfile2)

##
print 'Regridding file: %s'%otherfile
dem3 = DEM(otherfile, meshgrid=False)
dem3.regrid(grd.x, grd.y)
dem3.savenc(outfile=outfile3)

#### 
# Blend all of the DEMs together
####
blendDEMs([outfile1, outfile2, outfile3], outfile,\
     weights, maxdist)   



## Clip the gebco bathy and interpolate onto the new GA grid
#dem1 = DEM(gebcofile, meshgrid=False)
#dem1.clip(x0,x1,y0,y1)

# Subset the GA data
#ds = xray.open_dataset(gafile)
#ga = ds.sel(X=slice(x0,x1), Y=slice(y0,y1))

bbox_xy = [x0,x1,y0,y1]

#dem0 = demBuilder(infile=gafile,\
#        h5groups = None,\
#        convert2utm=False,\
#        interptype='blockavg',\
#        #interptype='kriging',\
#        #interptype = 'nn',\
#	bbox=bbox_xy, \
#        dx=dx, \
#       )
#
#dem0.build()
##dem0.save(outfile='%s/%s'%(basedir, outfile1) )
##Z = dem0.interp(grd.X.ravel(), grd.Y.ravel(), method='linear')
##Z = dem0.regrid(grd.x,grd.y)

"""

