
# coding: utf-8

# # Internal tide atlas generation
# 
# Collate the internal tide amplitude data into a single gridded netcdf file
# 
# See:
#     - `calc_sunharmonics_monthly.py`
#     - `suntans_mode_fit_mpi.py`
#     - `suntans_mode_fit_uv_mpi.py`

# In[1]:


# Use a dask client
import dask
from dask import delayed
#from dask_jobqueue import SLURMCluster
#from dask.distributed import Client, LocalCluster, worker, progress, wait


# In[2]:


#cluster = SLURMCluster()
#client=Client(cluster)
#cluster.scale(4)
#client
# Manually set the dask scheduler
dask.config.set(scheduler='processes')



# In[12]:


#from soda.dataio.suntans.sunpy import Spatial
import xarray as xr
import dask
from netCDF4 import Dataset
import numpy as np
from glob import glob
from datetime import datetime
import re

from soda.dataio.suntans.sunxray import Sundask
from soda.utils.myproj import MyProj
from soda.utils.otherplot import axcolorbar
from soda.utils.fkriging import kriging

import matplotlib.pyplot as plt


# In[7]:


#######
# Output grid
xlims = [107.5,142.5]
ylims = [-25.0,-5.0]

dx = 0.02 # 2 km resolution

#basedir = '../SCENARIOS/OUTPUT_NWS_2km_hex_2019'
basedir = 'SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2013_2014'
########


# In[8]:


# List all of the netcdf files
#filestr = '../SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/NWS_2km_*_3D_ModeAmp.nc.0'
filestr = '{}/NWS_2km_*_3D_ModeAmp.nc.0'.format(basedir)


ncfiles = sorted(glob(filestr))
print(ncfiles)


# In[39]:


# List all of the netcdf files
#filestr = '../SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/NWS_2km_*_3D_ModeAmp.nc.0'
filestr = '{}/NWS_2km_*_3D_UVModeAmp.nc.0'.format(basedir)


uncfiles = sorted(glob(filestr))
print(uncfiles)


# In[9]:


#filestr = '../SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/NWS_2km_*_3D_Harmonics.nc.0'
filestr = '{}/NWS_2km_*_3D_Harmonics.nc.0'.format(basedir)


rhofiles = sorted(glob(filestr))
print(rhofiles)


# In[25]:


# Find the dates for each file
tstart = []
tend = []
for ncfile in ncfiles:
    match = re.search('\d{4}\d{2}\d{2}_\d{4}\d{2}\d{2}', ncfile)
    t1,t2 = match.group().split('_')
    tstart.append(datetime.strptime(t1, '%Y%m%d'))
    tend.append(datetime.strptime(t2, '%Y%m%d'))

# Use the half-way point as the data
time = []
for t1,t2 in zip(tstart, tend):
    time.append(t1 + (t2-t1)/2)
time



# Find the harmonics files (to get density)
# In[40]:


# Load one month
def load_suntans(ncfile):
    sun = Sundask(ncfile, timedim='Ntide')
    sun.cells[sun.cells<0] = 0
    
    # Set the projection
    P = MyProj('merc')
    sun.xp, sun.yp = P.to_ll(sun.xp, sun.yp)
    sun.xv, sun.yv = P.to_ll(sun.xv, sun.yv)
    sun._xy = None # reset this
    
    return sun
    
for myfile in ncfiles:
    print(myfile[:-1]+'*')
    sun = load_suntans(myfile[:-1]+'*')
    
    break

for myfile in uncfiles:
    print(myfile[:-1]+'*')
    sunu = load_suntans(myfile[:-1]+'*')
    
    break


# In[41]:


print(sun._ds)
print(sunu._ds)


# In[42]:


x = np.arange(xlims[0],xlims[1],dx)
y = np.arange(ylims[0],ylims[1],dx)
X,Y = np.meshgrid(x,y)


# In[43]:


# Interpolate the depth data
depth = sun.load_data('dv')
depth_2d = sun.interpolate(depth, X, Y, kind='linear')


# In[44]:


# Initialise the output array
nfrq = sun._ds.dims['Ntide']
nmodes = sun._ds.dims['Nmode']
ntime = len(time)
ny,nx = X.shape
nz = sun._ds.z_r.shape[0]

ntime, nfrq, nmodes, ny, nx
# Dimensions: time, nfrq, nmodes, latitude, longitude

amp_re_xy = np.zeros((ntime, nfrq, nmodes, ny, nx))
amp_im_xy = np.zeros((ntime, nfrq, nmodes, ny, nx))

amp_u_re_xy = np.zeros((ntime, nfrq, nmodes, ny, nx))
amp_u_im_xy = np.zeros((ntime, nfrq, nmodes, ny, nx))

amp_v_re_xy = np.zeros((ntime, nfrq, nmodes, ny, nx))
amp_v_im_xy = np.zeros((ntime, nfrq, nmodes, ny, nx))

rho_xy = np.zeros((ntime, nz, ny, nx))

#amp_re_xy = dask.array.zeros((ntime, nfrq, nmodes, ny, nx),chunks=(1,1,1,ny,nx))
#amp_im_xy = dask.array.zeros((ntime, nfrq, nmodes, ny, nx),chunks=(1,1,1,ny,nx))
#sun._ds.dims

# Test Interpolate depths onto the square grid
#Z = sun.interpolate(sun.cell2node(sun.dv), X, Y[::-1,:], kind='linear')
#Z = sun.interpolate(sun.dv, X, Y[::-1,:], kind='linear')
#
#plt.imshow(Z)@delayed
#def suntans_interp(sun, Z, X, Y):
#    return dask.array.from_array(sun.interpolate(sun.cell2node(Z), X, Y, kind='linear'))
## In[45]:



class SunInterp(object):
    def __init__(self, sun, X, Y, **kwargs):
        self.sun = sun
        # Build an interpolation object
        xyin = np.array([sun.xv.ravel(), sun.yv.ravel()]).T
        xyout = np.array([X.ravel(), Y.ravel()]).T
        self.F = kriging(xyin, xyout, **kwargs)
        self.cellidx = sun.find_cell(X.ravel(), Y.ravel())
        self.ny, self.nx = X.shape 
    def __call__(self, data, klayer=0):
        # Interpolate
        Z =self.F(data)

        # NaN out points < klayer
        kidx = self.sun.Nk[self.cellidx] < klayer+1
        Z[kidx] = np.nan

        # Nan out cells out side of the range
        Z[self.cellidx==-1] = np.nan
        Z = Z.reshape((ny,nx))
        
        return Z

class SunNearest(object):
    def __init__(self, sun, X, Y, **kwargs):
        self.sun = sun
        # Build an interpolation object
        self.cellidx = sun.find_cell(X.ravel(), Y.ravel())
        self.ny, self.nx = X.shape 
    def __call__(self, data, klayer=0):
        # Interpolate
        Z =data[self.cellidx]

        # NaN out points < klayer
        kidx = self.sun.Nk[self.cellidx] < klayer+1
        Z[kidx] = np.nan

        # Nan out cells out side of the range
        Z[self.cellidx==-1] = np.nan
        Z = Z.reshape((ny,nx))
        
        return Z

# Build the interpolation object
#Finterp = SunInterp(sun, X, Y, maxdist=2e4, vrange=2000.)
Finterp = SunNearest(sun, X, Y)

Z = Finterp(sun.dv)
#plt.imshow(Z)
# In[46]:


# for each time step
for tt, ncfile in enumerate(ncfiles):
    
    print('Processing file:\n\t%s'%ncfile)
    sun = load_suntans(ncfile[:-1]+'*')
    # Load the data
    amp_re = sun.load_data('amp_b_re',)
    amp_im = sun.load_data('amp_b_im',)
    # loop through the frequencies and modes
    
    uncfile = uncfiles[tt]
    print('Processing file:\n\t%s'%uncfile)
    sunu = load_suntans(uncfile[:-1]+'*')
    # Load the data
    amp_u_re = sunu.load_data('amp_u_re',)
    amp_u_im = sunu.load_data('amp_u_im',)
    
    amp_v_re = sunu.load_data('amp_v_re',)
    amp_v_im = sunu.load_data('amp_v_im',)
    # loop through the frequencies and modes

    for ff in range(nfrq):
        for nn in range(nmodes):
            print('\tFrq: %d, mode: %d'%(ff,nn))
            #a_re.append( suntans_interp(sun, amp_re[ff,nn,:], X, Y))

            # Numpy method
            #a_re =\
            #    sun.interpolate(amp_re[ff,nn,:], X, Y, kind='linear')
            #a_im =\
            #    sun.interpolate(amp_im[ff,nn,:], X, Y, kind='linear')
            a_re = Finterp(amp_re[ff,nn,:])
            a_im = Finterp(amp_im[ff,nn,:])

            amp_re_xy[tt,ff,nn,:,:] = np.abs(a_re + 1j*a_im)
            amp_im_xy[tt,ff,nn,:,:] = np.angle(a_re + 1j*a_im)
            
            a_re = Finterp(amp_u_re[ff,nn,:])
            a_im = Finterp(amp_u_im[ff,nn,:])

            amp_u_re_xy[tt,ff,nn,:,:] = np.abs(a_re + 1j*a_im)
            amp_v_im_xy[tt,ff,nn,:,:] = np.angle(a_re + 1j*a_im)
            
            a_re = Finterp(amp_v_re[ff,nn,:])
            a_im = Finterp(amp_v_im[ff,nn,:])

            amp_v_re_xy[tt,ff,nn,:,:] = np.abs(a_re + 1j*a_im)
            amp_v_im_xy[tt,ff,nn,:,:] = np.angle(a_re + 1j*a_im)


    print('Loading density data...')
    # Load the data
    rho = sun.load_data('rho')
    for kk in range(nz):
        #print('Interpolating density on level %d...'%kk)
        rho_xy[tt,kk,:] = Finterp(rho[kk,:], klayer=kk)
    #amp_re_xy = dask.array.concatenate(a_re, axis=0)
    #del sun # Free up memory

print('Done.')


# In[47]:

#sun._ds
#
## Load the density data
## for each time step
#for tt, ncfile in enumerate(rhofiles):
#    
#    print('Processing file:\n\t%s'%ncfile)
#    sun = load_suntans(ncfile[:-1]+'*')
#    break
#sun._dz
## In[49]:


# Create an output dataset
a_re = xr.DataArray(amp_re_xy,         dims=('time','frq', 'modes', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'frq':sun._ds['omega'].values,\
                 'modes':sun._ds['modes'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'m',\
                'long_name':'Modal-harmonic buoyancy perturbation amplitude '\
                }
                   )

a_im = xr.DataArray(amp_im_xy,         dims=('time','frq', 'modes', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'frq':sun._ds['omega'].values,\
                 'modes':sun._ds['modes'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'',\
                'long_name':'Modal-harmonic buoyancy perturbation phase'\
                }
                   )

a_u_re = xr.DataArray(amp_u_re_xy,         dims=('time','frq', 'modes', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'frq':sun._ds['omega'].values,\
                 'modes':sun._ds['modes'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'m s-1',\
                'long_name':'Modal-harmonic eastward velocity amplitude '\
                }
                   )

a_u_im = xr.DataArray(amp_u_im_xy,         dims=('time','frq', 'modes', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'frq':sun._ds['omega'].values,\
                 'modes':sun._ds['modes'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'',\
                'long_name':'Modal-harmonic eastward velocity phase'\
                }
                   )

a_v_re = xr.DataArray(amp_v_re_xy,         dims=('time','frq', 'modes', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'frq':sun._ds['omega'].values,\
                 'modes':sun._ds['modes'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'m s-1',\
                'long_name':'Modal-harmonic northward velocity amplitude '\
                }
                   )

a_v_im = xr.DataArray(amp_v_im_xy,         dims=('time','frq', 'modes', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'frq':sun._ds['omega'].values,\
                 'modes':sun._ds['modes'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'',\
                'long_name':'Modal-harmonic northward velocity phase'\
                }
                   )

rho_da = xr.DataArray(rho_xy,         dims=('time','depth', 'latitude', 'longitude'),
        coords = {'time':time,\
                 'depth':sun._ds['z_r'].values,\
                 'latitude':y,
                 'longitude':x},
        attrs = {'units':'kg m-3',\
                'long_name':'Background density'\
                }
                   )

z = xr.DataArray(depth_2d,         dims=('latitude', 'longitude'),
        coords = {'latitude':y,
                 'longitude':x},
        attrs = {'units':'m',\
                'long_name':'Depth'\
                }
                   )


dsout = xr.Dataset({'amp_b':a_re, 'phs_b':a_im,
                    'amp_u':a_u_re, 'phs_u':a_u_im,
                    'amp_v':a_v_re, 'phs_v':a_v_im,
                    'z':z, 'rho':rho_da},
                   attrs = {
                    'Name':'North West Shelf internal tide amplitude climatology',
                    'Date created': datetime.now().strftime('%Y:%m:%d %H:%M:%S'),
                    'Author':'Matt Rayson',
                    'Institution':'University of Western Australia',
                    'Input Files':ncfiles
                   }
                  )
outfile = '{}/NWS_internal_tide_atlas_2019.nc'.format(basedir)
dsout.to_netcdf(outfile,
        encoding={'amp_b':{'zlib':1}, 'phs_b':{'zlib':1},
                  'amp_u':{'zlib':1}, 'phs_u':{'zlib':1},
                  'amp_v':{'zlib':1}, 'phs_v':{'zlib':1},'rho':{'zlib':1}})
print(dsout)
print('Wrote to: {}'.format(outfile))
print(72*'#')


