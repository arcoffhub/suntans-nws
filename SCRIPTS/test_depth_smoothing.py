"""
Test the suntans depth smoothing function
"""
import matplotlib
matplotlib.use('Qt5Agg')


from soda.dataio.suntans.sunpy import Grid
from soda.dataio.suntans.sundepths import DepthDriver

import numpy as np 
import scipy.io as io
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import os
from datetime import datetime, timedelta

import pdb

suntanspath = './GRIDS/NWS_15_2k_hex/'

####
# Bathymetry interpolation options
####
#basedir = '/home/suntans/Share/ScottReef/DATA'
#basedir2 = '/home/suntans/Share/TimorSea/DATA'
basedir = '/group/pawsey0106/mrayson/DATA'
basedir2 = '/group/pawsey0106/mrayson/DATA'

#depthfile = '%s/BATHYMETRY/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir
depthfile = '%s/BATHYMETRY/TimorSea_GAWEL_Multi_GEBCO_Combined_DEM.nc'%basedir
#depthfile = '%s/BATHYMETRY/GEBCO_2014_TimorSea.nc'%basedir

isDEM = True # Interpolate straight from DEM (other options are ignored)

depthmax=10.
depthmin=6000.

# Interpolation method
interpmethod='nn' # 'nn', 'idw', 'kriging', 'griddata'

# Type of plot
plottype=None # 'mpl', 'vtk2' or 'vtk3'

# Interpolation options
NNear=4
p = 1.0 #  power for inverse distance weighting
# kriging options
varmodel = 'spherical'
nugget = 0.1
sill = 0.8
vrange = 250.0
interpnodes=False

# Projection conversion info for input data
convert2utm=True
clip=False # Clip unused points in bathy grid
projstr='merc'
CS='WGS84' # Not used
utmzone=51 # Not used
isnorth=False # Not used
vdatum = 'MSL'

# Smoothing options
smooth=False
smoothmethod='gaussian' # USe kriging or idw for smoothing
smoothnear=4 # No. of points to use for smoothing
smoothrange=1.5e3
smoothmindepth=500.

print('Loading depth driver...')
D = DepthDriver(depthfile,\
    isDEM=isDEM,\
    interpmethod=interpmethod,\
    plottype=plottype,\
    NNear=NNear,\
    p=p,\
    varmodel=varmodel,\
    nugget=nugget,\
    sill=sill,\
    vrange=vrange,\
    clip=clip,\
    convert2utm=convert2utm,\
    utmzone=utmzone,\
    isnorth=isnorth,\
    projstr=projstr,\
    vdatum=vdatum,\
    smooth=smooth,\
    smoothmethod=smoothmethod,\
    smoothnear=smoothnear,\
    smoothrange=smoothrange,\
    smoothmindepth=smoothmindepth,\
    )
    
D(suntanspath, \
    depthmax=depthmax,\
    depthmin=depthmin,\
    interpnodes=interpnodes)


# Load the model grid and bathymetry into the object 'grd' and initialise the vertical spacing
grd1 = Grid(suntanspath)
# Load the depth data into the grid object
grd1.loadBathy(suntanspath+'/depths.dat-voro')
print(grd1.dv.std())
dvold = 1*grd1.dv # Make a copy of this

# Do it again with smoothing
D.smooth = True

D(suntanspath, \
    depthmax=depthmax,\
    depthmin=depthmin,\
    interpnodes=interpnodes)


grd2 = Grid(suntanspath)
# Load the depth data into the grid object
grd2.loadBathy(suntanspath+'/depths.dat-voro')
print(grd2.dv.std())


grd1.dv = grd1.dv - grd2.dv

grd1.clim = [-50,50]

plt.figure()
grd1.plot(cmap='RdBu')
plt.title('Difference [No smooth - Smooth]')
plt.show()
