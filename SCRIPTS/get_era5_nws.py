#!/usr/bin/env python
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()

xrange = [107.5,142.5]
yrange = [-25.0,-3.0]
#filename = '/home/suntans/DATA/ATMOSPHERE/ERA_test.nc'
filename = 'ERA_test.nc'


server.retrieve({
    "class": "ea",
    "dataset": "era5",
    "date": "2014-01-01/to/2014-01-31",
    "expver": "1",
    "levtype": "sfc",
    "number": "0",
    "param": "151.128/164.128/165.128/166.128/167.128/168.128",
    "stream": "enda",
    "time": "00:00:00/03:00:00/06:00:00/09:00:00/12:00:00/15:00:00/18:00:00/21:00:00",
    "type": "an",
    #"format" : "netcdf",
    "target": filename,
    #"area" : latmax+"/"+lonmin+"/"+latmin+"/"+lonmax,
    "area" : "%3.1f/%3.1f/%3.1f/%3.1f"%(yrange[1],xrange[0],yrange[0],xrange[1])
})

"""
# Precipitation data (only available every 12 hours)
server.retrieve({
    "class": "ea",
    "dataset": "era5",
    "date": "2010-02-19/to/2010-02-28",
    "expver": "1",
    "levtype": "sfc",
    "number": "0",
    "param": "228.128",
    "step": "0",
    "stream": "enda",
    "time": "09:00:00/21:00:00",
    "type": "4v",
    "target": "output",
})
"""
