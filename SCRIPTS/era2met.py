"""
Convert ERA file to a SUNTANS netdf
"""

from soda.dataio.datadownload.get_metocean_dap import get_metocean_local
from soda.dataio.suntans.metfile import SunMet, metfile
from soda.utils.maptools import ll2utm
from soda.utils import myairsea
from soda.utils.myproj import MyProj

from datetime import datetime
import numpy as np

#######
ncfile = 'MET/ERA_INTERIM_NWS_20052015.nc'
#ncfile = '/group/pawsey0106/mrayson/DATA/ATMOSPHERE/ERA_interim_TimorSea_2009.nc'
metfile = 'MET/ERA_SUNTANS_NWS_20052015_merc.nc'

vardict = {
    'tair':{'scale':1.0,'offset':-273.15,'data':[]},
    'uwind':{'scale':1.0,'offset':0.0,'data':[]},
    'vwind':{'scale':1.0,'offset':0.0,'data':[]},
    'pair':{'scale':0.01,'offset':0.0,'data':[]},
    'tdew':{'scale':1.0,'offset':-273.15,'data':[]},
    'cloud':{'scale':1.00,'offset':0.0,'data':[]},
    #'rain':{'scale':1.0,'offset':0.0,'data':[]},
    }

projstr='merc'
utmzone = 51
isnorth = False
########

# Load all of the data
for vv in vardict.keys():
    data, nc = get_metocean_local(ncfile,vv,name='ERA')
    # Zero masked values
    if isinstance(data,np.ma.MaskedArray):
        data[data.mask]=0.

    vardict[vv]['data']=data*vardict[vv]['scale']+vardict[vv]['offset']

# Convert the coordinates
X,Y = np.meshgrid(nc.X,nc.Y)
#ll = np.vstack([X.ravel(),Y.ravel()]).T
#xy = ll2utm(ll,utmzone,north=isnorth)
P = MyProj(projstr, utmzone=utmzone, isnorth=isnorth)
xpt, ypt = P(X.ravel(), Y.ravel())

#xpt = xy[:,0]
#ypt = xy[:,1]

zpt = 10.0*np.ones_like(xpt)

nxy = xpt.shape[0]
nt = nc.time.shape[0]

# Get the time in string format
dt = nc.time[1]-nc.time[0]
starttime = datetime.strftime(nc.time[0],'%Y%m%d.%H%M%S')
#endtime = datetime.strftime(nc.time[-1]+dt,'%Y%m%d.%H%M%S')
endtime = datetime.strftime(nc.time[-1],'%Y%m%d.%H%M%S')
# Load into a metfile
met = SunMet(xpt,ypt,zpt,(starttime,endtime,dt.total_seconds() ))

# Convert the dew point temperatature to relative humidity
rh = myairsea.relHumFromTdew(vardict['tair']['data'].squeeze(),\
        vardict['tdew']['data'].squeeze(), P=vardict['pair']['data'].squeeze() )

rh[rh>100.] = 100.
rh[rh<0.] = 0.

    
# Fill the arrays
met.Uwind.data[:] = vardict['uwind']['data'].reshape((nt,nxy))
met.Vwind.data[:] = vardict['vwind']['data'].reshape((nt,nxy))
met.Tair.data[:] = vardict['tair']['data'].reshape((nt,nxy))
met.Pair.data[:] = vardict['pair']['data'].reshape((nt,nxy))
met.RH.data[:] = rh.reshape((nt,nxy))
met.cloud.data[:] = vardict['cloud']['data'].reshape((nt,nxy))

# No rain in this file
#met.rain.data[:] = vardict['rain']['data'].reshape((nt,nxy))

met.write2NC(metfile)
