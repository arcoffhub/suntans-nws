"""
Download BRAN 3.5 data
"""

import numpy as np
import pandas as pd

from soda.dataio.datadownload.get_metocean_dap import get_cfsr_tds
from soda.dataio.datadownload.mythredds import MFncdap, GetDAP

import pdb
import xray
#from thredds_crawler.crawl import Crawl

metoceandict = {\
    'BRAN2016':{\
        'ncurl':[],\
        'multifile':True,\
        'type':'ocean',\
        'u':'u',\
        'v':'v',\
        'temp':'temp',\
        'salt':'salt',\
        'ssh':'eta_t',\
        'u_file':'u',\
        'v_file':'v',\
        'temp_file':'temp',\
        'salt_file':'salt',\
        'ssh_file':'eta',\

    },\
}

class BRAN2016(object):
    """
    Class for returning the list of BRAN files for a given time 
    range and variable list
    """

    #uname = 'noncom18'
    #pwd = 'XcbS1E'
    #baseurl = 'http://%s:%s@www.cmar.csiro.au/thredds/dodsC/BRAN3p5'%(uname,pwd)
    #baseurl = r'http://noncom18:XcbS1E@www.cmar.csiro.au/thredds/dodsC/BRAN3p5'
    baseurl = r'http://dapds00.nci.org.au/thredds/dodsC/gb6/BRAN/BRAN_2016/OFAM'
    #/ocean_temp_2016_08.nc

    dt = 24.
    dt_units = 'H'

    time_min = '1994-01-01 12:00:00'
    time_max = '2016-08-31 12:00:00'

    timevar = 'Time'

    def __init__(self, trange, tdsdict, **kwargs):
        self.__dict__.update(**kwargs)

        self.tdsdict = tdsdict

        # generate a list of time variables
        self.t0 = pd.datetime.strptime(trange[0], '%Y%m%d.%H%M%S')
        self.t1 = pd.datetime.strptime(trange[1], '%Y%m%d.%H%M%S')

        time = pd.date_range(self.t0, self.t1,\
               freq = '%d%s'%(self.dt,self.dt_units))

        self.time = np.array([tout.to_datetime() for tout in time])

        #self.timestr = [pd.datetime.strptime(tt, '%Y%m%d.%H%M%S') for tt in self.time]

        # Create a dictionary with the file names for the first variables only
        fstr = self.tdsdict['%s_file'%'salt']
        filelist = [self.generate_url(self.baseurl, tt, fstr) for tt in self.time]

        self.ncfilelist = np.unique(filelist)
        self.tdsdict['ncurl'] = self.ncfilelist

        # Create an MFncdap object for time/variable lookup
        self.MF = MFncdap(self.ncfilelist, timevar=self.timevar)


    def __call__(self, time, var=None):
        """
        Return the same output as a call to MFncdap:
            filename, tstep
        """
        tind, fnames,tslice = self.MF(time)

        # Replace the file name with the correct one for my variable
        fstr = self.tdsdict['%s_file'%'salt']
        fstrnew = self.tdsdict['%s_file'%var]
        fnamenew = [fname.replace(fstr, fstrnew) for fname in fnames]

        # Replace the keys in the dictionary
        for ff in tslice.keys():
            new_key = ff.replace(fstr, fstrnew)
            tslice[new_key] = tslice.pop(ff)


        return tind, fnamenew, tslice
        #return filename, tslice, vname

    def generate_url(self,baseurl, time, varname):
        """
        Create the url string
        """
        timestr = pd.datetime.strftime(time,'%Y_%m')
        return r'%s/ocean_%s_%s.nc'%(baseurl,varname,timestr)
        #return r'%s/%s/ocean_%s_%s.nc'%(baseurl,varname,varname,timestr)
        #filename = baseurl+'/'+varname+'/'+'ocean_'+varname+'_'+timestr+'.nc'
        #return filename

    def get_filename_only(self, var=None):
        """
        Returns the first file only
        """
        fname =  self.ncfilelist[0]
        if not var == None:
            fstr = self.tdsdict['%s_file'%'salt']
            fstrnew = self.tdsdict['%s_file'%var]
            fnamenew = fname.replace(fstr, fstrnew)
            
        else:
            fnamenew = fname

        return fnamenew

class GetDAP_BRAN(GetDAP):
    """
    Special class for dealing with Bran data
    """
    
    def __init__(self,**kwargs):
        GetDAP.__init__(self, **kwargs)

    def get_coord_names(self,varname):
        """
        Just use the dimension names
        """
        timecoord,zcoord, ycoord,xcoord =  self._nc.variables[varname].dimensions
        return timecoord, xcoord, ycoord,zcoord

def get_bran2016_tds(xrange,yrange,zrange,trange,outfile,variables):
    """
    Extract BRAN 2016 ocean model data
    """
    mydict = metoceandict['BRAN2016']
    bran = BRAN2016(trange, mydict)
    # Create the thredds object
    TDS = GetDAP_BRAN(variables=variables, MF = bran,  **mydict)
    
    # Call the object
    TDS(xrange,yrange,trange,zrange=zrange,outfile=outfile)
    return TDS



#########
# Testing
#########

#uri = 'http://www.cmar.csiro.au/thredds/BRAN3p5/u/catalog.html'

#uname = 'noncom18'
#pwd = 'XcbS1E'
#uri = 'http://%s:%s@www.cmar.csiro.au/thredds/dodsC/BRAN3p5/u/ocean_u_1993_01.nc'%(uname,pwd)
#
#nc = xray.open_dataset(uri)

#trange =  ['20061227.120000','20070202.120000']
#mydict = metoceandict['BRAN_3p5']
#cfsr = BRAN_3p5(trange, mydict)

xrange = [107.5,142.5]
yrange = [-25.0,-3.0]
zrange = [0.,6000.]

vars = ['salt','temp','u','v']
#
#outfile = '../../../DATA/OCEAN/BRAN3p5/BRAN3p5_ScottReef_Jan2007.nc'
#TDS = get_bran3p5_tds(xrange,yrange,zrange,trange,outfile,vars)

outfilestr = '/home/mrayson/group/mrayson/DATA/OCEAN/BRAN2016/BRAN2016_TimorSea_%s.nc'
##d = pd.date_range('2005-12-31','2008-1-31',freq='M')
##d = pd.date_range('2007-12-31','2008-1-31',freq='M')
##d = pd.date_range('2006-12-31','2007-1-31',freq='M')
##d = d.strftime('%Y%m%d.%H%M%S')
##d = d.tolist()
#
#d = pd.date_range('2005-12-31 12:00:00','2008-1-31 12:00:00',freq='D')
#d = pd.date_range('2007-02-27 12:00:00','2008-1-31 12:00:00',freq='D')
#d = pd.date_range('2009-08-01 12:00:00','2010-2-01 12:00:00',freq='D')
#d = pd.date_range('2009-07-01 12:00:00','2009-08-01 12:00:00',freq='D')
#d = pd.date_range('2010-02-01 12:00:00','2010-03-01 12:00:00',freq='D')
#d = pd.date_range('2014-12-01 12:00:00','2015-01-31 12:00:00',freq='D')
d = pd.date_range('2013-01-01 12:00:00','2014-01-01 12:00:00',freq='D')
#d = pd.date_range('2015-04-01 12:00:00','2015-5-01 12:00:00',freq='D')
##d = pd.date_range('2006-12-31 12:00:00','2007-1-31 12:00:00',freq='D')

for t1,t2 in zip(d[0:-1],d[1::]):
     trange=[t1.strftime('%Y%m%d.%H%M%S'),t2.strftime('%Y%m%d.%H%M%S')]
     outfile = outfilestr%(t1.strftime('%Y%m%d') )
     print outfile

     get_bran2016_tds(xrange,yrange,zrange,trange,outfile,vars)

