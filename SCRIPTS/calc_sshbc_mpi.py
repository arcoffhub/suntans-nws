"""
Calculate the baroclinic sea surface height perturbation
"""
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from glob import glob

from netCDF4 import Dataset

from soda.dataio.suntans.suntans_ugrid import ugrid
from soda.dataio.suntans.sunpy import Spatial

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    print('MPI running: rank = %d, ncores = %d'%(rank,size))
    verbose=True
else:
    verbose=False
    
    
def calc_ssh(rho, rholow, dz, rho0=1024., g=9.81, axis=0):
    rhopr = rho-rho_low
    b = -rhopr*g/rho0
    
    p_surf = rho0* np.sum(b*dz[:,None], axis=axis)
    
    return p_surf / (rho0*g)

###
# This should go into soda...
class OnlineFilter(object):
    """
    Online filtering class
    """
    def __init__(self, dt, cutoff_dt, P, shape, loadfunc):
        """
        Inputs:
            dt - data sampling rate
            cutoff_dt - cutoff time step
            P - filter order
            shape - array shape not including the time dimension
            loadfunc - function to load a chunk of data
                should take an index as input and return an array of size (1, shape)
        """
        self.P=P
        self.dt = dt
        self.cutoff_dt = cutoff_dt
        self.shape = shape
        self.loadfunc = loadfunc
        
        # Compute the filter coefficients
        Wn = dt/cutoff_dt
        (b,a) = signal.butter(P, Wn, fs=1)
        self.b, self.a = b, a
        
        # Initialize the filtering arrays
        self.x_tmp = np.zeros((self.P+1,)+self.shape)
        self.y_tmp = np.zeros((self.P+1,)+self.shape)

        # Load up the initial values of x_tmp 
        self._ii = 0
        for nn in range(0, self.P):
            self.x_tmp[nn,...] = loadfunc(self._ii)
            self._ii+=1
        
        # Initialise y_tmp with the mean
        self.y_tmp[:,...] = self.x_tmp[0:self.P,...].mean(axis=0)

    def __call__(self):
        """
        Apply the filter to the next value
        """
        P = self.P
        # shuffle everything back in x and y
        self.x_tmp[1:P+1,...] = self.x_tmp[0:P,...]
        self.y_tmp[1:P+1,...] = self.y_tmp[0:P,...]

        # Load new data step
        self.x_tmp[0,...] = self.loadfunc(self._ii)
        
        # Zero the first step
        self.y_tmp[0,...] = 0.
        
        for n in range(0,P+1):
            self.y_tmp[0,...] += self.b[n]*self.x_tmp[n,...]

        for j in range(1,P+1):
            self.y_tmp[0,...] -= self.a[j]*self.y_tmp[j,...]
            
        self._ii += 1
        
        return self.y_tmp[0,...]


def load_suntans_mf(my_node):
    """
    Load a suntans object that is split across directories
    """
    
    basedir = 'SCENARIOS/NWS_2km_GLORYS_hex_Nk80dt60_2013_2014'
    basefile = 'NWS_2km_GLORYS_hex_*'

    testfile = []
    for ii in range(1,13):
        testfile.append(
            glob( '{}_data{}/{}.nc.{}'.\
                format(basedir, ii, basefile, my_node))[0]
        )

    return Spatial(testfile, _FillValue=999999, klayer=[-99])


    
####
numfiles = 144


####
# Main loop
for my_node in range(rank, numfiles, size): # Breaks up the list for MPI


    # Filter cutoff period
    cutoff_dt = 60*3600
    # Filter order
    P = 3

    outpath = 'SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2013_2014'
    basefile = 'NWS_2km_GLORYS_hex_2013_2014_SSHBC'
    outfile = '{}/{}.nc.{}'.format(outpath, basefile, my_node)
    #########

    if verbose:
        print(72*'#')
        print('Calculating on processor: ', my_node)
        
    # Load the object
    sun = load_suntans_mf(my_node)

    def load_rho(ii):
        sun.tstep = [ii]
        return sun.loadData(variable='rho')*1000+1000.

    shape=(sun.Nkmax, sun.Nc)
    dt = sun.timeraw[2]-sun.timeraw[1]

    # Create the filter object
    F = OnlineFilter(dt, cutoff_dt, P, shape, load_rho)

    # Loop through and calculkate the sea surface height anomaly
    ssh_bc = np.zeros((sun.Nt,sun.Nc))
    for ii in range(P, sun.Nt-1):
        if ii%100 == 0:
            if verbose:
                print(ii,sun.Nt)
        rho_low = F()
        rho = F.x_tmp[0,:,:]

        ssh_bc[ii,:] = calc_ssh(rho,rho_low, sun.dz)

    #
    # Create the output file 
    sun.Nk = np.zeros((sun.Nc,),dtype=np.int32)
    #sun.dv = np.ones((sun.Nc,),dtype=np.int32)
    sun.Nkmax = 1
    sun.z_r = 0.
    sun.z_w = [0,1.]
    sun.dz = 0.

    print(sun.dv.max())

    sun.writeNC(outfile)

    # Create the output variables
    dims = ('time', 'Nc')
    attrs = {'ssh_bc':\
            {'long_name':'baroclinic sea surface height anomaly',\
            'coordinates':'time Nc',\
            'units':'m'},\
        }

    varlist = ['ssh_bc']
    for vv in varlist:
        sun.create_nc_var(outfile, vv,\
            dims,\
            attrs[vv],\
        )

    sun.create_nc_var(outfile,'time',\
        ugrid['time']['dimensions'],\
        ugrid['time']['attributes'])

    # Create the solution
    T = sun.timeraw
    nt = T.shape[0]

    nc = Dataset(outfile,'a')

    ## Save it in one hit
    nc.variables['time'][:] = T 
    sun.tstep = range(0,nt)

    vv = 'ssh_bc'
    if verbose:
        print('Saving variable %s...'%vv)

    nc.variables[vv][:] = ssh_bc
    nc.close()

    if verbose:
        print('Done. Save to file: ', outfile)
