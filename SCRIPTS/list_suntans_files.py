"""
List all of the suntans files in basedir
"""

import os
from fnmatch import fnmatch

basedir = '/scratch/pawsey0106/mrayson/SUNTANS/NWS/SCENARIOS'

ncfiles = []
ncdirs = []
for root, dirs, files in os.walk(basedir):
    for file in files:
        #if file.endswith(".nc.*"):
        if fnmatch(file,"*.nc.*"):
            print('%s/%s'%(root,file))
            ncfiles.append(os.path.join(root, file))
            if root not in ncdirs:
                ncdirs.append(root)
                #print(root)

for nc in sorted(ncdirs):
     print(nc)


