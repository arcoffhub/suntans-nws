"""
Script for grabbing observations from the IMOS (AODN) thredds server
"""

import urllib2  
from xml.dom import minidom

import xray

IMOSvars = {
   'TEMP':'watertemp',\
   'PSAL':'salinity',\
   'DEPTH':'waterlevel',\
   'UCUR':'water_u',\
   'VCUR':'water_v',\
   'WCUR':'water_w',\
   }

IMOSattrs = {
   #'DepthHeight':'height',
   'LONGITUDE':'longitude',
   'LATITUDE':'latitude',\
}

def process_imos_file(ncfile, ctr):
    print 72*'#'
    print 'Processing file: %s'%ncfile

    nc = xray.open_dataset(ncfile)

    # Create an output dataset
    ncout = xray.Dataset(attrs=nc.attrs)


    # Convert the data variables and insert them into the output object
    for varname in IMOSvars.keys():
        if varname in nc.data_vars.keys():
            newvarname = IMOSvars[varname]
            #V = convert_rps_variable(nc, varname, newvarname)
            V = nc[varname]
            ncout.update({newvarname: V})

    # Put the attribute variables into the Dataset as well
    for attr in IMOSattrs.keys():
        if attr in nc.data_vars.keys():
            ncout.update({attr:nc[attr]})
    
    # Get the 'height' attribute
    ncout.attrs.update({'height':nc.attrs['instrument_nominal_height']})

    # Get the station name
    #name = get_station_name(nc,ctr)
    name = '%s_%s_%03d'%(nc.attrs['deployment_code'],\
       nc.attrs['instrument'].replace('-','').replace(' ','_'), ctr) 

    ncout.attrs.update({'stationname':name})
    ncout.attrs.update({'original_file':ncfile})

    print 'Done\n'
    return ncout, name # Use the name as the netcdf group as well

def get_thredds_xml(baseurl, folder):
    """
    List all xml files under a given directory
    """
    xmlfile = '%s/%s/catalog.xml'%(baseurl, folder)
    print xmlfile


    doc = minidom.parse(urllib2.urlopen(xmlfile))
    urls = []
    IDS = []
    for node in doc.getElementsByTagName('dataset'):
        for cat in node.getElementsByTagName('catalogRef'):
            url = cat.getAttribute('xlink:href')
            ID = cat.getAttribute('ID')
            if 'catalog.xml' in url:
                urls.append('%s%s'%(baseurl, url))
                IDS.append(ID)
                #print url, ID

    return urls,IDS

def get_thredds_ncfilenames(xmlfile):
    "List all of the netcdf filenames in an xml catalog"
    print xmlfile
    doc = minidom.parse(urllib2.urlopen(xmlfile))
    ncurls = []
    for node in doc.getElementsByTagName('dataset'):
        url = node.getAttribute('urlPath')
        if '.nc' in url:
            ncurls.append(url)

    return ncurls

######
# Inputs 
baseurl = 'http://thredds.aodn.org.au/thredds/'
folder = 'IMOS/ANMN/QLD/'
targetfolders = ['Velocity', 'CTD_timeseries', 'Temperature']

targetsites = ['KIM','PIL']
outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/IMOS/IMOS_moorings_PILKIM.nc'

targetsites = ['GBR','ITF','NIN']
outfile = '/home/suntans/Share/ARCHub/DATA/FIELD/IMOS/IMOS_moorings_GBRITFNIN.nc'

mode = 'w'
######

# List all of the station subfolders
xmlfiles, folders = get_thredds_xml(baseurl, folder)

# For each subfolder list the sub-subfolders
ncfolders = []
for subfolder in folders:
    urls, subsubfolders = get_thredds_xml(baseurl, subfolder)

    for vv in targetfolders:
        for ss in targetsites:
            for subsubfolder in subsubfolders:
                if vv in subsubfolder and ss in subsubfolder:
                    ncfolders.append(subsubfolder)

# The ncfolders list now contains all of the folders containing nc files
ncfiles = []
for ncfolder in ncfolders:
    xmlfile = '%s/%s/catalog.xml'%(baseurl, ncfolder)
    ncfile = get_thredds_ncfilenames(xmlfile)
    for nc in ncfile:
        ncfiles.append('%s/dodsC/%s'%(baseurl,nc))

# Got through and write each one to disk
ctr = 0
for nc in ncfiles:
    nc, name = process_imos_file(nc, ctr)
    nc.to_netcdf(outfile, group=name, mode=mode)
    mode='a'
    ctr+=1
    print 'Data saved to %s\n\tGroup: %s'%(outfile, name)

