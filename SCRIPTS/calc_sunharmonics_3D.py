"""
Perform a harmonic analysis on SUNTANS output
"""

from soda.dataio.suntans.suntides import suntides
from datetime import datetime

import pandas as pd
from glob import glob

###
# Input variables
res='5k'

#tstart='20090802.0000'
#tend='20090902.0000'

outfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex/'

# Use glob to get the files from multiple directories
ncfile = glob('SCENARIOS/NWS_5km_hex_data*/NWS_5km_hex_*01_0000.nc')

#frqnames = ['M2','S2','N2','K1','M4','O1','M6','MK3','S4','MN4','NU2','S6','MU2','2N','O0','LAM2','S1','M1','J1','MM','SSA','SA','MSF','MF','RHO1','Q1','T2','R2','2Q','P1','2SM','M3','L2','2MK3','K2','M8','MS4']
frqnames = ['M2','S2','N2','K1','O1']
varnames = ['eta', 'uc', 'vc','w','rho','temp']

#frqnames = ['M2','S2','K1','O1','MSF','MM']
#varnames = ['temp']

#varnames = ['eta','ubar','vbar','uc','vc','rho','temp']

###

times = pd.date_range('2014-01-01','2015-02-01',freq='15D')

istart = 0
ii=-1

t1s = times[:-2]
t2s = times[2:]
for t1, t2 in zip(t1s[17::], t2s[17::]):
    ii+=1
    if ii < istart:
        continue

    print '\n', 72*'#','\n'
    print t1, t2
    tstart = t1.strftime('%Y%m%d.%H%M')
    tend = t2.strftime('%Y%m%d.%H%M')
    outfile = '%s/NWS_%skm_%s_%s_3D_Harmonics.nc'%(outfolder,res,\
        t1.strftime('%Y%m%d'), t2.strftime('%Y%m%d'))

    sun=suntides(sorted(ncfile),frqnames=frqnames)
    sun(tstart, tend, varnames=varnames)
    sun.tides2nc(outfile)

    # Clear the memory
    del sun
    sun=None
