"""
Calculates internal wave energy from SUNTANS harmonic output
"""

from soda.dataio.suntans.suntides import suntides
from soda.dataio.suntans.sunpy import Spatial

from netCDF4 import Dataset
import matplotlib.pyplot as plt
import numpy as np

from glob import glob
import pdb



def main(sunfile, outfile):
    #Constants
    RHO0=1024.
    g=9.81

    # Load the object
    sun=suntides(sunfile)
    Nf=sun.frq.shape[0]

    # Re-calculate the grid variables
    #sun.reCalcGrid()
    sun.calc_def()

    #sun = Spatial(sunfile,klayer=[-99])

    #
    ## calculate the buoyancy frequecny
    #rho = sun.loadData(variable='rho')*1000.0
    rho = sun.Mean['rho']*1000

    # Get the mask array
    mask = rho >= 999999.
    mask3d = np.tile(mask,(Nf,1,1))

    rho[mask]=0

    print 'Calculating N^2...'
    N2 = -g/RHO0 * sun.gradZ(rho)
    N2[N2<1e-5]=1e-5
    N2[mask]=0

    # Put everything in complex form
    print 'Converting arrays into complex form...'
    rho = sun.Amp['rho']*1000.0*np.cos(sun.Phs['rho']) + 1j * sun.Amp['rho']*1000.0*np.sin(sun.Phs['rho'])
    uc = sun.Amp['uc']*np.cos(sun.Phs['uc']) + 1j * sun.Amp['uc']*np.sin(sun.Phs['uc'])
    vc = sun.Amp['vc']*np.cos(sun.Phs['vc']) + 1j * sun.Amp['vc']*np.sin(sun.Phs['vc'])
    #ubar = sun.Amp['ubar']*np.cos(sun.Phs['ubar']) + 1j * sun.Amp['ubar']*np.sin(sun.Phs['ubar'])
    #vbar = sun.Amp['vbar']*np.cos(sun.Phs['vbar']) + 1j * sun.Amp['vbar']*np.sin(sun.Phs['ubar'])

    # Mask these arrays
    rho[mask3d]=0
    uc[mask3d]=0
    vc[mask3d]=0

    # Calculate the buoyancy perturbation
    b =  -g/RHO0 * rho

    # Calculate the baroclinic velocity
    print 'Calculating baroclinic velocity terms...'
    u_bc = np.zeros((Nf,sun.Nkmax,sun.Nc),dtype=np.complex128)
    v_bc = np.zeros((Nf,sun.Nkmax,sun.Nc),dtype=np.complex128)
    ubar = np.zeros((Nf,sun.Nc),dtype=np.complex128)
    vbar = np.zeros((Nf,sun.Nc),dtype=np.complex128)

    for ff in range(Nf):
        ubar2 = sun.depthave(uc[ff,:,:].squeeze())
        vbar2 = sun.depthave(vc[ff,:,:].squeeze())
        for kk in range(sun.Nkmax):
            u_bc[ff,kk,:] = uc[ff,kk,:] - ubar2
            v_bc[ff,kk,:] = vc[ff,kk,:] - vbar2

        # Replace the old barotropic velocity
        ubar[ff,:]=ubar2
        vbar[ff,:]=vbar2

    u_bc[mask3d]=0
    v_bc[mask3d]=0

    # Calculate the APE
    print 'Calculating APE...'
    APEi= np.zeros((Nf,sun.Nc),dtype=np.complex128)
    for ff in range(Nf):
        tmp = b[ff,:,:]**2/N2
        tmp[mask]=0
        APEi[ff,:] = RHO0/4.0 * sun.depthint(tmp) 

    APE = np.abs(APEi)

    # Calculate the HKE (note that this is total HKE BT+BC)
    print 'Calculating HKE...'
    HKEi= np.zeros((Nf,sun.Nc),dtype=np.complex128)
    HKEi_bc= np.zeros((Nf,sun.Nc),dtype=np.complex128)
    for ff in range(Nf):
        HKEi[ff,:] = RHO0/4.0 * sun.depthint(uc[ff,:,:]**2 + vc[ff,:,:]**2) 
        HKEi_bc[ff,:] = RHO0/4.0 * sun.depthint(u_bc[ff,:,:]**2 + v_bc[ff,:,:]**2) 

    HKE = np.abs(HKEi)
    HKE_bc = np.abs(HKEi_bc)

    # Calculate linear energy fluxes
    print 'Calculating energy flux...'
    Fu = np.zeros((Nf,sun.Nc),dtype=np.float64)
    Fv = np.zeros((Nf,sun.Nc),dtype=np.float64)
    for ff in range(Nf):
        # Note that signs needs to be reversed since z is positive
        p = sun.depthint(-b[ff,:,:],cumulative=True)
        p[mask]=0
        p = p - sun.depthave(p) # Remove the depth-average pressure
        Fu[ff,:] = np.real(RHO0/2.0 * sun.depthint( u_bc[ff,:,:]*p.conj() ))
        Fv[ff,:] = np.real(RHO0/2.0 * sun.depthint( v_bc[ff,:,:]*p.conj() ))
        
    # Calculate the linear flux divergence    
    print 'Calculating flux divergence...'
    divF = np.zeros((Nf,sun.Nc),dtype=np.float64)
    for ff in range(Nf):
        dFu_dx,dFu_dy = sun.gradH(Fu[ff,:])
        dFv_dx,dFv_dy = sun.gradH(Fv[ff,:])
        divF[ff,:] = dFu_dx + dFv_dy


    # Calculate the non-linear energy flux (u'E)
    print 'Calculating non-linear flux...'
    ###
    # This method only applies for M2-M2 --> M4 type systems
    ### 
    Fu_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)
    Fv_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)
    Fu_nl_bt = np.zeros((Nf,sun.Nc),dtype=np.float64)
    Fv_nl_bt = np.zeros((Nf,sun.Nc),dtype=np.float64)
    Fpe_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)

    #for ff in range(Nf):
    #    KE = RHO0*(u_bc[0,:,:]**2 + v_bc[0,:,:]**2)

    #    # Advection of baroclinic energy by baroclinic terms
    #    Fu_nl_bc[1,:] = np.real(0.375 * sun.depthint( KE * u_bc[1,:,:].conj() ))
    #    Fu_nl_bc[1,:] = np.real(0.375 * sun.depthint( KE * v_bc[1,:,:].conj() ))

    #    KEbar = RHO0*(ubar[0,:]**2 + vbar[0,:]**2)

    #    # Advection of baroclinic energy by baroctropic tide ???
    #    Fu_nl_bt[1,:] = np.real(0.375 *\
    #        sun.depthint(KEbar[np.newaxis,:] * u_bc[1,:,:].conj() ))
    #    Fv_nl_bt[1,:] = np.real(0.375 *\
    #        sun.depthint(KEbar[np.newaxis,:] * v_bc[1,:,:].conj() ))

    #    # Potential energy conversion
    #    fac = g**2/(RHO0**2)
    #    fac /= N2
    #    fac[mask]=0
    #    Fpe_nl_bc[1,:] = np.real(0.375*\
    #        sun.depthint(fac *rho[1,:,:].conj() * rho[0,:,:]*u_bc[0,:,:]) ) 


    ###
    # This is wrong
    ###
    #Fu_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)
    #Fv_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)
    #Fu_nl_bt = np.zeros((Nf,sun.Nc),dtype=np.float64)
    #Fv_nl_bt = np.zeros((Nf,sun.Nc),dtype=np.float64)
    #for ff in range(Nf):
    #    HKEz = RHO0/4.0 * (u_bc[ff,:,:]**2 + v_bc[ff,:,:]**2)
    #    APEz = RHO0/4.0 * b[ff,:,:]**2/N2
    #    #E = HKEz+APEz
    #    E = HKEz
    #
    #    E[mask]=0
    #    # Advection of baroclinic energy by baroclinic terms
    #    Fu_nl_bc[ff,:] = np.real(0.5 * sun.depthint( uc[ff,:,:]*E.conj() ))
    #    Fv_nl_bc[ff,:] = np.real(0.5 * sun.depthint( vc[ff,:,:]*E.conj() ))
    #
    #    # Advection of baroclinic energy by baroctropic tide
    #    Fu_nl_bt[ff,:] = np.real(0.5 * sun.depthint( ubar[ff,np.newaxis,:]*E.conj() ))
    #    Fv_nl_bt[ff,:] = np.real(0.5 * sun.depthint( vbar[ff,np.newaxis,:]*E.conj() ))
    # 
    #del HKEz
    #del APEz

    ## Calculate the non-linear energy flux divergence term
    print 'Calculating non-linear flux divergence...'
    divF_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)
    divF_nl_bt = np.zeros((Nf,sun.Nc),dtype=np.float64)
    divPE_nl_bc = np.zeros((Nf,sun.Nc),dtype=np.float64)
    #for ff in range(Nf):
    #    dFu_dx,dFu_dy = sun.gradH(Fu_nl_bc[ff,:])
    #    dFv_dx,dFv_dy = sun.gradH(Fv_nl_bc[ff,:])
    #    divF_nl_bc[ff,:] = -(dFu_dx + dFv_dy)

    #    dFu_dx,dFu_dy = sun.gradH(Fu_nl_bt[ff,:])
    #    dFv_dx,dFv_dy = sun.gradH(Fv_nl_bt[ff,:])
    #    divF_nl_bt[ff,:] = -(dFu_dx + dFv_dy)

    #    dFu_dx,dFu_dy = sun.gradH(Fpe_nl_bc[ff,:])
    #    divPE_nl_bc[ff,:] = -(dFu_dx + dFv_dy)

    # Calculate tihe linear conversion time (w_bt p')
    print 'Calculating the BT-BC conversion term...'
    dH_dx,dH_dy = sun.gradH(sun.dv)
    C = np.zeros((Nf,sun.Nc),dtype=np.float64)
    Psurf = np.zeros((Nf,sun.Nc),dtype=np.complex)
    for ff in range(Nf):
        p = sun.depthint(-b[ff,:,:],cumulative=True)
        p[mask]=0
        Psurf[ff,:] = sun.depthave(p)
        p = p - Psurf[ff,:] # Remove the depth-average

        p_bed = p[sun.Nk,range(sun.Nc)]
        w_bt = -(ubar[ff,:] * dH_dx + vbar[ff,:] * dH_dy)

        C[ff,:] = np.real( RHO0/2.0 * w_bt * p_bed.conj() ) 

    # Calculate Baines forcing term as expressed in Sherwin, Vlasenko et al., 2002
    #print 'Calculating the body force term...'
    #N2z = N2.T*sun.z_r
    #N2z = sun.depthint(N2z.T)
    #
    #F_body = np.zeros((Nf,sun.Nc),dtype=np.float64)
    #for ff in range(Nf):
    #    u0 = np.abs(ubar[ff,:])
    #    v0 = np.abs(vbar[ff,:])
    #    F_body[ff,:] = RHO0/sun.frq[ff] * N2z * ( (u0*sun.dv/dH_dx)**2. + (v0*sun.dv/dH_dy)**2 )**0.5 



    # Write the output to netcdf
    print 'Writing the output to netcdf...'

    sun.writeNC(outfile)

    nc = Dataset(outfile,'a')
    nc.Title = 'SUNTANS harmonic energy output'
    nc.Constituent_Names = ' '.join(sun.frqnames)

    # Add another dimension
    nc.createDimension('Ntide', Nf)

    nc.close()
    coords = 'xv yv Ntides'

    sun.create_nc_var(outfile, 'HKE', ('Ntide','Nc'), {'long_name':'Horizontal\
    baroclinic kinetic energy','units':'J m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'APE', ('Ntide','Nc'), {'long_name':'Available potential energy','units':'J m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'Fu', ('Ntide','Nc'), {'long_name':'Eastward linear energy flux','units':'W m-1','coordinates':coords})
    sun.create_nc_var(outfile, 'Fv', ('Ntide','Nc'), {'long_name':'Northward linear energy flux','units':'W m-1','coordinates':coords})
    sun.create_nc_var(outfile, 'Fu_nl_bc', ('Ntide','Nc'), {'long_name':'Eastward baroclinic non-linear energy flux','units':'W m-1','coordinates':coords})
    sun.create_nc_var(outfile, 'Fv_nl_bc', ('Ntide','Nc'), {'long_name':'Northward baroclinic non-linear energy flux','units':'W m-1','coordinates':coords})
    sun.create_nc_var(outfile, 'Fu_nl_bt', ('Ntide','Nc'), {'long_name':'Eastward barotropic non-linear energy flux','units':'W m-1','coordinates':coords})
    sun.create_nc_var(outfile, 'Fv_nl_bt', ('Ntide','Nc'), {'long_name':'Northward barotropic non-linear energy flux','units':'W m-1','coordinates':coords})
    sun.create_nc_var(outfile, 'divF', ('Ntide','Nc'), {'long_name':'Linear energy flux divergence','units':'W m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'divF_nl_bc', ('Ntide','Nc'), {'long_name':'Barolinic non-linear energy flux divergence','units':'W m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'divF_nl_bt', ('Ntide','Nc'), {'long_name':'Baroptropic non-linear energy flux divergence','units':'W m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'divPE_nl_bc', ('Ntide','Nc'),{'long_name':'Potential energy non-linear energy flux divergence','units':'W m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'C', ('Ntide','Nc'), {'long_name':'Barotropic to baroclinic energy conversion','units':'W m-2','coordinates':coords})
    sun.create_nc_var(outfile, 'Psurf', ('Ntide','Nc'), {'long_name':'Surface baroclinic presurre perturbation','units':'Pa','coordinates':coords})
    #sun.create_nc_var(outfile, 'F_body', ('Ntide','Nk','Nc'), {'long_name':'Internal body force','units':'kg m s-2'})

    # Write the data

    print 'Writing the variable data to netcdf...'

    nc = Dataset(outfile,'a')
    nc.variables['HKE'][:]=HKE_bc
    nc.variables['APE'][:]=APE
    nc.variables['Fu'][:]=Fu
    nc.variables['Fv'][:]=Fv
    nc.variables['Fu_nl_bc'][:]=Fu_nl_bc
    nc.variables['Fv_nl_bc'][:]=Fv_nl_bc
    nc.variables['Fu_nl_bt'][:]=Fu_nl_bt
    nc.variables['Fv_nl_bt'][:]=Fv_nl_bt
    nc.variables['divF'][:]=divF
    nc.variables['divF_nl_bt'][:]=divF_nl_bt
    nc.variables['divF_nl_bc'][:]=divF_nl_bc
    nc.variables['divPE_nl_bc'][:]=divPE_nl_bc
    nc.variables['C'][:]=C
    nc.variables['Psurf'][:] = np.abs(Psurf)*RHO0
    #nc.variables['F_body'][:]=F_body
    nc.close()

    print 'Done.'
    #cg_u = Fu / (HKE+APE)
    #cg_v = Fv / (HKE+APE)
    #cg = np.abs(cg_u+1j*cg_v)

    # Plot some results
    #sun.clim=[-0.25,0.25]
    #sun.plot(z=cg[0,:],titlestr='M2 frequency group velocity [m s-1]',cmap=plt.cm.RdBu)


    #plt.show()

#sunfile = 'rundata3D/ScottReef3D_Jan2007_Harmonics_2day.nc'
#outfile = 'rundata3D/ScottReef3D_Jan2007_Energetics.nc'
##sunfile = '../rundata3D/ScottReef3D_Jan2009_001.nc'
#runfolder =  'sunhex100m'
#sunfile = '%s/ScottReef3D_hex100_NH_Harmonics_M2M4M6.nc'%runfolder
#outfile = '%s/ScottReef3D_hex100_NH_Energetics_M2M4M6.nc'%runfolder

outfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex/'

# Use glob to get the files from multiple directories
ncfiles = glob('%s/NWS_5kkm_*_3D_Harmonics.nc'%outfolder)

for ncfile in ncfiles:
    outfile = ncfile.replace('Harmonics','Energetics')

    print '\n', 72*'#', '\n'
    print outfile
    main(ncfile, outfile)



