
# coding: utf-8

# # Fit vertical mode to SUNTANS harmonic data
# 
# $$
# b(z,t) = A(t)N^2(z)\phi(z)
# $$

# In[1]:



from soda.dataio.suntans.suntides import suntides
from soda.dataio.suntans.sunpy import Spatial
from soda.utils import mynumpy as mynp

from iwaves.utils.isw import iwave_modes_uneven
from iwaves.utils.tools import grad_z

from netCDF4 import Dataset
import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg as la

from glob import glob
import pdb

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


if rank == 0:
    print('MPI running: rank = %d, ncores = %d'%(rank,size))
    verbose=True
else:
    verbose=False


def calc_alpha(phi, c, N2, z):
    """
    Nonlinearity parameter
    """
    #phi_z = np.gradient(phi,-np.abs(dz))
    z2d = np.repeat(z[:,np.newaxis],phi.shape[1], axis=1)
    phi_z = mynp.grad_z(phi, z2d)

    #phi_z = mynp.grad_z(phi, z, axis=-1)
    num = 3*c*np.trapz( phi_z**3., x=z2d, axis=0)
    den = 2*np.trapz( phi_z**2., x=z2d, axis=0)

    return num/den


# In[2]:


###
nmodes_all = 4
mindepth = 100.
###


# In[3]:


def main(sunfile, outfile):
    #Constants
    RHO0=1024.
    g=9.81

    # Load the object
    sun=suntides(sunfile,_FillValue=999999)
    Nf=sun.frq.shape[0]

    # Re-calculate the grid variables
    #sun.reCalcGrid()
    #sun.calc_def()


    ## calculate the buoyancy frequecny
    #rho = sun.loadData(variable='rho')*1000.0
    rhobar = sun.Mean['rho']*1000

    # Get the mask array
    mask = rhobar >= 999999.
    mask3d = np.tile(mask,(Nf,1,1))

    rhobar[mask]=0

    #print 'Calculating N^2...'
    N2 = -g/RHO0 * sun.gradZ(rhobar)
    N2[N2<1e-9]=1e-9
    N2[mask]=0

    # Put everything in complex form
    #print 'Converting arrays into complex form...'
    #rho = sun.Amp['rho']*1000.0*np.cos(sun.Phs['rho']) + 1j * sun.Amp['rho']*1000.0*np.sin(sun.Phs['rho'])
    uc = sun.Amp['uc']*np.cos(sun.Phs['uc']) + 1j * sun.Amp['uc']*np.sin(sun.Phs['uc'])
    vc = sun.Amp['vc']*np.cos(sun.Phs['vc']) + 1j * sun.Amp['vc']*np.sin(sun.Phs['vc'])

    # Mask these arrays
    uc[mask3d]=0
    vc[mask3d]=0

    # Calculate the buoyancy perturbation
    #b =  -g/RHO0 * rho

    nf, nz, nc = uc.shape

    def mid_extrap(A, nk):
        B = np.zeros((nk+1,), dtype=A.dtype)
        B[1:-1] = 0.5*A[0:-1]+0.5*A[1::]
        B[0] = B[1]
        B[-1] = B[-2]
        return B


    # Create the output arrays
    cnall = np.zeros((nmodes_all, nc))
    alphaall = np.zeros((nmodes_all, nc))
    phiall = np.zeros((nz+1, nmodes_all, nc )) # Put modes last so we can array multiply
    uampall = np.zeros((nf, nmodes_all, nc ), dtype=np.complex)
    vampall = np.zeros((nf, nmodes_all, nc ), dtype=np.complex)
    N2out = np.zeros((nz+1, nc)) # Put modes last so we can array multiply
    rhoout = np.zeros((nz,nc))


    maxamp = 0.
    for ii in range(nc):
        if ii%100 == 0 and rank == 0:
            print("%d of %d (max amp = %3.1f m)..."%(ii,nc, maxamp))
            maxamp = 0.

        # Extract the N2 profile at a point
        nk = sun.Nk[ii]
        
        if nk < 6:
            continue
            #elif nk < 10:
            #    nmodes = nmodes_all # Use two modes on the shelf < 100 m 
        else:
            nmodes = nmodes_all


        myN2 = N2[0:nk,ii]
        z = -sun.z_r[0:nk]
        zw = -sun.z_w[0:nk+1]
        dz = zw[1:] - zw[0:-1]

        # Interpolate N2 onto the edge points
        N2mid = mid_extrap(myN2, nk)

        # Calculate the mode shapes
        phi, cn = iwave_modes_uneven(N2mid, zw)

        # Compute the gradient
        #phiz = grad_z(phi, zw, axis=0)
        z2d = np.repeat(zw[:,np.newaxis],phi.shape[1], axis=1)
        phiz = mynp.grad_z(phi, z2d)

        alpha_n = calc_alpha(phi[:,0:nmodes], cn[0:nmodes], N2mid, zw)

        # for each frequency
        for ff in range(nf):
            # Get velocity at cell-edges
            myu = uc[ff,0:nk,ii]
            umid = mid_extrap(myu, nk)
            myv = vc[ff,0:nk,ii]
            vmid = mid_extrap(myv, nk)

            # Compute the LHS
            L = phiz[:,0:nmodes] # Don't scale by the phase speed

            myuamp,_,_,_ = la.lstsq(L, umid)
            myvamp,_,_,_ = la.lstsq(L, vmid)

            uampall[ff,:,ii] = myuamp
            vampall[ff,:,ii] = myvamp
            
            if myuamp.max() > maxamp:
                maxamp = myuamp.max()*1

        # Output the variables
        cnall[:,ii] = cn[0:nmodes]
        alphaall[:,ii] = alpha_n
        phiall[0:nk+1,:,ii] = phiz[:,0:nmodes]
        N2out[0:nk+1,ii] = N2mid
        rhoout[0:nk,ii] = rhobar[0:nk,ii]

    #plt.subplot(121)
    #plt.plot(myN2,z)
    #plt.plot(N2mid,zw)
    #
    #plt.subplot(122)
    #plt.plot(phi[:,1],zw)
    #
    #plt.show()

    ####
    # Write the output to netcdf
    #print 'Writing the output to netcdf...'

    sun.writeNC(outfile)

    nc = Dataset(outfile,'a')
    nc.Title = 'SUNTANS harmonic modal amplitudes'
    nc.Constituent_Names = ' '.join(sun.frqnames)

    # Add another dimension
    nc.createDimension('Ntide', nf)
    nc.createDimension('Nmode', nmodes_all)
    nc.close()

    coords = 'xv yv Ntides'

    sun.create_nc_var(outfile,'omega', ('Ntide',),\
            {'long_name':'frequency','units':'rad s-1'})

    sun.create_nc_var(outfile,'modes', ('Nmode',),\
            {'long_name':'Vertical mode number','units':''})


    sun.create_nc_var(outfile, 'cn', ('Nmode','Nc'),\
            {'long_name':'Baroclinic phase speed','units':'m s-1',\
            'coordinates':coords})

    sun.create_nc_var(outfile, 'alpha_n', ('Nmode','Nc'),\
            {'long_name':'Nonlinearity Parameter','units':'m-1',\
            'coordinates':coords})


    sun.create_nc_var(outfile, 'phiz', ('Nkw', 'Nmode','Nc'),\
            {'long_name':'Vertical eigenfunction gradient',\
              'units':'m-1',\
              'coordinates':coords})

    sun.create_nc_var(outfile, 'N2', ('Nkw','Nc'),\
            {'long_name':'Buoyancy frequency squared',\
              'units':'s-2',\
              'coordinates':coords})

    sun.create_nc_var(outfile, 'rho', ('Nk','Nc'),\
            {'long_name':'Buoyancy frequency squared',\
              'units':'s-2',\
              'coordinates':coords})

    sun.create_nc_var(outfile, 'amp_u_re', ('Ntide', 'Nmode','Nc',),\
        {'long_name':'Eastward velocity real amplitude',\
        'units':'m s-1', \
        'coordinates':coords})

    sun.create_nc_var(outfile, 'amp_u_im', ('Ntide', 'Nmode','Nc',),\
        {'long_name':'Eastward velocity imaginary amplitude',\
        'units':'m s-1', \
        'coordinates':coords})

    sun.create_nc_var(outfile, 'amp_v_re', ('Ntide', 'Nmode','Nc',),\
        {'long_name':'Northward velocity real amplitude',\
        'units':'m s-1', \
        'coordinates':coords})

    sun.create_nc_var(outfile, 'amp_v_im', ('Ntide', 'Nmode','Nc',),\
        {'long_name':'Northward velocity imaginary amplitude',\
        'units':'m s-1', \
        'coordinates':coords})

    # Write the data

    #print 'Writing the variable data to netcdf...'

    nc = Dataset(outfile,'a')
    nc.variables['omega'][:]=sun.frq
    nc.variables['modes'][:]=range(nmodes_all)
    nc.variables['cn'][:]=cnall
    nc.variables['alpha_n'][:]=alphaall
    nc.variables['phiz'][:]=phiall
    nc.variables['N2'][:]=N2out
    nc.variables['rho'][:]=rhoout
    nc.variables['amp_u_re'][:]=np.real(uampall)
    nc.variables['amp_u_im'][:]=np.imag(uampall)
    nc.variables['amp_v_re'][:]=np.real(vampall)
    nc.variables['amp_v_im'][:]=np.imag(vampall)

    nc.close()

    #print 'Done.'


    # In[ ]:


#####
# Inputs
#ncfiles = 'SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/NWS_2km_20130701_20130731_3D_Harmonics.nc.%d'
#numfiles = 144

ncfiles = sorted(glob('SCENARIOS/OUTPUT_NWS_2km_GLORYS_hex_2013_2014/NWS_2km*3D_Harmonics.nc.*'))
#ncfiles = sorted(glob('SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/NWS_2km*3D_Harmonics.nc.*'))
#ncfiles = sorted(glob('SCENARIOS/OUTPUT_NWS_2km_hex_2019/NWS_2km*3D_Harmonics.nc.*'))
numfiles = len(ncfiles)
#####

for my_node in range(rank, numfiles, size): # Breaks up the list for MPI

    #ncfile = ncfiles%my_node
    ncfile = ncfiles[my_node]
    outfile = ncfile.replace('Harmonics','UVModeAmp')

    if rank == 0:
        print('\n', 72*'#', '\n')
        print(outfile)
    main(ncfile, outfile)


