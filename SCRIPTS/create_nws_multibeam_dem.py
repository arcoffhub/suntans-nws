"""
Create DEMs for the entire NWS/Northern Australia region
"""
import numpy as np
from glob import glob
import matplotlib.pyplot as plt

import pandas as pd
import xray

from soda.utils.cartgrid import RegGrid
from soda.dataio.conversion.demBuilder import demBuilder
from soda.dataio.conversion.dem import DEM, blendDEMs

#####
basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'

outfile1= '%s/GA_Multibeam_NWS_250m_DEM.nc'%basedir
outfile2= '%s/WEL_Multibeam_Browse_250m_DEM.nc'%basedir
outfile3= '%s/WEL_Multibeam_NWS_250m_DEM.nc'%basedir
#outfile4 = '%s/NGDC_Singlebeam_NWS_250m_DEM.nc'%basedir


outfile = '%s/GA_WEL_MultiBeam_NWSBrowse_DEM.nc'%basedir
#outfile = '%s/NGDC_GA_WEL_MultiBeam_NWSBrowse_DEM.nc'%basedir

weights = [1., 2., 2.]
maxdist = [.01, .01, .01] # Degrees
#weights = [1., 2., 2., 2.]
#maxdist = [.01, .01, .01, 0.] # Degrees

#######
#### 
# Blend all of the DEMs together
####
blendDEMs([outfile1, outfile2, outfile3], outfile,\
     weights, maxdist)   

