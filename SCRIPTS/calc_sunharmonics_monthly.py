"""
Perform a harmonic analysis on SUNTANS output
"""

from soda.dataio.suntans.suntides import suntides
from soda.dataio.suntans.sunpy import Spatial

from datetime import datetime

import pandas as pd
from glob import glob

from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


if rank == 0:
    print('MPI running: rank = %d, ncores = %d'%(rank,size))
    verbose=True
else:
    verbose=False

###
# Input variables
res='2'

#tstart='20090802.0000'
#tend='20090902.0000'

outfolder = 'SCENARIOS/OUTPUT_NWS_2km_hex_2013_2014/'

# Use glob to get the files from multiple directories
numfiles = 144

#frqnames = ['M2','S2','N2','K1','M4','O1','M6','MK3','S4','MN4','NU2','S6','MU2','2N','O0','LAM2','S1','M1','J1','MM','SSA','SA','MSF','MF','RHO1','Q1','T2','R2','2Q','P1','2SM','M3','L2','2MK3','K2','M8','MS4']
frqnames = ['M2','S2','N2','K1','O1']
varnames = ['eta', 'uc', 'vc','w','rho','temp']

#frqnames = ['M2','S2','K1','O1','MSF','MM']
#varnames = ['temp']

#varnames = ['eta','ubar','vbar','uc','vc','rho','temp']

###

for ii in range(1,13):
    for my_node in range(rank, numfiles, size): # Breaks up the list for MPI

        fn = my_node

        ncfile = glob('SCENARIOS/NWS_2km_hex_2013_2014_data%d/NWS_2km_hex_*.nc.%d'%(ii,fn))
        if rank==0:
            print(ncfile)

       
        tmp = Spatial(ncfile)

        t1 = tmp.time[0]
        t2 = tmp.time[-1]
        tstart = t1.strftime('%Y%m%d.%H%M')
        tend = t2.strftime('%Y%m%d.%H%M')

        if rank==0:
            print( '\n', 72*'#','\n')
            print( t1, t2)
 

        outfile = '%s/NWS_%skm_%s_%s_3D_Harmonics.nc.%d'%(outfolder,res,\
            t1.strftime('%Y%m%d'), t2.strftime('%Y%m%d'), fn)

        sun=suntides(sorted(ncfile),frqnames=frqnames, fit_all_k=True, verbose=verbose)

        sun(tstart, tend, varnames=varnames)
        sun.tides2nc(outfile)

        # Clear the memory
        del sun
        sun=None
    
    # Wait for the other nodes before moving on...
    if rank == 0:
        print('Waiting for the other nodes...')

    comm.barrier()
