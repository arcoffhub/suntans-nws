"""
Convert xyz data to a netcdf DEM model
"""


from soda.dataio.conversion.demBuilder import demBuilder
from soda.dataio.conversion.dem import blendDEMs
from soda.utils.maptools import Contour2Shp, utm2ll
from soda.dataio.suntans.sunpy import Grid

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

####

#basedir = '/home/suntans/Share/Projects/ScottReef/DATA/BATHYMETRY'
#basedir = u'/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'
basedir = '/home/group/mrayson/DATA/BATHYMETRY/'
#basedir = u'/home/suntans/Share/ScottReef/DATA/BATHYMETRY/'
#basedir = u'/home/suntans/Share/ARCHub/DATA/BATHYMETRY/'

hdffile = 'GA_50m_multibeam_bathymetry.h5'
outfile = 'GA_Multibeam_NWS_250m_DEM.nc'
scale = -1.
mygroups = None
#mygroups = 'SD50'

hdffile = 'BrowseRawBathymetry_xyz.h5'
outfile = 'WEL_Multibeam_Browse_250m_DEM.nc'
scale = 1.
mygroups = None

#hdffile = 'WEL_NWS_raw_bathymetry.h5'
#outfile = 'WEL_Multibeam_NWS_250m_DEM.nc'
#scale = 1.
#mygroups = None

#hdffile = 'NGDC_SingleBeam_NWS_raw_bathymetry.h5'
#outfile = 'NGDC_Singlebeam_NWS_250m_DEM.nc'
#scale = -1.
#mygroups = None


# Grid parameters
dx = 250. / 1e5

#x0 = 106.
#x1 = 144.
#y0=-26.
#y1=-4.

x0 = 110.
x1 = 126.
y0=-24.
y1=-10.


#####

bbox = [x0,x1,y0,y1]
#####
# Build a DEM with the geoscience survey bathy
datafile = '%s/%s'%(basedir,hdffile)

# Load the hdf file
h5 = pd.HDFStore( datafile , 'r')
print h5.keys()
#groups = [ii for ii in h5.keys() if groupstr in ii]
groups = [ii for ii in h5.keys()]


# Find the domain bounds
myx0 = np.inf
myx1 = -np.inf
myy0 = np.inf
myy1 = -np.inf

allgroups = []
for group in groups:
    print group
    #myx0 = min(myx0, h5[group]['X'].min())
    #myx1 = max(myx1, h5[group]['X'].max())
    #myy0 = min(myy0, h5[group]['Y'].min())
    #myy1 = max(myy1, h5[group]['Y'].max())
    myx0 = h5.get(group)['X'].min()
    myx1 = h5.get(group)['X'].max()
    myy0 = h5.get(group)['Y'].min()
    myy1 = h5.get(group)['Y'].max()

    
    if myx0 > bbox[1] or myx1 < bbox[0] or myy0 > bbox[3] or myy1 < bbox[2]:
        print '\tClipping group: ', group, myx0,myx1,myy0,myy1
    else:
        if mygroups is None:
            print 'Keeping group: ', group, myx0,myx1,myy0,myy1
            allgroups.append(group)
        elif mygroups not in group:
            print '\tClipping group: ', group, myx0,myx1,myy0,myy1
        elif mygroups in group:
            print 'Keeping group: ', group, myx0,myx1,myy0,myy1
            allgroups.append(group)
            



h5.close()

print 'Using groups: ', allgroups

dem0 = demBuilder(infile=datafile,\
        h5groups = allgroups,\
        convert2utm=False,\
        isnorth=False, \
        utmzone=51, \
        interptype='blockavg',\
        #interptype='kriging',\
        #interptype = 'nn',\
	bbox=bbox, \
        dx=dx, \
        maxdist=np.inf,\
        NNear = 12,\
        scale = scale,\
       )

dem0.build()
dem0.save(outfile='%s/%s'%(basedir, outfile) )


'''
#######
infile = r'DATA/EchucaDEM_GDA94_zone51.xyz'
datafile = '../ScottReef/DATA/GeoScience_Woodside_Bathy_v2.nc'
cutoff = -200
clevs = np.arange(-100, 0, 10).tolist()

sunpath = 'grids/Echuca_75m'
utmzone = 51
maxdist = 1000.
dx = 200

datafile_out = '../ScottReef/DATA/GeoScience_Woodside_Browse_UTM.nc'
infile_out = r'DATA/EchucaDEM_GDA94_zone51.nc'
outfile = 'DATA/GeoScience_Woodside_Echuca_Blend_200m.nc'
#######

#####
# Build a DEM with the geoscience bathy
#####
datafile = '../ScottReef/DATA/GeoScience_Woodside_Bathy_v2.nc'

dem0 = demBuilder(infile=datafile, convert2utm=True, interptype='nn',\
	bbox=bbox_ll, utmzone=utmzone, isnorth=False, dx=dx, maxdist=maxdist)
dem0.build()
dem0.save(outfile=datafile_out)

######
### Build the DEM
######
dem = demBuilder(infile=infile, convert2utm=False, interptype='blockavg',bbox=bbox_xy,dx=dx)
dem.build()

# Mask out the bad data
mask = dem.Z <=cutoff
dem.Z = np.ma.MaskedArray(dem.Z,mask=mask)
#dem.save(outfile=infile_out)

######
# Blend the two files
######
#ncfiles = [datafile_out,infile_out]
#weights = [1.,10.]
#maxdist = [10000.,10000.]
#blendDEMs(ncfiles,outfile,weights,maxdist)

######
## Plot the result
######
#dem.plot(vmin=-3000,vmax=0)
##C = dem.contourf(vv=clevs, cmap=plt.cm.gist_earth)
##Contour2Shp(C, shpfile)
#plt.show()
'''
