"""
Extract tides at Scott Reef
"""

import numpy as np
from soda.utils.othertime import TimeVector
from soda.dataio.conversion.readotps import tide_pred

import matplotlib.pyplot as plt
######
lon = 123.3513
lat = -13.7654

tstart = '20170326.000000'
tend = '20170411.000000'

# Rowley
lon = 119.000
lat = -17.823

tstart = '20190101.000000'
tend = '20191231.000000'

dt = 600.

tidemod = '/home/suntans/Share/ScottReef/DATA/TIDES/TPXO7.2/Model_tpxo7.2'

titlestr='Rowley Tides'
####

# Create time vector
time = TimeVector(tstart, tend, dt)

# Get the tides
h,u,v = tide_pred(tidemod, np.array([lon]), np.array([lat]), time)

for tt, zz in zip(time,h):
    print( '%s, %3.3f'%(tt, zz[0]))

## plot
plt.figure(figsize=(12,6))
plt.plot(time, h)
plt.title(titlestr)
plt.ylabel('SSH [m]')
plt.grid(b=True)
plt.tight_layout()
plt.xticks(rotation=17)

plt.tight_layout()

plt.show()

