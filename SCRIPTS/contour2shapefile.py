"""
Convert bathymetry data to a shapefile
"""

from soda.dataio.conversion.dem import DEM
from soda.utils.maptools import Contour2Shp, utm2ll
from soda.dataio.suntans.sunpy import Grid

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


###
# Inputs
basedir = '/home/suntans/Share/ScottReef/DATA/'
#demfile = '%s/BATHYMETRY/Browse_Blended_250m_DEM.nc'%basedir
#demfile = '%s/BATHYMETRY/GeoScience_Woodside_Bathy_v2.nc'%basedir
#demfile = '%s/BATHYMETRY/TimorSea_GAWEL_Multi_GEBCO_Combined_DEM.nc'%basedir
demfile = '%s/BATHYMETRY/TimorSea_GA_GEBCO_Combined_DEM.nc'%basedir


#clevs = [0.,0.]
#outfile = '%s/GIS/ScottReef_Coastline_LINE.shp'%basedir

#clevs = [0., -20., -50., -100., -200.,-300.,\
#        -400., -500., -1000., -2000., -3000., -4000., -5000]
#clevs = clevs[::-1]
#outfile = '%s/GIS/Browse_Blended_250m_contours.shp'%basedir
#outfile = '%s/GIS/NWS_Blended_250m_contours.shp'%basedir

#clevs = np.arange(-400,-90.,10.)
clevs = -np.array( [50,100,150,200,250,300,400,500,1000,2000,3000,4000,5000,6000])[::-1]
#outfile = '%s/GIS/Browse_Blended_250m_contours_100to400.shp'%basedir
outfile = '%s/GIS/NWS_TimorSea_GA_GEBCO_Blended_250m_contours.shp'%basedir


#utmzone = 51
#north=False
###

dem = DEM(demfile)


cc = dem.contour(vv=clevs, linewidths=0.25)

#Contour2Shp(cc, outfile, projection='UTM', zone=utmzone, north=north)
Contour2Shp(cc, outfile, projection='WGS84')

plt.show()
