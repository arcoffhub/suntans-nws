"""
Convert the GA bathy to a netcdf file

"""

import numpy as np
from glob import glob

import pandas as pd
import xray

from soda.utils.cartgrid import RegGrid


###########

# Domain size
x0 = 92.
x1 = 172.

y0 = -60.
y1 = -8.

dx = 0.0025

txtpath = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/GA2009/xyz_ascii_part_?/*.txt'
outfile = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY/GA2009/GA2009_250m_fullgrid.nc'


###########

# Get a list of all the txt files
filenames =  glob(txtpath)
nf = len(filenames)

# Create a grid object (used for finding indices)
grd = RegGrid([x0,x1], [y0,y1], dx, dx, meshgrid=False)

# Create an output array for the depth data
Z = 9999*np.ones((grd.ny, grd.nx), dtype=np.int16)

for ii, filename in enumerate(filenames):      
    print 'Loading (%d of %d)  %s...'%(ii, nf, filename)
    #data = np.loadtxt(filename, delimiter=',', skiprows=1)
    # Use pandas - numpy sucks
    data = pd.read_csv(filename)

    # Find the global grid indices of all points
    ii,jj = grd.returnij(data['X'], data['Y'])

    # Insert the data
    Z[jj,ii] = data['Z']
    

# Create an xray dataset
da = xray.DataArray(Z,
        dims=('lat','lon'),
        coords={'lat':grd.y, 'lon':grd.x},
        attrs={'long_name':'Topography height',
            'units':'m',
            'positive':'up',
            '_FillValue':9999}
        )
ds = xray.Dataset({'topo':da},
        attrs={'Name':'Geoscience Australian 2009 250 m dataset'}
     )
ds.to_netcdf(outfile, encoding={'topo':{'zlib':True}})
print 'Saved to netcdf file: %s'%outfile
print 72*'#'
