"""
Extract the OTPS tides over the domain
"""

from mpl_toolkits.basemap import Basemap
import numpy as np
import xray
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from datetime import datetime

from soda.utils.otherplot import axcolorbar

from soda.utils import othertime
#import cmocean
from soda.utils.maptools import plotmap
from soda.dataio.suntans.sunpy import Spatial

import pdb

def quivergrid(sun, ax,  u, v, dx, xlims, ylims, **kwargs):
    """
    Create a quiverplot on a regular grid
    """
    # Create the grid
    x0, x1 = xlims[0], xlims[1]
    y0, y1 = ylims[0], ylims[1]
    Xl,Yl = np.meshgrid( np.arange(x0, x1, dx),\
        np.arange(y0,y1, dx))

    # Convert the coordinates
    X,Y = ax(Xl, Yl)

    # Interpolate
    ug = sun.interpolate(u, X, Y)
    vg = sun.interpolate(v, X, Y)

    qq = plt.quiver(X, Y, ug, vg, **kwargs)

    return qq


######

# SUNTANS harmonics folder
runfolder =  'SCENARIOS/OUTPUT_NWS_5km_hex/'
ncfile = '%s/NWS_5km_hex_20*_surface.nc'%runfolder
#ncfile = '%s/TimorSea_15_2k_tri_3D_sponge_20*_AVG_0000.nc'%runfolder
outpath = 'FIGURES/SUNTANS_MonthlyMean_%s_Hex5k.png'


#cmap = cmocean.cm.amp
cmap = 'Spectral_r'

# Depth plotting
#basedir = r'/home/suntans/Share/ScottReef/DATA'
basedir = r'/group/pawsey0106/mrayson/DATA'

bathyfile = r'%s/BATHYMETRY/ETOPO1/ETOPO1_Bed.nc'%basedir
#bathylevs= [-1000.,-100.]
bathylevs= [100.,1000.]

#figfile = '../../FIGURES/TimorSea_SUNTANS_TideComparison'

xlims = [107.5,142.5]
ylims = [-25.0,-5.0]

clevs = np.arange(24, 32., 0.5)

ss = 6 # subsample interval for vectors
scalefac = 25.
dxvec = 0.6 #  degrees

klayer = [0]

dx = 0.025

#coastfile = r'%s/COAST/GSHHS_shp/i/GSHHS_i_L1.shp'%basedir

#######

# open the bathy
dsz = xray.open_dataset(bathyfile)

# Bathymetry plotting function
def plot_bathy(ax):
    
    #xlims = [da.lon.min(),da.lon.max()]
    #ylims = [da.lat.min(),da.lat.max()]
    z = dsz.topo.sel(lon=slice(xlims[0],xlims[1]), lat=slice(ylims[1],ylims[0]))
    #plt.contour(z.lon.values, z.lat.values, z.values,\
    #    bathylevs,
    #    linewidths=0.3,
    #    linestyles='-', colors='k')
    clevs = np.arange(-6000,200,100)
    x,y = np.meshgrid(z.lon.values, z.lat.values)
    ax.contour(z.lon.values, z.lat.values, z.values,\
        bathylevs,
        linewidths=0.3,
        linestyles='-', colors='k',
        latlon=True)

    return C



#da = xray.open_dataset(ncfile)

#xlims = [da.lon.min(),da.lon.max()]
#xlims = [112., 135.]
#ylims = [da.lat.min(),da.lat.max()]
#X, Y = np.meshgrid(da.lon.values, da.lat.values)
x = np.arange(xlims[0],xlims[1],dx)
y = np.arange(ylims[0],ylims[1],dx)
X,Y = np.meshgrid(x,y)

# Load the suntans object
sun = Spatial(ncfile, projstr='merc', klayer=klayer)

def plot_suntans_mean(tstart, tend, convert=True):
    print 'Loading data from %s to %s...'%(tstart, tend)
    # Compute the mean
    sun.tstep = sun.getTstep(tstart, tend)
    print 'tstep ', sun.tstep[0], sun.tstep[-1]

    print 'Loading temp...'
    temp = sun.loadData(variable='temp').mean(axis=0)
    print 'Loading uc...'
    u = sun.loadData(variable='uc').mean(axis=0)
    print 'Loading vc...'
    v = sun.loadData(variable='vc').mean(axis=0)

    ######
    # Plot it up
    ######
    fig = plt.figure(figsize=(12,8))

    ax = Basemap(projection='merc',\
            llcrnrlat=ylims[0],\
            urcrnrlat=ylims[1],\
            llcrnrlon=xlims[0],\
            urcrnrlon=xlims[1],\
            lat_0=ylims[0],\
            lon_0=xlims[0],\
                resolution='i')

    if convert:
        # Convert the suntans coordinates to lat/lon
        sun.xp, sun.yp = sun.to_latlon(sun.xp, sun.yp)
        sun.xv, sun.yv = sun.to_latlon(sun.xv, sun.yv)
        # Convert latlon to map coordinate
        sun.xp, sun.yp = ax(sun.xp, sun.yp)
        sun.xv, sun.yv = ax(sun.xv, sun.yv)
        sun.xy = sun.cellxy(sun.xp, sun.yp)



    ### SUNTANS data
    #ax = plt.subplot(211)

    #sun.plot(z=temp, xlims=xlims, ylims=ylims, cmap=cmap)
    #plt.colorbar(sun.patches)
    cf = sun.contourf(z=temp, clevs=clevs, xlims=xlims, ylims=ylims,\
            colorbar=False, cmap=cmap,\
            extend='min')


    qq = quivergrid(sun, ax, u, v, dxvec, xlims,ylims,\
            scale=scalefac,\
            #scale_units='xy',\
            color='k')

    qx,qy = ax(135., -22.)
    plt.quiverkey(qq, qx, qy, 1.0, '1.0 [m/s]', zorder=1e6, coordinates='data')

    #plt.quiverkey(qq, 0.8, 0.1, Escale, r'[%3.1f kW m$^{-1}$]'%(Escale*1e-3))

    #plotmap(coastfile, fieldname=None)

    # Plot the bathymetry
    cf2 = sun.contourf(z=sun.dv, clevs=bathylevs,\
            xlims=xlims, ylims=ylims,\
            colorbar=False, filled=False, \
            colors='k', linewidths=0.5,\
        )



    #plt.ylabel('Latitude [$^{\circ}$N]')
    #plt.xlabel('Longitude [$^{\circ}$E]')
    ##plt.text(0.7,0.1,'(a) SUNTANS $\eta_{%s}$'%con, transform=ax.transAxes)

    plt.title('')


    # Put this last to reset the axis
    ax.drawcoastlines()
    ax.fillcontinents()
    parallels = np.arange(np.ceil(ylims[0]), np.floor(ylims[1]),4.)
    meridians = np.arange(np.ceil(xlims[0]), np.floor(xlims[1]),4.)

    ax.drawparallels(parallels,labels=[1,0,0,0], linewidth=0.2)
    ax.drawmeridians(meridians,labels=[0,0,0,1],rotation=17, linewidth=0.2)
    ax.drawmapboundary(fill_color='w')
    ax.drawmapscale(128,-20, 130, -19.5, 500,barstyle='fancy',fontsize=9)

    cb=axcolorbar(cf, pos=[0.4,0.15,0.3,0.04])
    cb.ax.set_title('Temperature [$^{\circ}$C]')

    plt.tight_layout(pad=3.)

    outfile = outpath%tend[0:6]
    plt.savefig(outfile, dpi=150)

    print 'Figure saved to %s.'%outfile
    print 12*'#'

    #plt.show()

convert=True
d = pd.date_range('2013-12-31','2015-1-01',freq='M')
for t1,t2 in zip(d[0:],d[1:]):
    t1s = t1.strftime('%Y%m%d.%H%M')
    t2s = t2.strftime('%Y%m%d.%H%M')
    plot_suntans_mean(t1s, t2s, convert=convert)

    convert=False

print 'Done.'

