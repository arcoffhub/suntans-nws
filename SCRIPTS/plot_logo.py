import matplotlib.pyplot as plt
import matplotlib.image as image

imgfile = 'FIGURES/offhub_logo.png'

img = image.imread(imgfile)


plt.figure()
ax1 = plt.axis([0,1,0,1])

aximg = plt.axes([0.0,-0.1, 0.6, 0.6], frameon=True) 
aximg.imshow(img)
aximg.axis('off')

plt.show()
