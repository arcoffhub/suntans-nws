"""
Convert xyz data to a netcdf DEM model
"""


from soda.dataio.conversion.demBuilder import demBuilder
from soda.dataio.conversion.dem import blendDEMs
from soda.utils.maptools import Contour2Shp, utm2ll
from soda.dataio.suntans.sunpy import Grid

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

####

basedir = '/home/suntans/Share/ScottReef/DATA/BATHYMETRY'
#basedir = u'/home/suntans/Share/ARCHub/DATA/BATHYMETRY/'

#hdffile = 'WEL_NWS_raw_bathymetry.h5'
#outfile = 'Woodside_NWS_250m_DEM.nc'

#outfile = 'GA_Browse_SingleBeam_250_DEM.nc'
#groupstr = 'ga'

hdffile = 'BrowseRawBathymetry_xyz.h5'
outfile = 'WEL_Multibeam_Browse_250m_DEM.nc'
groupstr = None

# Coordinate info
utmzone = 51
isnorth = False
CS = 'WGS84'

# Grid parameters
dx = 250. / 1e5
#####

#####
## Get the domain bounds from the SUNTANS grid
#####
#grd = Grid(sunpath)
#xy = np.array([grd.xv,grd.yv]).T
#bbox_xy =\
#        [xy[:,0].min()-extend,xy[:,0].max()+extend,\
#	xy[:,1].min()-extend, xy[:,1].max()+extend]
#        
#
#
##ll = utm2ll(xy,zone=utmzone, north=False)
##bbox_ll = [ll[:,0].min(), ll[:,0].max(),\
#	#ll[:,1].min(), ll[:,1].max(),]
#


#####
# Build a DEM with the geoscience survey bathy
datafile = '%s/%s'%(basedir,hdffile)

# Load the hdf file
h5 = pd.HDFStore( datafile , 'r')
print h5.keys()
#groups = [ii for ii in h5.keys() if groupstr in ii]
groups = [ii for ii in h5.keys()]
#groups.remove('/gwf') # This is a bad file...

# Find the domain bounds
x0 = np.inf
x1 = -np.inf
y0 = np.inf
y1 = -np.inf
for group in groups:
    x0 = min(x0, h5[group]['X'].min())
    x1 = max(x1, h5[group]['X'].max())
    y0 = min(y0, h5[group]['Y'].min())
    y1 = max(y1, h5[group]['Y'].max())
    print group, x0,x1,y0,y1


h5.close()
bbox_xy = [x0,x1,y0,y1]



dem0 = demBuilder(infile=datafile,\
        h5groups = groups,\
        convert2utm=False,\
        isnorth=False, \
        utmzone=utmzone, \
        CS = CS, \
        interptype='blockavg',\
        #interptype='kriging',\
        #interptype = 'nn',\
	bbox=bbox_xy, \
        dx=dx, \
        maxdist=np.inf,\
        NNear = 12,\
       )

dem0.build()
dem0.save(outfile='%s/%s'%(basedir, outfile) )

plt.figure()
dem0.plot(shade=False)
plt.show()



'''
#######
infile = r'DATA/EchucaDEM_GDA94_zone51.xyz'
datafile = '../ScottReef/DATA/GeoScience_Woodside_Bathy_v2.nc'
cutoff = -200
clevs = np.arange(-100, 0, 10).tolist()

sunpath = 'grids/Echuca_75m'
utmzone = 51
maxdist = 1000.
dx = 200

datafile_out = '../ScottReef/DATA/GeoScience_Woodside_Browse_UTM.nc'
infile_out = r'DATA/EchucaDEM_GDA94_zone51.nc'
outfile = 'DATA/GeoScience_Woodside_Echuca_Blend_200m.nc'
#######

#####
# Build a DEM with the geoscience bathy
#####
datafile = '../ScottReef/DATA/GeoScience_Woodside_Bathy_v2.nc'

dem0 = demBuilder(infile=datafile, convert2utm=True, interptype='nn',\
	bbox=bbox_ll, utmzone=utmzone, isnorth=False, dx=dx, maxdist=maxdist)
dem0.build()
dem0.save(outfile=datafile_out)

######
### Build the DEM
######
dem = demBuilder(infile=infile, convert2utm=False, interptype='blockavg',bbox=bbox_xy,dx=dx)
dem.build()

# Mask out the bad data
mask = dem.Z <=cutoff
dem.Z = np.ma.MaskedArray(dem.Z,mask=mask)
#dem.save(outfile=infile_out)

######
# Blend the two files
######
#ncfiles = [datafile_out,infile_out]
#weights = [1.,10.]
#maxdist = [10000.,10000.]
#blendDEMs(ncfiles,outfile,weights,maxdist)

######
## Plot the result
######
#dem.plot(vmin=-3000,vmax=0)
##C = dem.contourf(vv=clevs, cmap=plt.cm.gist_earth)
##Contour2Shp(C, shpfile)
#plt.show()
'''
