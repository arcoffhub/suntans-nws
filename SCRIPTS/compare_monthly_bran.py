"""
Extract the OTPS tides over the domain
"""

import numpy as np
import xray
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from datetime import datetime

from soda.utils.otherplot import axcolorbar

from soda.utils import othertime
#import cmocean
from soda.utils.maptools import plotmap
from soda.dataio.suntans.sunpy import Spatial

def quivergrid(sun, u, v, dx, xlims, ylims, **kwargs):
    """
    Create a quiverplot on a regular grid
    """
    # Create the grid
    x0, x1 = xlims[0], xlims[1]
    y0, y1 = ylims[0], ylims[1]
    X,Y = np.meshgrid( np.arange(x0, x1, dx),\
        np.arange(y0,y1, dx))

    # Interpolate
    ug = sun.interpolate(u, X, Y)
    vg = sun.interpolate(v, X, Y)

    qq = plt.quiver(X, Y, ug, vg, **kwargs)

    return qq


######

# SUNTANS harmonics folder
runfolder =  'SCENARIOS/OUTPUT_TimorSea_5k_tri/'
ncfile = '%s/TimorSea_15_5k_200*_surface.nc'%runfolder
#ncfile = '%s/TimorSea_15_5k_tri_3D_sponge_20*_AVG_0000.nc'%runfolder
outpath = 'FIGURES/SUNTANS_MonthlyMean_%s_Tri5k_sponge.png'

#runfolder =  'SCENARIOS/OUTPUT_TimorSea_2k_tri/'
##ncfile = '%s/TimorSea_15_5k_200*_surface.nc'%runfolder
#ncfile = '%s/TimorSea_15_2k_tri_3D_sponge_20*_AVG_0000.nc'%runfolder
#outpath = 'FIGURES/SUNTANS_MonthlyMean_%s_Tri2k_sponge.png'

#runfolder =  'SCENARIOS/OUTPUT_TimorSea_5k_tri_notides/'
##ncfile = '%s/TimorSea_15_5k_200*_surface.nc'%runfolder
#ncfile = '%s/TimorSea_15_5k_tri_3D_notides_20*_AVG_0000.nc'%runfolder
#outpath = 'FIGURES/SUNTANS_MonthlyMean_%s_Tri5k_notides.png'


#runfolder =  'SCENARIOS/OUTPUT_TimorSea_2k_tri/'
#ncfile = '%s/TimorSea_15_2k_200*_surface.nc'%runfolder

## August
#runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_data1/'
#ncfile = '%s/TimorSea_15_5k_tri_3D_20090801_0*.nc'%runfolder
#runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_sponge_data1/'
#ncfile = '%s/TimorSea_15_5k_tri_3D_sponge_20090801_0*.nc'%runfolder
#ncfile = '%s/TimorSea_15_5k_tri_3D_sponge_20090801_AVG_0*.nc'%runfolder

tstart='20090801.0000'
tend='20090901.0000'
##outfile = 'FIGURES/SUNTANS_MonthlyMean_200908_Tri5k.png'
#outfile = 'FIGURES/SUNTANS_MonthlyMean_200908_Tri5k_sponge.png'


### September
##runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_data2/'
##ncfile = '%s/TimorSea_15_5k_tri_3D_20090901_0000.nc'%runfolder
#runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_sponge_data2/'
#ncfile = '%s/TimorSea_15_5k_tri_3D_sponge_20090901_0000.nc'%runfolder
#
#tstart='20090901.0000'
#tend='20091001.0000'
##outfile = 'FIGURES/SUNTANS_MonthlyMean_200909_Tri5k.png'
#outfile = 'FIGURES/SUNTANS_MonthlyMean_200909_Tri5k_sponge.png'


### October
##runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_data3/'
##ncfile = '%s/TimorSea_15_5k_tri_3D_20091001_0000.nc'%runfolder
#runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_sponge_data3/'
#ncfile = '%s/TimorSea_15_5k_tri_3D_sponge_20091001_0000.nc'%runfolder
#
#tstart='20091001.0000'
#tend='20091101.0000'
##outfile = 'FIGURES/SUNTANS_MonthlyMean_200910_Tri5k.png'
#outfile = 'FIGURES/SUNTANS_MonthlyMean_200910_Tri5k_sponge.png'

## November
##runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_data4/'
##ncfile = '%s/TimorSea_15_5k_tri_3D_20091101_0000.nc'%runfolder
#runfolder = '/scratch/pawsey0106/mrayson/SUNTANS/TimorSea/SCENARIOS/TimorSea_15_5k_tri_3D_sponge_data4/'
#ncfile = '%s/TimorSea_15_5k_tri_3D_sponge_20091101_0000.nc'%runfolder
#
#tstart='20091101.0000'
#tend='20091201.0000'
##outfile = 'FIGURES/SUNTANS_MonthlyMean_200911_Tri5k.png'
#outfile = 'FIGURES/SUNTANS_MonthlyMean_200911_Tri5k_sponge.png'

## December
#tstart='20091201.0000'
#tend='20100101.0000'
##outfile = 'FIGURES/SUNTANS_MonthlyMean_200912_Tri5k.png'
#outfile = 'FIGURES/SUNTANS_MonthlyMean_200912_Tri5k_sponge.png'

## January
#tstart='20100101.0000'
#tend='20100201.0000'
##outfile = 'FIGURES/SUNTANS_MonthlyMean_201001_Tri5k.png'
#outfile = 'FIGURES/SUNTANS_MonthlyMean_201001_Tri5k_sponge.png'




#cmap = cmocean.cm.amp
cmap = 'Spectral_r'

# Depth plotting
basedir = r'/home/suntans/Share/ScottReef/DATA'
#basedir = r'/group/pawsey0106/mrayson/DATA'
bathyfile = r'%s/BATHYMETRY/ETOPO1/ETOPO1_Bed.nc'%basedir
#bathylevs= [-1000.,-100.]
bathylevs= [100.,1000.]

#figfile = '../../FIGURES/TimorSea_SUNTANS_TideComparison'

xlims = [107.5,142.5]
ylims = [-25.0,-5.0]

clevs = np.arange(24, 32., 0.5)

ss = 6 # subsample interval for vectors
scalefac = 25.
dxvec = 0.6 #  degrees

klayer = [0]

dx = 0.025

coastfile = r'%s/COAST/GSHHS_shp/i/GSHHS_i_L1.shp'%basedir

#######

# open the bathy
dsz = xray.open_dataset(bathyfile)

# Bathymetry plotting function
def plot_bathy():
    #plot_skill()
    #xlims = [da.lon.min(),da.lon.max()]
    #ylims = [da.lat.min(),da.lat.max()]
    z = dsz.topo.sel(lon=slice(xlims[0],xlims[1]), lat=slice(ylims[1],ylims[0]))
    plt.contour(z.lon.values, z.lat.values, z.values,\
        bathylevs,
        linewidths=0.3,
        linestyles='-', colors='k')

#da = xray.open_dataset(ncfile)

#xlims = [da.lon.min(),da.lon.max()]
#xlims = [112., 135.]
#ylims = [da.lat.min(),da.lat.max()]
#X, Y = np.meshgrid(da.lon.values, da.lat.values)
x = np.arange(xlims[0],xlims[1],dx)
y = np.arange(ylims[0],ylims[1],dx)
X,Y = np.meshgrid(x,y)

# Load the suntans object
sun = Spatial(ncfile, projstr='merc', klayer=klayer)

# Convert the suntans coordinates to lat/lon
sun.xp, sun.yp = sun.to_latlon(sun.xp, sun.yp)
sun.xv, sun.yv = sun.to_latlon(sun.xv, sun.yv)
sun.xy = sun.cellxy(sun.xp, sun.yp)

def plot_suntans_mean(tstart, tend):
    print 'Loading data from %s to %s...'%(tstart, tend)
    # Compute the mean
    sun.tstep = sun.getTstep(tstart, tend)
    print 'tstep ', sun.tstep[0], sun.tstep[-1]

    print 'Loading temp...'
    temp = sun.loadData(variable='temp').mean(axis=0)
    print 'Loading uc...'
    u = sun.loadData(variable='uc').mean(axis=0)
    print 'Loading vc...'
    v = sun.loadData(variable='vc').mean(axis=0)

    ######
    # Plot it up
    ######
    fig = plt.figure(figsize=(12,8))

    ### SUNTANS data
    #ax = plt.subplot(211)

    #sun.plot(z=temp, xlims=xlims, ylims=ylims, cmap=cmap)
    #plt.colorbar(sun.patches)
    cf = sun.contourf(z=temp, clevs=clevs, xlims=xlims, ylims=ylims,\
            colorbar=False, cmap=cmap,\
            extend='min')

    qq = quivergrid(sun, u, v, dxvec, xlims,ylims,\
            scale=scalefac,\
            #scale_units='xy',\
            color='k')

    plt.quiverkey(qq, 135., -22., 1.0, '1.0 [m/s]', zorder=1e6, coordinates='data')

    #plt.quiverkey(qq, 0.8, 0.1, Escale, r'[%3.1f kW m$^{-1}$]'%(Escale*1e-3))

    plotmap(coastfile, fieldname=None)

    # Plot the bathymetry
    cf2 = sun.contourf(z=sun.dv, clevs=bathylevs,\
            xlims=xlims, ylims=ylims,\
            colorbar=False, filled=False, \
            colors='k', linewidths=0.5,\
        )



    plt.ylabel('Latitude [$^{\circ}$N]')
    plt.xlabel('Longitude [$^{\circ}$E]')
    #plt.text(0.7,0.1,'(a) SUNTANS $\eta_{%s}$'%con, transform=ax.transAxes)

    plt.title('')

    cb=axcolorbar(cf, pos=[0.4,0.15,0.3,0.04])
    cb.ax.set_title('Temperature [$^{\circ}$C]')

    plt.tight_layout()

    outfile = outpath%tend[0:6]
    plt.savefig(outfile, dpi=150)

    print 'Figure saved to %s.'%outfile
    print 12*'#'

    plt.show()

d = pd.date_range('2009-07-31','2010-2-01',freq='M')
for t1,t2 in zip(d[0:],d[1:]):
    t1s = t1.strftime('%Y%m%d.%H%M')
    t2s = t2.strftime('%Y%m%d.%H%M')
    plot_suntans_mean(t1s, t2s)

print 'Done.'

