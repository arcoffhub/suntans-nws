"""
Convert xyz data to a netcdf DEM model
"""


from soda.dataio.conversion.demBuilder import demBuilder
from soda.dataio.conversion.dem import blendDEMs
from soda.utils.maptools import Contour2Shp, utm2ll
from soda.dataio.suntans.sunpy import Grid

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

####

basedir = '/home/suntans/Share/Projects/ScottReef/DATA/BATHYMETRY'

sunpath = 'GRIDS/TOM_10k_250m/'

outfile = 'GeoScience_Woodside_Browse_250m_DEM.nc'
#outfile = 'GA_Browse_SingleBeam_250_DEM.nc'
#groupstr = 'ga'

#outfile = 'WEL_Browse_Composite_250m_DEM.nc'
#groupstr = 'Scott'

# Coordinate info
utmzone = 51
isnorth = False
CS = 'WGS84'

# Grid parameters
dx = 250.
extend = 1e4
#####

####
# Get the domain bounds from the SUNTANS grid
####
grd = Grid(sunpath)
xy = np.array([grd.xv,grd.yv]).T
bbox_xy =\
        [xy[:,0].min()-extend,xy[:,0].max()+extend,\
	xy[:,1].min()-extend, xy[:,1].max()+extend]
        
#ll = utm2ll(xy,zone=utmzone, north=False)
#bbox_ll = [ll[:,0].min(), ll[:,0].max(),\
	#ll[:,1].min(), ll[:,1].max(),]

#####
# Build a DEM with the geoscience survey bathy
#hdffile = 'BrowseRawBathymetry_xyz.h5'
#datafile = '%s/%s'%(basedir,hdffile)
#
## Load the hdf file
#h5 = pd.HDFStore( datafile , 'r')
#print h5.keys()
#groups = [ii for ii in h5.keys() if groupstr in ii]
#h5.close()

groups=None
ncfile = 'GeoScience_Woodside_Bathy_v2.nc'
datafile = '%s/%s'%(basedir,ncfile)


dem0 = demBuilder(infile=datafile,\
        h5groups = groups,\
        convert2utm=True,\
        isnorth=False, \
        utmzone=utmzone, \
        CS = CS, \
        interptype='blockavg',\
        #interptype='kriging',\
        #interptype = 'nn',\
	bbox=bbox_xy, \
        dx=dx, \
        maxdist=np.inf,\
        NNear = 12,\
       )

dem0.build()
dem0.save(outfile='%s/%s'%(basedir, outfile) )


'''
#######
infile = r'DATA/EchucaDEM_GDA94_zone51.xyz'
datafile = '../ScottReef/DATA/GeoScience_Woodside_Bathy_v2.nc'
cutoff = -200
clevs = np.arange(-100, 0, 10).tolist()

sunpath = 'grids/Echuca_75m'
utmzone = 51
maxdist = 1000.
dx = 200

datafile_out = '../ScottReef/DATA/GeoScience_Woodside_Browse_UTM.nc'
infile_out = r'DATA/EchucaDEM_GDA94_zone51.nc'
outfile = 'DATA/GeoScience_Woodside_Echuca_Blend_200m.nc'
#######

#####
# Build a DEM with the geoscience bathy
#####
datafile = '../ScottReef/DATA/GeoScience_Woodside_Bathy_v2.nc'

dem0 = demBuilder(infile=datafile, convert2utm=True, interptype='nn',\
	bbox=bbox_ll, utmzone=utmzone, isnorth=False, dx=dx, maxdist=maxdist)
dem0.build()
dem0.save(outfile=datafile_out)

######
### Build the DEM
######
dem = demBuilder(infile=infile, convert2utm=False, interptype='blockavg',bbox=bbox_xy,dx=dx)
dem.build()

# Mask out the bad data
mask = dem.Z <=cutoff
dem.Z = np.ma.MaskedArray(dem.Z,mask=mask)
#dem.save(outfile=infile_out)

######
# Blend the two files
######
#ncfiles = [datafile_out,infile_out]
#weights = [1.,10.]
#maxdist = [10000.,10000.]
#blendDEMs(ncfiles,outfile,weights,maxdist)

######
## Plot the result
######
#dem.plot(vmin=-3000,vmax=0)
##C = dem.contourf(vv=clevs, cmap=plt.cm.gist_earth)
##Contour2Shp(C, shpfile)
#plt.show()
'''
