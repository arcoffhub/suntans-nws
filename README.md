# SUNTANS North West Shelf domain

## SCENARIO Descriptions

 - NWS_2km_GLORYS_hex_Nk80dt60_2013_2014: Base case for GLORYS run (dt=60, Nk=80, z0=0.002, dzmin=0)
   Notes: ran to completion
 - NWS_2km_GLORYS_hex_z0dzfix: Updated z0 0.002 --> 1e-5, Activated fixdzz and dzsmall=0.2
        Notes: 
            - Case 1 w/ above parameters: failed during month=6
            - Case 2: z0=0.002 : failed during month=7
 - NWS_2km_GLORYS_hex_Cddzfix: Quadratic drag w/ CdB=0.0025, Activated fixdzz and dzsmall=0.2

---

Matt Rayson

UWA

December 2017
